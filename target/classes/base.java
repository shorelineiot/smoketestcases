package Resources;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.time.Duration;

import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.PageLoadStrategy;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;

public class base {
	
	//Global Declaration
	public static WebDriver driver;
	public Properties prop;
	
	
	
	protected static  String fileSeprator = File.separator;
	protected static String downloadFilePath = System.getProperty("user.dir")+ fileSeprator + "src" + fileSeprator
			+ "main" + fileSeprator + "resources" + fileSeprator + "downloads";
	
	//Initialize WebDriver  and Browser Invokation
	public WebDriver InitializeDriver() throws IOException
	{
		prop = new Properties();
		FileInputStream fis = new FileInputStream(System.getProperty("user.dir")+"/src/main/java/Resources/data.properties");
		prop.load(fis);
	    
	    String browserName=prop.getProperty("browser");
	    System.out.println("Browser:"+browserName);
	    
	    if (browserName.equalsIgnoreCase("chrome"))
	    {
	    	System.setProperty("webdriver.chrome.silentOutput","true");
	    	String destinationFileChrome = System.getProperty("user.dir")+"/drivers/chromedriver";
	    	System.setProperty("webdriver.chrome.driver", destinationFileChrome);
	    	ChromeOptions options = new ChromeOptions();
	    	options.setPageLoadStrategy(PageLoadStrategy.EAGER);
	    	driver = new ChromeDriver(options);
	    }
	    
	    else if(browserName.equalsIgnoreCase("firefox"))
	    {
	    	String destinationFileFirefox = System.getProperty("user.dir")+"/drivers/geckodriver";
	    	System.setProperty("webdriver.gecko.driver", destinationFileFirefox);
	    	driver = new FirefoxDriver();
	    }
	    
	    //ImplicitWait
	    //driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	    driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(90));
	    
	    return driver;	    
	}
	
	//Wait Methods
	public static WebDriverWait getWait(int timeOutInSeconds, int pollingEveryInMilliSec)
    {
         //WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
         WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(120));
         //wait.pollingEvery(pollingEveryInMiliSec, TimeUnit.MILLISECONDS);
         wait.ignoring(NoSuchElementException.class);
         wait.ignoring(ElementNotVisibleException.class);
         wait.ignoring(StaleElementReferenceException.class);
         return wait;
    }
	//waitForElementToBeVisible
	public static void waitForElementToBeVisible(WebElement locator, int timeOutInSeconds,
			int pollingEveryInMiliSec) {
		WebDriverWait wait = getWait(timeOutInSeconds, pollingEveryInMiliSec);
		wait.until(ExpectedConditions.visibilityOf(locator));
	}
	//waitForElementToBeSelected
	public static void waitForElementToBeSelected(WebElement locator, int timeOutInSeconds,
			int pollingEveryInMiliSec) {
		WebDriverWait wait = getWait(timeOutInSeconds, pollingEveryInMiliSec);
		wait.until(ExpectedConditions.elementToBeSelected(locator));
	}
	//waitForInvisibilityOfElementLocated
	public static void waitForInvisibilityOfElementLocated(WebElement locator, int timeOutInSeconds,
			int pollingEveryInMiliSec) {
		WebDriverWait wait = getWait(timeOutInSeconds, pollingEveryInMiliSec);
		wait.until(ExpectedConditions.invisibilityOf(locator));
	}
	// waitForElementToBeClickable	
	public static void waitForElementToBeClickable(WebElement locator, int timeOutInSeconds,
			int pollingEveryInMiliSec) {
		WebDriverWait wait = getWait(timeOutInSeconds, pollingEveryInMiliSec);
		wait.until(ExpectedConditions.elementToBeClickable(locator));
	}
	//isElementExists
	public static boolean isElementExists(WebElement ele) {
		try {
			return ele.isEnabled();
		} catch (Exception e) {
			return false;
		}
	}
	//ScrollTillElement
	public static void scrollTillElement(WebElement element) {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
	}
    //Download File
	public static boolean downloadFile(String file_name) {
		File folder = new File(downloadFilePath);

		// List the files on that folder
		File[] listOfFiles = folder.listFiles();
		boolean found = false;
		File f = null;
		for (File listOfFile : listOfFiles) {
			if (listOfFile.isFile()) {
				String fileName = listOfFile.getName();
				//System.out.println("File " + listOfFile.getName());
				if (fileName.contains(file_name)) {		
					f = new File(downloadFilePath + fileSeprator + fileName);
					found = true;
					//System.out.println(f.delete());
				}
			}
		}
		return found;
	}
	/*
	//Screenshot
	public String getScreenShotPath(String testcaseName, WebDriver driver) throws IOException
	{
		TakesScreenshot ts = (TakesScreenshot) driver;
		File source = ts.getScreenshotAs(OutputType.FILE);
		String destinationFile = System.getProperty("user.dir")+"/reports/"+testcaseName+".png";
		FileUtils.copyFile(source, new File(destinationFile));
		return destinationFile;
	}
	*/
}
