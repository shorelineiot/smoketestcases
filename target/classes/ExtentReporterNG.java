package Resources;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;

public class ExtentReporterNG {
	
	public static ExtentReports extent;
	public static ExtentSparkReporter reporter;
	
	public static ExtentReports getReportObject()
	{
		String path = System.getProperty("user.dir")+"/reports/AutomationReport.html";
		reporter = new ExtentSparkReporter(path);
		reporter.config().setReportName("Web Automation Results");
		reporter.config().setDocumentTitle("SL-Cloud Web Report");
		
		extent = new ExtentReports();
		extent.attachReporter(reporter);
		extent.setSystemInfo("QA", "Amey Deshpande");
		return extent;
	}
}
