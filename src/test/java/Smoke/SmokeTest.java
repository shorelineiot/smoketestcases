package Smoke;

import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.ViewName;

import java.io.File;
import java.io.IOException;
import java.time.Duration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;

import PageObjects.ActivityLogsPage;
import PageObjects.AlarmsPage;
import PageObjects.DashboardPage;
import PageObjects.DeviceDataPage;
import PageObjects.DeviceSettingsPage;
import PageObjects.DevicesPage;
import PageObjects.LoginPage;
import PageObjects.NarrowbandPage;
import PageObjects.OrgSettingsPage;
import PageObjects.RulesPage;
import Resources.base;

public class SmokeTest extends base{
	
	public static Logger log =LogManager.getLogger(SmokeTest.class.getName());
	
	//Declare WebDriver  Globally
    public WebDriver driver;
    
    
    /*Login Method*/
    public void LoginTest() throws IOException, InterruptedException
    {
    	//Login Screen
    	LoginPage lp = new LoginPage(driver);
    	//Email Address
    	lp.getEmailAddress();	
    	//RememberMe Checkbox
    	lp.getRememberMeCheckbox();
    	//NextButton
    	lp.getNextButton();
    	//Password
    	lp.getPassword();
    	Thread.sleep(3000);
    	//LoginButton
    	lp.getLoginButton();
    	Thread.sleep(8000);
    }
    
    /*Logout Method*/
    public void LogoutTest() throws InterruptedException
    {
    	//DashboardPage
  		DashboardPage dp1 = new DashboardPage(driver);		
		//Username
		dp1.getUserName().click();
		//User Logout
		dp1.getUserLogout();
		driver.navigate().refresh();  	
    }
    
    @BeforeTest
	public void BeforeTest() throws IOException, InterruptedException
	{
		System.out.println("Execution started for class 'SmokeTest'");
		log.info("Execution started for class 'SmokeTest'");
		
		driver=InitializeDriver();			
		driver.manage().window().maximize();
		System.out.println("Initialised Driver and Maximised the Browser Window");
		log.info("Initialised Driver and Maximised the Browser Window");
		
		//Launch
		driver.get(prop.getProperty("testv2url"));
		System.out.println("Launched Admin Portal:"+driver.getCurrentUrl());
		log.info("Launched Admin Portal:"+driver.getCurrentUrl());
				
	}
    
    
    //User Login Cases   
    //TestCase:  User Login Test
    @Test(priority=1)
    public void UserLoginTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: User Login Test");
  		log.info("Test Case: User Login Test");
  		  		
  		driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(90));
		
  		//Calling LoginTest Method
  		LoginTest();
		//DashboardPage
  		DashboardPage dp1 = new DashboardPage(driver);		
		//Username
		dp1.getUserName();
		
	}
    
    //TestCase:  User Logout Test
    @Test(priority=2)
    public void UserLogout() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: User Logout Test");
  		log.info("Test Case: User Logout Test");
  		
  		driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(90));
		
  		//Calling Logout Test Method
  		LogoutTest();		
	}
    
    //Alarms Cases//
    //TestCase: Alarm Tab click
    @Test(priority=3)
    public void AlarmTabTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Alarm Tab click");
  		log.info("Test Case: Alarm Tab click");
  	      		
  		//Calling LoginTest Method
  		LoginTest();
  	    //DashboardPage
  		DashboardPage dp1 = new DashboardPage(driver);		
		//Username
		dp1.getUserName();	
  		//AlarmsPage
  		AlarmsPage ap = new AlarmsPage(driver);
  		//Alarms Tab
  		ap.getAlarmsTab();  		
    }
    
    //TestCase: Alarms Page Clear filter Test 
    @Test(priority=4)
    public void AlarmClearFilterTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Alarms Page Clear filter Test");
  		log.info("Test Case: Alarms page Clear filter Test"); 		
  		  		
  		//loginTestCase();
  		AlarmsPage ap = new AlarmsPage(driver);
  		//lp.getAlarmsTab();
  		Thread.sleep(2000);
  		//Clear All Alarms
  		ap.clearAllAlarms();
  		//driver.navigate().refresh();
    }
    
    //TestCase:Alarms Select Severity Filter Test
    @Test(priority=5)
    public void AlarmSelectSeverityFilterTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Alarms Select Severity Filter Test");
  		log.info("Test Case: Alarms Select Severity Filter Test");
  		
  		//loginTestCase();
  	    //DashboardPage
  	  	DashboardPage dp1 = new DashboardPage(driver);		 		
  		//Thread.sleep(8000);		
		//Username
		dp1.getUserName();
  		//Devices Tab
  		dp1.getDevicesTab();
  		//Thread.sleep(6000);
  		//Alarms  Page
  		AlarmsPage ap = new AlarmsPage(driver);
  		//Alarms Tab
  		ap.getAlarmsTab();
  		Thread.sleep(2000);
  		//Severity Filter
  		ap.SelectSeverityFilter();
  		driver.navigate().refresh();
  		Thread.sleep(4000);
  		//Calling Logout Method
  		LogoutTest();
  		//driver.navigate().refresh();
    }
    
    //TestCase: Alarms Fixed Alarm Test
    @Test(priority=6)
    public void AlarmFixedAlarmTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Alarms Fixed Alarm Test");
  		log.info("Test Case: Alarms Fixed Alarm Test");
  		
  		//Calling Login Method
  		LoginTest();
  		//Thread.sleep(6000);
  	    //DashboardPage
  	  	DashboardPage dp1 = new DashboardPage(driver);		
		//Username
		dp1.getUserName();
  	  	//Devices Tab
  		//dp1.getDevicesTab();
  		//Alarms Page
  		AlarmsPage ap = new AlarmsPage(driver);
  		//Alarms Tab
        Thread.sleep(3000);
  		ap.getAlarmsTab();
  		//Fixed Alarm
  		ap.fixedAlarms();
  	    //Calling Logout Method
  		LogoutTest();
  		//driver.navigate().refresh();
  		//Thread.sleep(4000);
    }
    
    //TestCase: Alarms False Alarm Test
    @Test(priority=7)
    public void AlarmFalseAlarmTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Alarms Page False Alarm Test");
  		log.info("Test Case: Alarms Page False Alarm Test");
  	    //Calling Login Method
  		LoginTest();
  		//Thread.sleep(6000);
  	    //DashboardPage
  		DashboardPage dp1 = new DashboardPage(driver);		
		//Username
		dp1.getUserName();
  		//Alarms Page
  		AlarmsPage ap = new AlarmsPage(driver);
  		//Alarms Tab
  		Thread.sleep(3000);
  		ap.getAlarmsTab();  		
  		//False Alarm
  		ap.falseAlarms();
  		//driver.navigate().refresh();
    }
    
    //TestCase: Alarms Color
    @Test(priority=8)
    public void AlarmColorTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Alarms Color");
  		log.info("Test Case: Alarms Color");
  	    //Calling Login Method
  		//LoginTest();
  		//Thread.sleep(6000);
  		AlarmsPage ap = new AlarmsPage(driver);
  		//ap.getAlarmsTab();
  		//Thread.sleep(6000);
  		//Crtical  Alrm Text
  		ap.getCriticalAlarms();
  		//Critical Alarms Color
  		ap.getCriticalAlarmsColor();
  	    //Moderate  Alrm Text
  		ap.getModerateAlarms();
  		//Moderate Alarms Color
  		ap.getModerateAlarmsColor();
  	    //High  Alrm Text
  		ap.getHighAlarms();
  		//High Alarms Color
  		ap.getHighAlarmsColor();
  	    //Low  Alrm Text
  		ap.getLowAlarms();
  		//Critical Alarms Color
  		ap.getLowAlarmsColor();
  	    //Calling Logout Method
  		LogoutTest();
    }
    
    //RulesCases
    //TestCase: Rules Create
    @Test(priority=9)
    public void RulesCreateTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Rules Create");
  		log.info("Test Case: Rules Create");
  	    //Calling Login Method
  		LoginTest();
  		Thread.sleep(6000);
  	    //DashboardPage
  		DashboardPage dp1 = new DashboardPage(driver);		
		//Username
		dp1.getUserName();
  		//RulesTab
  		dp1.getRulesTab();
  		//RulesPage
  		RulesPage rp = new RulesPage(driver);
  		//Create Rule Button
  		rp.getCreateRuleButton();
  		//Rule Name
  		rp.getRuleNameField();
  		//HoldOffTime
  		rp.getRuleHoldOffTime();
  		//Rule Level dropdown
  		rp.getRuleLevelDropdown();
  		//Rule Level dropdown select
  		rp.getRuleLevelDropdownSelect();
  		//Select Device Dropdown
  		rp.getRuleDeviceDropdown();
  		Thread.sleep(2000);
  		// Select Device
  		rp.getRuleDeviceSelect();
  		Thread.sleep(3000);
  		//Sensor Dropdown
  		rp.getRuleSensorDropdown();
  		Thread.sleep(2000);
  		//SensorDropdownSelect
  		rp.getRuleSensorSelect();
  		Thread.sleep(2000);
  		//condition Select
  		rp.getRuleConditionDropdown();
  		//Condition Select
  		rp.getRuleConditionSelect();
  		//Condition Value
  		rp.getRuleConditionValue();
  		//Next Button
  		rp.getNextButton();
  		//Save Rule
  		Thread.sleep(2000);
  		rp.getSaveRuleButton();
  		Thread.sleep(2000);
  	    //Toaster
  		//rp.getToasterMessageRuleCreate();
  	    //Calling Logout Method
  		LogoutTest();
  	    driver.navigate().refresh();
    }
    
    //TestCase: Rules Delete
    @Test(priority=10)
    public void RulesDeleteTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Rules Delete");
  		log.info("Test Case: Rules Delete");
  	    //Calling Login Method
  		LoginTest();
  		//Thread.sleep(6000);
  		//DashboardPage
  		DashboardPage dp = new DashboardPage(driver);		
		//Username
		dp.getUserName();
  		//RulesTab
  		dp.getRulesTab();
  		//RulesPage
  		RulesPage rp = new RulesPage(driver);
        //Ssarch Rule
  		rp.getSearchRuleField();
  		Thread.sleep(3000);
  		//Delete
  		rp.getDeleteSearchRule();
  		//Apply Button
  		rp.getApplyButtonConfirmation();
  		Thread.sleep(3000);
  		//Toaster
  		//rp.getToasterMessageRuleDelete();
  	    //Calling Logout Method
  		LogoutTest();
  		driver.navigate().refresh();
    }
    
    //TestCase: Rules Update
    @Test(priority=11)
    public void RulesUpdateTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Rules Update");
  		log.info("Test Case: Rules Update");
  	    //Calling Login Method
  		LoginTest();
  		//Thread.sleep(6000);
  		//DashboardPage
  		DashboardPage dp = new DashboardPage(driver);
  	    //Username
  		dp.getUserName();
  	    //RulesTab
  		dp.getRulesTab();
  		//RulesPage
  		RulesPage rp = new RulesPage(driver);
  	    //Ssarch Rule
  		rp.getSearchRuleFieldUpdate();
  		Thread.sleep(3000);
        //First Rule Dropdown
  		rp.getSecondRuleDropdowwn();
  		Thread.sleep(4000);
  		//First Rule Dropdown Select
  		rp.getSecondRuleDropdowwnSelect();
  		//Apply Button
  		rp.getApplyButtonConfirmation();
  		//Toaster
  		rp.getToasterMessageRuleUpdate();
  	    //Calling Logout Method
  		LogoutTest();
    }

     
    //Narrowband Rules
    @Test
    public void NarrowbandRulesSearchTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Narrowband Rules search");
  		log.info("Test Case: Narrowband Rules search");
  	    //Calling Login Method
  		LoginTest();
  		Thread.sleep(6000);
  	    //DashboardPage
  		DashboardPage dp1 = new DashboardPage(driver);		
		//Username
		dp1.getUserName();
  		//RulesTab
  		dp1.getRulesTab();
  		Thread.sleep(2000);
  		RulesPage rp = new RulesPage(driver);
  		rp.getNarrowbandRuleButton();
  		rp.searchNarrowband();
  		LogoutTest();
    }
    @Test
    public void NarrowbandRulesEditTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Narrowband Rules edit");
  		log.info("Test Case: Narrowband Rules edit");
  	    //Calling Login Method
  		LoginTest();
  		Thread.sleep(6000);
  	    //DashboardPage
  		DashboardPage dp1 = new DashboardPage(driver);		
		//Username
		dp1.getUserName();
  		//RulesTab
  		dp1.getRulesTab();
  		Thread.sleep(2000);
  		RulesPage rp = new RulesPage(driver);
  		rp.getNarrowbandRuleButton();
  		rp.editNarrowbandRule();
  		LogoutTest();
    }
    @Test
    public void NarrowbandRulesMessageTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Narrowband Rules edit");
  		log.info("Test Case: Narrowband Rules edit");
  	    //Calling Login Method
  		LoginTest();
  		Thread.sleep(6000);
  	    //DashboardPage
  		DashboardPage dp1 = new DashboardPage(driver);		
		//Username
		dp1.getUserName();
  		//RulesTab
  		dp1.getRulesTab();
  		Thread.sleep(2000);
  		RulesPage rp = new RulesPage(driver);
  		rp.getNarrowbandRuleButton();
  		rp.MessageNarrowbandRule();
  		LogoutTest();
    }
    @Test
    public void NarrowbandRulesDeleteTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Narrowband Rules delete");
  		log.info("Test Case: Narrowband Rules delete");
  	    //Calling Login Method
  		LoginTest();
  		Thread.sleep(6000);
  	    //DashboardPage
  		DashboardPage dp1 = new DashboardPage(driver);		
		//Username
		dp1.getUserName();
  		//RulesTab
  		dp1.getRulesTab();
  		Thread.sleep(2000);
  		RulesPage rp = new RulesPage(driver);
  		rp.getNarrowbandRuleButton();
  		rp.deleteNarrowbandRule();
  		LogoutTest();
    }
    

    //Device Cases
    //TestCase: Device Tab Click Test
    @Test(priority=12)
    public void DeviceTabClickTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Device Tab Click Test");
  		log.info("Test Case: Device Tab Click Test");
  		
  		//Calling LoginTest Method
  		LoginTest();
  		//DashboardPage
  	  	DashboardPage dp1 = new DashboardPage(driver);
  	    //Username
  		dp1.getUserName();
  		//Thread.sleep(8000);
  		//Devices Tab
  		dp1.getDevicesTab();
    }
    
    //TestCase: UpdateDeviceDetails
    @Test(priority=13)
    public void UpdateDeviceTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Update device details Test");
  		log.info("Test Case: Update device details Test");
  	 
  		//DevicesPage
  		DevicesPage dp1 = new DevicesPage(driver);
  		//First Device
  	    dp1.getFirstDevice();
  	    //DeviceDataPage
  	    DeviceDataPage dp2 = new DeviceDataPage(driver);
  	    //Settings Icon
  	    dp2.getSettingsIcon();
  	    //DeviceSettingsPage
  	    DeviceSettingsPage dp3 = new DeviceSettingsPage(driver);
  	    // DeviceName
  	    dp3.getDeviceName();
  	    //Save
  	    dp3.getDeviceNameSave();
  	    Thread.sleep(3000);
  	    //ToasterMessage
  	    //dp3.getToasterMessage();
  	    //Calling Logout Method
  		LogoutTest();
    }
    
    //TestCase: Create Device Test
    @Test(priority=14)
    public void CreateDeviceTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Create Device Test");
  		log.info("Test Case: Create Device Test");
  		
  		driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(90));
  		
  	    //Calling LoginTest Method
  		LoginTest();
  		//DashboardPage
  	  	DashboardPage dp1 = new DashboardPage(driver);
  	    //Username
  		dp1.getUserName();  		
  		//Thread.sleep(8000);
  		//Devices Tab
  		dp1.getDevicesTab();
  		
  		//DevicesPage
  		DevicesPage dp2 = new DevicesPage(driver);
  	    //Username
  		dp1.getUserName();
  		//Thread.sleep(4000);
  		//Create Device Button
  		dp2.getCreateDeviceButton();
  		//Select Device
  		dp2.getSelectDeviceCreation();
  		//Device Name
  		dp2.getDeviceName();
  		//Device Type Dropdown
  		dp2.getDeviceTypeDropdown();
  		//Device Type Icastsense
  		dp2.getDeviceTypeDropdownICastSense();
  		Thread.sleep(10000);
  		//Group
  		//dp2.getGroupDropdown();
  		//Group AD-Test
  		//dp2.getGroupDropdownADTest();
  	    //hread.sleep(5000);
  		//Save Button
  		dp2.getSaveButton();
  		//Success Toaster
  		//dp2.getSuccessToasterText();
  		Thread.sleep(4000);
  		driver.navigate().refresh();
  	    //Calling Logout Method
  		LogoutTest();  		
    }
    
    //TestCase: Device Delete
    @Test(priority=15)
    public void DeviceDelete() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Device Delete");
  		log.info("Test Case: Device Delete");
  	 
  	    //Calling LoginTest Method
  		LoginTest();
  	    //DashboardPage
  	  	DashboardPage dp1 = new DashboardPage(driver);
  	    //Username
  		dp1.getUserName();
  		//Thread.sleep(8000);
  		//Devices Tab
  		dp1.getDevicesTab();
  	    //DevicesPage
  		DevicesPage dp2 = new DevicesPage(driver);
  		//First Device
  	    dp2.getFirstDevice();
  	    //DeviceDataPage
  	    DeviceDataPage dp3 = new DeviceDataPage(driver);
  	    Thread.sleep(4000);
  	    //Settings Icon
  	    dp3.getSettingsIcon();
  		//DeviceSettingsPage
  	    DeviceSettingsPage dp4 = new DeviceSettingsPage(driver);
  	    driver.navigate().refresh();
  	    Thread.sleep(8000);
  	    // DeviceDelete
  	    dp4.getDeviceDelete();
  	    //Delete Button Popup
  	    dp4.getDeleteButtonPopup();
  	    Thread.sleep(8000);
  	    //Toaster Message
  	    //dp3.getToasterMessage();
  	    driver.navigate().refresh();
	    //Calling Logout Method
		LogoutTest();     
    }
    
    //TestCase: Group Creation
    @Test(priority=35)
    public void GroupCreationTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Group Creation Test");
  		log.info("Test Case: Group Creation Test");
  		
  	    //Calling LoginTest Method
  		LoginTest();
  	    //DashboardPage
  	  	DashboardPage dp1 = new DashboardPage(driver);
  	    //Username
  		dp1.getUserName();
  	    //Devices Tab
  		dp1.getDevicesTab();
  		//DashboardPage
  	  	DevicesPage dp2 = new DevicesPage(driver);		 		
  		//Thread.sleep(8000);
  		//Create Group Tab
  		dp2.getCreateGroup();
  		//Group Name
  		dp2.getGroupName();
  		//Save Group  		
  		Thread.sleep(4000);
  		dp2.getSaveGroupName();
  		Thread.sleep(4000);
  		 //Username
  		dp1.getUserName();
  		//driver.navigate().refresh();
  	    //Calling Logout Method
  	    //LogoutTest();
    }
    
    
    //TestCase: Group Update
    @Test(enabled=false,priority=17)
    public void GroupUpdateGroupNameTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Group Updation Test");
  		log.info("Test Case: Group Updation Test");
  		
  	    //Calling LoginTest Method
  		LoginTest();
  		//Thread.sleep(4000);
  		//DashboardPage
  		DashboardPage dp1 = new DashboardPage(driver);
  	    //Username
  		dp1.getUserName();
  	  	//Devices Tab
  		dp1.getDevicesTab();
  		Thread.sleep(8000);
  		//Devices Page
  		DevicesPage dp2 = new DevicesPage(driver);		 		
  		//Create Group Tab
  		dp2.getCreateGroup();
  		//Group Name
  		dp2.getGroupName();
  		//Save Group
  		dp2.getSaveGroupName();
  		Thread.sleep(20000);
  		dp2.UpdateGroupName();
    }
    
    //TestCase: Group Delete
    @Test(enabled=false,priority=18)
    public void GroupDeleteGroupTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Group Delete Test");
  		log.info("Test Case: Group Delete Test");
  	    //Calling LoginTest Method
  		LoginTest();
  		//Thread.sleep(4000);
  		//DashboardPage
  		DashboardPage dp1 = new DashboardPage(driver);
  	    //Username
  		dp1.getUserName();
  	    //Devices Tab
  		dp1.getDevicesTab();
  		Thread.sleep(8000);
  		//DevicesPage
  		DevicesPage dp2 = new DevicesPage(driver);		 		  		
  		//Create Group Tab
  		dp2.getCreateGroup();
  		//Group Name
  		dp2.getGroupName();
  		//Save Group
  		dp2.getSaveGroupName();
  		Thread.sleep(10000);
  		dp2.DeleteGroupName();
  		Thread.sleep(10000);
  	    //Calling Logout Method
  	    LogoutTest();	
    }
    
    //Test Case: OrgSettings Tab Test
    @Test(priority=19)
    public void OrgSettingTabTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Org setting Tab Test");
  		log.info("Test Case: Org setting Tab Test");
  		
  	    //Calling LoginTest Method
  		LoginTest();
  		//Thread.sleep(4000);
  		//DashboardPage
  		DashboardPage dp1 = new DashboardPage(driver);
  	    //Username
  		dp1.getUserName();
  		Thread.sleep(6000);
  		//OrgSettingsPage
  		OrgSettingsPage orgSettings = new OrgSettingsPage(driver);
  		//OrgSettingsTab
  		orgSettings.getOrgSettingsTab();
  		dp1.getUserName();
  	    //Calling Logout Method
  	    LogoutTest(); 	
    }
    
    //Test Case: Users Tab Click Test
    @Test(priority=20)
    public void OrgSettingUserTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Users Tab Click Test");
  		log.info("Test Case: Users Tab Click Test");
  	    //Calling LoginTest Method
  		LoginTest();
  		//Thread.sleep(4000);
  		//DashboardPage
  		DashboardPage dp1 = new DashboardPage(driver);
  	    //Username
  		dp1.getUserName();
  		Thread.sleep(6000);
  		//Org Settings
  		OrgSettingsPage orgSettings = new OrgSettingsPage(driver);
  	    //OrgSettingsTab
  		orgSettings.getOrgSettingsTab();
  		//Thread.sleep(5000);
  		//Users Tab
  		Thread.sleep(5000);
  		orgSettings.getUsersTab();
  		Thread.sleep(5000);
  		dp1.getUserName();
  	    //Calling Logout Method
  	    LogoutTest();
    }
    
    //Test Case: User Invite
    @Test(priority=21)
    public void OrgSettingInviteUserTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: User Invite");
  		log.info("Test Case:User Invite");
  	    //Calling LoginTest Method
  		LoginTest();
  		//Thread.sleep(4000);
  		//DashboardPage
  		DashboardPage dp1 = new DashboardPage(driver);
  	    //Username
  		dp1.getUserName();
  		Thread.sleep(6000);
  	    //Org Settings  	
  		OrgSettingsPage orgSettings = new OrgSettingsPage(driver);
  	    //OrgSettingsTab
  		orgSettings.getOrgSettingsTab();
  		Thread.sleep(2000);
  		//Invite User
  		orgSettings.Inviteuser();
  		Thread.sleep(9000);
  		dp1.getUserName();
  	    //Calling Logout Method
  	    LogoutTest();
    }
    
    //Test Case: User Block
    @Test(enabled=false,priority=22)
    public void OrgSettingBlockUserTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Org setting Block User Test");
  		log.info("Test Case: Org setting Block User Test");
  	    //Calling LoginTest Method
  		LoginTest();
  		//Thread.sleep(4000);
  		//DashboardPage
  		DashboardPage dp1 = new DashboardPage(driver);
  	    //Username
  		dp1.getUserName();
  		Thread.sleep(6000);
  	    //Org Settings
  		OrgSettingsPage orgSettings = new OrgSettingsPage(driver);
  	    //OrgSettingsTab
  		orgSettings.getOrgSettingsTab();
  		Thread.sleep(3000);
  		//Block User
  		orgSettings.blockUser();
  		Thread.sleep(5000);
  		dp1.getUserName();
  	    //Calling Logout Method
  	    LogoutTest();
    }
    
    //Test Case: User Delete
    @Test(enabled=false,priority=23)
    public void OrgSettingDeleteUserTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Org setting Delete User Test");
  		log.info("Test Case: Org setting Delete User Test");
  	    //Calling LoginTest Method
  		LoginTest();
  		//Thread.sleep(4000);
  		//DashboardPage
  		DashboardPage dp1 = new DashboardPage(driver);
  	    //Username
  		dp1.getUserName();
  		Thread.sleep(6000);
  		//OrgSettings
  		OrgSettingsPage orgSettings = new OrgSettingsPage(driver);
  	    //OrgSettingsTab
  		orgSettings.getOrgSettingsTab();
  		Thread.sleep(3000);
  		//Delete User
  		orgSettings.deleteUser();
  		Thread.sleep(5000);
  		dp1.getUserName();
  	    //Calling Logout Method
  	    LogoutTest();
    }
    
    
    //Test Case: Org Settings Alert Option
    @Test(priority=24)
    public void OrgSettingAlertOptionTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Org setting Alert option Test");
  		log.info("Test Case: Org setting Alert option Test");
  	    //Calling LoginTest Method
  		LoginTest();
  		//Thread.sleep(4000);
  		//DashboardPage
  		DashboardPage dp1 = new DashboardPage(driver);
  	    //Username
  		dp1.getUserName();
  		Thread.sleep(6000);
  		//OrgSettings
  		OrgSettingsPage orgSettings = new OrgSettingsPage(driver);
  		//Alert
  		orgSettings.alertOption();
  		dp1.getUserName();
  	    //Calling Logout Method
  	    LogoutTest();
    }

    //Test Case: Org Settings: Billing Option
    @Test(priority=25)
    public void OrgSettingBillingOptionTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Org setting Billing Option Test");
  		log.info("Test Case: Org setting Billing Option Test");
  	    //Calling LoginTest Method
  		LoginTest();
  		//Thread.sleep(4000);
  		//DashboardPage
  		DashboardPage dp1 = new DashboardPage(driver);
  	    //Username
  		dp1.getUserName();
  		Thread.sleep(6000);
  		//Org Settings
  		OrgSettingsPage orgSettings = new OrgSettingsPage(driver);
  		//Billing Options
  		orgSettings.billingOption();
  		dp1.getUserName();
  	    //Calling Logout Method
  	    LogoutTest();
    }
    
    //Test Case: Org Settings: Role Option
    @Test(enabled=false,priority=26)
    public void OrgSettingRoleOptionTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Org setting Role Option Test");
  		log.info("Test Case: Org setting Role Option Test");
  	    //Calling LoginTest Method
  		LoginTest();
  		//Thread.sleep(4000);
  		//DashboardPage
  		DashboardPage dp1 = new DashboardPage(driver);
  	    //Username
  		dp1.getUserName();
  		Thread.sleep(6000);
  		//OrgSettings
  		OrgSettingsPage orgSettings = new OrgSettingsPage(driver);
  		//Role Option
  		orgSettings.roleOption();
  		dp1.getUserName();
  	    //Calling Logout Method
  	    LogoutTest();
    }
    
    //Test Case: Device Settings: Device Reboot
    @Test(priority=27)
    public void DeviceSettingRebootDeviceTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Device Setting Tab Test");
  		log.info("Test Case: Device Setting Tab Test");
  	    //Calling LoginTest Method
  		LoginTest();
  		//Thread.sleep(4000);
  		DashboardPage dp1 = new DashboardPage(driver);	
  	    //Username
  		dp1.getUserName();
  		Thread.sleep(6000);
  		dp1.getDevicesTab();
  		//Devices Settings Page
  		DeviceSettingsPage dsp = new DeviceSettingsPage(driver);
  		Thread.sleep(3000);
  		//First Device
  		dsp.ClickOnDevice();
  		//Device Settings
  		dsp.ClickOnDeviceSettings();
  		Thread.sleep(8000);
  		//reboot Device
  		dsp.RebootDevice();
  		Thread.sleep(3000);
  		dp1.getUserName();
  	    //Calling Logout Method
  	    LogoutTest();
    }
    //Test Case: Cloud Update Interval Test
    @Test(priority=28)
    public void DeviceSettingCloudUpdateIntervalTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Cloud Update Interval Test");
  		log.info("Test Case: Cloud Update IntervalTest");
  	    //Calling LoginTest Method
  		LoginTest();
  		//Thread.sleep(4000);
  		DashboardPage dp1 = new DashboardPage(driver);	
  	    //Username
  		dp1.getUserName();
  		Thread.sleep(6000);
  		dp1.getDevicesTab(); 	
  		//Device Settings Page
  		DeviceSettingsPage dsp = new DeviceSettingsPage(driver);
  		Thread.sleep(4000);
  		//Device
  		dsp.ClickOnDevice();
  		Thread.sleep(2000);
  		//Device Settings
  		dsp.ClickOnDeviceSettings();
  		Thread.sleep(8000);
  		//Cloud Update Interval
  		dsp.CloudUpdateInterval();
  		dp1.getUserName();
  	    //Calling Logout Method
  	    LogoutTest();
    }
    
    //Test Case: Cloud Update Interval Same As Battery Test
    @Test(priority=29)
    public void DeviceSettingCloudUpdateIntervalSameAsBatteryTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Cloud Update Interval Same As Battery Test");
  		log.info("Test Case: Cloud Update Interval Same As Battery Test");
  	    //Calling LoginTest Method
  		LoginTest();
  		//Thread.sleep(4000);
  		DashboardPage dp1 = new DashboardPage(driver);	
  	    //Username
  		dp1.getUserName();
  		Thread.sleep(6000);
  		dp1.getDevicesTab();
        //Device Settings Page
  		DeviceSettingsPage dsp = new DeviceSettingsPage(driver);
  		//Device
  		dsp.ClickOnDevice();
  		Thread.sleep(2000);
  		//Device Settings
  		dsp.ClickOnDeviceSettings();
  		Thread.sleep(3000);
  		//Cloud Update Interval Same as Battery 	
  		dsp.CloudUpdateIntervalSameAsBattery();
  		dp1.getUserName();
  		//Calling Logout Method
  	    LogoutTest();
    }
    
    //Test Case: Operating Hours Test 
    @Test (priority=30)
    public void DeviceSettingOperatingHourTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Operating Hours Test");
  		log.info("Test Case: Operating Hours Test");
  	    //Calling LoginTest Method
  		LoginTest();
  		//Thread.sleep(4000);
  		DashboardPage dp1 = new DashboardPage(driver);	
  	    //Username
  		dp1.getUserName();
  		Thread.sleep(6000);
  		dp1.getDevicesTab();
  		//Device Settings Page
  		DeviceSettingsPage dsp = new DeviceSettingsPage(driver);
  		//Device
  		dsp.ClickOnDevice();
  		Thread.sleep(2000);
  		//Device Settings
  		dsp.ClickOnDeviceSettings();
  		Thread.sleep(3000);
  		//Operating Hours
  		dsp.operatingHour();
  		dp1.getUserName();
  		//Calling Logout Method
  	    LogoutTest();
    }
    
    //Test Case: Firmware Test
    @Test (priority=31)
    public void DeviceSettingFirmwareTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Firmware Test");
  		log.info("Test Case: Firmware Test");
  	    //Calling LoginTest Method
  		LoginTest();
  		//Thread.sleep(4000);
  		DashboardPage dp1 = new DashboardPage(driver);	
  	    //Username
  		dp1.getUserName();
  		Thread.sleep(6000);
  		//Devices Tab
  		dp1.getDevicesTab();
  		//Device Settings Page
  		DeviceSettingsPage dsp = new DeviceSettingsPage(driver);
  		//Device
  		dsp.ClickOnDevice();
  		Thread.sleep(2000);
  		//Device Settings
  		dsp.ClickOnDeviceSettings();
  		Thread.sleep(3000);
  		//Upload Firmware
  		dsp.UploadFirmware();
  		dp1.getUserName();
  		//Calling Logout Method
  	    LogoutTest();
    }
    
    //Test Case: Export-Device Settings Export
    @Test(priority=32)
    public void DeviceSettingExportTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Device Settings Export");
  		log.info("Test Case: Device Settings Export");
  	    //Calling LoginTest Method
  		LoginTest();
  		//Thread.sleep(4000);
  		DashboardPage dp1 = new DashboardPage(driver);	
  	    //Username
  		dp1.getUserName();
  		Thread.sleep(6000);
  		//Devices Tab
  		dp1.getDevicesTab();
  		//Device Settings Page
  		DeviceSettingsPage dsp = new DeviceSettingsPage(driver);
  		//Device
  		dsp.ClickOnDevice();
  		Thread.sleep(3000);
  		//Export Download
  		dsp.exportDowload();
  		Thread.sleep(3000);
  		//Export Outside Click
  		dsp.ExportOutsideClick();
  		dp1.getUserName();
  		//Calling Logout Method
  	    LogoutTest();
    }
    
    //Test Case: Export-Device Settings Export Email
    @Test(priority=33)
    public void DeviceSettingExportEmailTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Export-Email");
  		log.info("Test Case: Export-Email");
  	    //Calling LoginTest Method
  		LoginTest();
  		//Thread.sleep(4000);
  		DashboardPage dp1 = new DashboardPage(driver);	
  	    //Username
  		dp1.getUserName();
  		Thread.sleep(6000);
  		//Devices Tab
  		dp1.getDevicesTab();
  	    //Device Settings Page
  		DeviceSettingsPage dsp = new DeviceSettingsPage(driver);
  		//Device
  		dsp.ClickOnDevice();
  		Thread.sleep(3000);
  		//Export 
  		dsp.exportDowloadEmail();
  	    //Export Outside Click
  		dsp.ExportOutsideClick();
  		dp1.getUserName();
  		//Calling Logout Method
  	    LogoutTest();
    }
    
    //Test Case: Device Activity Logs
    @Test(priority=34)
    public void DeviceSettingActivityLOgTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Device Activity log Test");
  		log.info("Test Case: Device Activity log Test");
  	    //Calling LoginTest Method
  		LoginTest();
  		//Thread.sleep(4000);
  		DashboardPage dp1 = new DashboardPage(driver);	
  	    //Username
  		dp1.getUserName();
  		Thread.sleep(6000);
  		//Devices Tab
  		dp1.getDevicesTab();
  	    //Device Settings Page
  		DeviceSettingsPage dsp = new DeviceSettingsPage(driver);  		
  		//Device
  		dsp.ClickOnDevice();
  		Thread.sleep(3000);
  		//Device Activity Logs
  		dsp.ActivityLog();
  		dp1.getUserName();
  		//Calling Logout Method
  	    LogoutTest();
    }
    
    /*
    //TestCase: Create Pwwertrain Test
    @Test(priority=35)
    public void CreatePowertrainTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Create Powertrain Test");
  		log.info("Test Case: Create Powertrain Test");
  		
  		driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(90));
  		
  	    //Calling LoginTest Method
  		LoginTest();
  		//DashboardPage
  	  	DashboardPage dp1 = new DashboardPage(driver);
  	    //Username
  		dp1.getUserName();  		
  		//Thread.sleep(8000);
  		//Devices Tab
  		dp1.getDevicesTab();
  	    //Username
  		dp1.getUserName();
  		//DevicesPage
  		DevicesPage dp2 = new DevicesPage(driver);
  		//Thread.sleep(4000);
  		//Create Device Button
  		dp2.getCreateDeviceButton();
  		//Select Powertrain
  		dp2.getSelectPowertrainCreation();
  		//Powertrain Name
  		dp2.getPowertrainName();
  		//Powertrain RPM Min
  		dp2.getPowertrainRPM_min();
  	    //Powertrain RPM Max
  		dp2.getPowertrainRPM_max();
  		//PrimeMover Name
  		dp2.getPrimeMoverName();
  		//Prime Mover Serial No
  		dp2.getPrimeMoverSerialNo();
  		//PrimeMover Component Class Dropdown
  		dp2.getPMComponentClassDropdown();
  		//PrimeMover Component Class Select
  		dp2.getPMComponentClassSelect();
  		//PrimeMover Component Sub Class Dropdown
  		dp2.getPMComponentSubClassDropdown();
  	    //PrimeMover Component Sub Class Select
  		dp2.getPMComponentSubClassSelect();
  		//PrimeMover Component Manufacturer
  		dp2.getPMComponentManufacturerDropdown();
  		//PrimeMover Component Manufacturer Select
  		dp2.getPMComponentManufacturerSelect();
  		//Save Button
  		dp2.getSaveButton();
  		Thread.sleep(4000);
  		driver.navigate().refresh();
  	    //Calling Logout Method
  		LogoutTest();  		
    }
    
    //TestCase: Update Powertrain Test
    @Test(priority=36)
    public void UpdatePowertrainTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Update Powertrain Test");
  		log.info("Test Case: Update Powertrain Test");
  		
  		driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(90));
  		
  	    //Calling LoginTest Method
  		LoginTest();
  		//DashboardPage
  	  	DashboardPage dp1 = new DashboardPage(driver);
  	    //Username
  		dp1.getUserName();  		
  		//Thread.sleep(8000);
  		//Devices Tab
  		dp1.getDevicesTab();
  	    //Username
  		dp1.getUserName();
  		//DevicesPage
  		DevicesPage dp2 = new DevicesPage(driver);
  		//Thread.sleep(4000);
  		//Create Device Button
  		dp2.getCreateDeviceButton();
  		//Select Powertrain
  		dp2.getSelectPowertrainCreation();
  		//Powertrain Name
  		dp2.getPowertrainName_Update();
  		//Powertrain RPM Min
  		dp2.getPowertrainRPM_min_Update();  	    
  		//Save Button
  		dp2.getSaveButton();
  		Thread.sleep(4000);
  		driver.navigate().refresh();
  	    //Calling Logout Method
  		LogoutTest();  		
    }
    

    //Activity Logs
    @Test
    public void ActivityLogTabTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: ActivityLog Test");
  		log.info("Test Case: ActivityLog Test");
  	    //Calling LoginTest Method
  		LoginTest();
  		//Thread.sleep(4000);
   		ActivityLogsPage activityLogsPage = new ActivityLogsPage(driver);
  	    //Activity log Tab
  		activityLogsPage.getActivityLogTab();
  		Thread.sleep(6000);
   	    LogoutTest();
    }
    @Test
    public void ActivityLogCreateSearchTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case:  Activity Log create Search  Test");
  		log.info("Test Case: Activity Log create Search Test");
  	    //Calling LoginTest Method
  		LoginTest();
  		Thread.sleep(3000);
   		ActivityLogsPage activityLogsPage = new ActivityLogsPage(driver);
  	    //activityLogSearch
  		activityLogsPage.activityLogCreateSearch();
  		Thread.sleep(3000);
   	    LogoutTest();
    }
    @Test
    public void ActivityLogUpdateSearchTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case:  Activity Log update Search  Test");
  		log.info("Test Case: Activity Log update Search Test");
  	    //Calling LoginTest Method
  		LoginTest();
  		Thread.sleep(3000);
   		ActivityLogsPage activityLogsPage = new ActivityLogsPage(driver);
  	    //activityLogSearch
  		activityLogsPage.activityLogUpdateSearch();
  		Thread.sleep(3000);
   	    LogoutTest();
    }
    @Test
    public void ActivityLogDeleteSearchTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case:  Activity Log update Search  Test");
  		log.info("Test Case: Activity Log update Search Test");
  	    //Calling LoginTest Method
  		LoginTest();
  		Thread.sleep(3000);
   		ActivityLogsPage activityLogsPage = new ActivityLogsPage(driver);
  	    //activityLogSearch
  		activityLogsPage.activityLogDeleteSearch();
  		Thread.sleep(3000);
   	    LogoutTest();
    }
    @Test
    public void ActivityLogRuleSearchTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case:  Activity Log update Search  Test");
  		log.info("Test Case: Activity Log update Search Test");
  	    //Calling LoginTest Method
  		LoginTest();
  		Thread.sleep(3000);
   		ActivityLogsPage activityLogsPage = new ActivityLogsPage(driver);
  	    //activityLogSearch
  		activityLogsPage.activityLogRuleSearch();
  		Thread.sleep(3000);
   	    LogoutTest();
    }
    */
// Changes needed   
//    @Test
//    public void ActivityLogCalenderTest() throws IOException, InterruptedException 
//    {
//    	System.out.println("Test Case:  Activity Log update Search  Test");
//  		log.info("Test Case: Activity Log update Search Test");
//  	    //Calling LoginTest Method
//  		LoginTest();
//  		Thread.sleep(3000);
//   		ActivityLogsPage activityLogsPage = new ActivityLogsPage(driver);
//  	    //activityLogSearch
//  		activityLogsPage.activityLogCalenderSearch();
//  		Thread.sleep(3000);
//   	    LogoutTest();
//    }
   
 //Manual narrowband
    @Test
    public void ManualNarrowbandTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: create manual narrowband Test");
  		log.info("Test Case: create manual narrowband Test");
  		LoginTest();
  		//DevicesPage
  		Thread.sleep(3000);
  		DashboardPage dash = new DashboardPage(driver);
  		DevicesPage dp1 = new DevicesPage(driver);
  		dash.getDevicesTab();
  		Thread.sleep(5000);
  		//First Device
  	    dp1.getFirstDevice();
  	    Thread.sleep(6000);
  	    //DeviceDataPage
  	   NarrowbandPage nw = new NarrowbandPage(driver);
  	    
        nw.AddSensor();
        Thread.sleep(3000);
  	    nw.selectNarrowband();
  	    Thread.sleep(8000);
  	    nw.ManualNarrowbandForm();
  		LogoutTest();
    }
    /*
    @Test
    public void ManualNarrowbandDeleteTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Manual narrowband delete Test");
  		log.info("Test Case: Manual narrowband delete Test");
  		LoginTest();
  		//DevicesPage
  		Thread.sleep(3000);
  		DashboardPage dash = new DashboardPage(driver);
  		DevicesPage dp1 = new DevicesPage(driver);
  		dash.getDevicesTab();
  		Thread.sleep(5000);
  		//First Device
  	    dp1.getFirstDevice();
  	    Thread.sleep(10000);
  	    //DeviceDataPage
  	   NarrowbandPage nw = new NarrowbandPage(driver);
  	    
        nw.deleteNarrowband();
        Thread.sleep(3000);
  		LogoutTest();
    }
    //Autoconfig
    @Test

    public void AutomaticNarrowbandTest() throws IOException, InterruptedException

    public void AutomaticNarrowbandTest() throws IOException, InterruptedException 

    {
    	System.out.println("Test Case: Automatic narrowband create Test");
  		log.info("Test Case: Automatic narrowband create Test");
  		LoginTest();
  		//DevicesPage
  		Thread.sleep(3000);
  		DashboardPage dash = new DashboardPage(driver);
  		DevicesPage dp1 = new DevicesPage(driver);
  		dash.getDevicesTab();
  		Thread.sleep(5000);
  		//First Device
  	    dp1.getFirstDevice();
  	    Thread.sleep(6000);
  	    //DeviceDataPage
  	   NarrowbandPage nw = new NarrowbandPage(driver);
        nw.AddSensor();
        Thread.sleep(3000);
        nw.selectNarrowband();
        Thread.sleep(8000);
  	    nw.AutoNArrowband();
  	    Thread.sleep(2000);
  		LogoutTest();
    }
    @Test

    public void AutomaticNarrowbandValueEditTest() throws IOException, InterruptedException

    public void AutomaticNarrowbandValueEditTest() throws IOException, InterruptedException 

    {
    	System.out.println("Test Case: Automatic narrowband value edit Test");
  		log.info("Test Case: Automatic narrowband value edit Test");
  		LoginTest();
  		//DevicesPage
  		Thread.sleep(3000);
  		DashboardPage dash = new DashboardPage(driver);
  		DevicesPage dp1 = new DevicesPage(driver);
  		dash.getDevicesTab();
  		Thread.sleep(5000);
  		//First Device
  	    dp1.getFirstDevice();
  	    Thread.sleep(6000);
  	    //DeviceDataPage
  	   NarrowbandPage nw = new NarrowbandPage(driver);
        nw.AddSensor();
        Thread.sleep(3000);
        nw.selectNarrowband();
        Thread.sleep(10000);
  	    nw.AutoNarrowbandValueEdit();
  	    Thread.sleep(2000);
  		LogoutTest();
    }
    @Test

    public void AutomaticNarrowbandClassEditTest() throws IOException, InterruptedException

    public void AutomaticNarrowbandClassEditTest() throws IOException, InterruptedException 

    {
    	System.out.println("Test Case: Automatic narrowband class edit Test");
  		log.info("Test Case: Automatic narrowband class edit Test");
  		LoginTest();
  		Thread.sleep(3000);
  		DashboardPage dash = new DashboardPage(driver);
  		DevicesPage dp1 = new DevicesPage(driver);
  		dash.getDevicesTab();
  		Thread.sleep(5000);
  		//First Device
  	    dp1.getFirstDevice();
  	    Thread.sleep(6000);

  	   NarrowbandPage nw = new NarrowbandPage(driver); 	   

  	   NarrowbandPage nw = new NarrowbandPage(driver); 	    

        nw.AddSensor();
        Thread.sleep(3000);
        nw.selectNarrowband();
        Thread.sleep(10000);
  	    nw.AutoNArrowbandClassEdit();
  	    Thread.sleep(2000);
  		LogoutTest();
    }
    @Test

    public void AutomaticNarrowbandDeleteTest() throws IOException, InterruptedException
    public void AutomaticNarrowbandDeleteTest() throws IOException, InterruptedException 

    {
    	System.out.println("Test Case: Automatic narrowband delete Test");
  		log.info("Test Case: Automatic narrowband delete Test");
  		LoginTest();
  		//DevicesPage
  		Thread.sleep(3000);
  		DashboardPage dash = new DashboardPage(driver);
  		DevicesPage dp1 = new DevicesPage(driver);
  		dash.getDevicesTab();
  		Thread.sleep(5000);
  		//First Device
  	    dp1.getFirstDevice();
  	    Thread.sleep(6000);

  	    NarrowbandPage nw = new NarrowbandPage(driver); 	   

  	    NarrowbandPage nw = new NarrowbandPage(driver); 	    

        nw.AddSensor();
        Thread.sleep(3000);
        nw.selectNarrowband();
        Thread.sleep(10000);
  	    nw.AutoNarrowbandDelete();
  	    Thread.sleep(2000);
  		LogoutTest();
    }

*/
	@AfterTest
	public void AfterTest()
	{
		driver.quit();
		System.out.println("Browser Closed");
		log.info("Test is Finished and Browser is closed");
		driver=null;	
	}
    
}
