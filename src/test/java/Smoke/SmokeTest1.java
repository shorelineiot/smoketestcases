package Smoke;

import java.io.File;
import java.io.IOException;
import java.time.Duration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.ViewName;

import PageObjects.AlarmsPage;
import PageObjects.DashboardPage;
import PageObjects.DeviceDataPage;
import PageObjects.DeviceSettingsPage;
import PageObjects.DevicesPage;
import PageObjects.LoginPage;
import PageObjects.OrgSettingsPage;
import PageObjects.RulesPage;
import Resources.base;

public class SmokeTest1 extends base{
	
public static Logger log =LogManager.getLogger(SmokeTest.class.getName());
	
	//Declare WebDriver  Globally
    public WebDriver driver;
    
    ExtentReports extent;
    ExtentTest test;
    
    /*Login Method*/
    public void LoginTest() throws IOException, InterruptedException
    {
    	//Login Screen
    	LoginPage lp = new LoginPage(driver);
    	//Email Address
    	lp.getEmailAddress();	
    	//RememberMe Checkbox
    	lp.getRememberMeCheckbox();
    	//NextButton
    	lp.getNextButton();
    	//Password
    	lp.getPassword();
    	Thread.sleep(3000);
    	//LoginButton
    	lp.getLoginButton();
    	Thread.sleep(8000);
    }
    
    /*Logout Method*/
    public void LogoutTest() throws InterruptedException
    {
    	//DashboardPage
  		DashboardPage dp1 = new DashboardPage(driver);		
		//Username
		dp1.getUserName().click();
		//User Logout
		dp1.getUserLogout();
		driver.navigate().refresh();  	
    }
    
    @BeforeTest
	public void BeforeTest() throws IOException, InterruptedException
	{
		System.out.println("Execution started for class 'SmokeTest'");
		log.info("Execution started for class 'SmokeTest'");
		
		driver=InitializeDriver();			
		driver.manage().window().maximize();
		System.out.println("Initialised Driver and Maximised the Browser Window");
		log.info("Initialised Driver and Maximised the Browser Window");
		
		//Launch
		driver.get(prop.getProperty("testv2url"));
		System.out.println("Launched Admin Portal:"+driver.getCurrentUrl());
		log.info("Launched Admin Portal:"+driver.getCurrentUrl());
				
	}

    @Test
    public void TestCases() throws InterruptedException, IOException
    {
    	//Extent Report
    	String path1 = System.getProperty("user.dir")+"/reports/AutomationReport.html";//html file will  be generated
		ExtentSparkReporter reporter = new ExtentSparkReporter(path1).viewConfigurer().viewOrder().as(new ViewName[] {ViewName.DASHBOARD, ViewName.TEST, ViewName.CATEGORY}).apply();
		//Json
		final File CONF = new File("extentconfig.json");
		reporter.loadJSONConfig(CONF);
		ExtentReports extent = new ExtentReports();
		extent.attachReporter(reporter);
		extent.setSystemInfo("QA", "Amey Deshpande");
		
		/*Login Cases*/
		//1) User Login
    	System.out.println("Test Case: User Login Test");
  		log.info("Test Case: User Login Test");
  		  		
  		driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(90));
  		
  		ExtentTest test = extent.createTest("LoginTest").assignCategory("User Login");
  		
  		//Calling LoginTest Method
  		LoginTest();
		//DashboardPage
  		DashboardPage dp1 = new DashboardPage(driver);		
		//Username
		dp1.getUserName();		
		extent.flush();
		
		//2) User Logout
		System.out.println("Test Case: User Logout Test");
  		log.info("Test Case: User Logout Test");
  		
  		driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(90));
  		ExtentTest test1 = extent.createTest("LogoutTest").assignCategory("User Login");
  		//Calling Logout Test Method
  		LogoutTest();	  		
  		extent.flush();
  		
  		/*Alarms Cases*/
  		//3)Alarms Tab
  		System.out.println("Test Case: Alarm Tab click");
  		log.info("Test Case: Alarm Tab click");
  	    
  		ExtentTest test2 = extent.createTest("Alarms Tab Test").assignCategory("Alarms");
  		
  		//Calling LoginTest Method
  		LoginTest();
  	    //DashboardPage
  		//DashboardPage dp1 = new DashboardPage(driver);		
		//Username
		dp1.getUserName();	
  		//AlarmsPage
  		AlarmsPage ap = new AlarmsPage(driver);
  		//Alarms Tab
  		ap.getAlarmsTab();
  	    //Calling Logout Test Method
  		LogoutTest();  		
  		extent.flush();
  		
  		//4)Alarms clear Filter
  		System.out.println("Test Case: Alarms Page Clear filter Test");
  		log.info("Test Case: Alarms page Clear filter Test"); 		
  		
  		ExtentTest test3 = extent.createTest("Alarms Clear Filter").assignCategory("Alarms");
  		
  	    //Calling LoginTest Method
  		LoginTest();
  	    //DashboardPage
  		//DashboardPage dp1 = new DashboardPage(driver);		
		//Username
		dp1.getUserName();
		//Alarms Tab
  		ap.getAlarmsTab();
		//Clear All Alarms
  		ap.clearAllAlarms();
  		//driver.navigate().refresh();
  		extent.flush();
  		
  		//4) Alarms-Severity Filter
  		System.out.println("Test Case: Alarms Select Severity Filter Test");
  		log.info("Test Case: Alarms Select Severity Filter Test");
  		
  		ExtentTest test4 = extent.createTest("Alarms Severity Filter Test").assignCategory("Alarms");
  		
  		//loginTestCase();
  	    //DashboardPage
  	  	//DashboardPage dp1 = new DashboardPage(driver);		 		
  		//Thread.sleep(8000);		
		//Username
		dp1.getUserName();
  		//Devices Tab
  		dp1.getDevicesTab();
  		//Thread.sleep(6000);
  		//Alarms  Page
  		//AlarmsPage ap = new AlarmsPage(driver);
  		//Alarms Tab
  		ap.getAlarmsTab();
  		Thread.sleep(2000);
  		//Severity Filter
  		ap.SelectSeverityFilter();
  		driver.navigate().refresh();
  		Thread.sleep(4000);
  		//Calling Logout Method
  		LogoutTest();
  		//driver.navigate().refresh();
  		extent.flush();
  		
  	    //5) Alarms-Fixed Alarms
  		System.out.println("Test Case: Alarms Fixed Alarm Test");
  		log.info("Test Case: Alarms Fixed Alarm Test");
  		
  		ExtentTest test5 = extent.createTest("Alarms fixed Alarms").assignCategory("Alarms");
  		//Calling Login Method
  		LoginTest();
  		//Thread.sleep(6000);
  	    //DashboardPage
  	  	//DashboardPage dp1 = new DashboardPage(driver);		
		//Username
		dp1.getUserName();
  	  	//Devices Tab
  		//dp1.getDevicesTab();
  		//Alarms Page
  		//AlarmsPage ap = new AlarmsPage(driver);
  		//Alarms Tab
        Thread.sleep(3000);
  		ap.getAlarmsTab();
  		//Fixed Alarm
  		ap.fixedAlarms();
  	    //Calling Logout Method
  		LogoutTest();
  		//driver.navigate().refresh();
  		//Thread.sleep(4000);
  		extent.flush();
  		
  	    //6) Alarms-False Alarms
  		System.out.println("Test Case: Alarms Page False Alarm Test");
  		log.info("Test Case: Alarms Page False Alarm Test");
  	    
  		ExtentTest test6 = extent.createTest("Alarms false Alarms").assignCategory("Alarms");
  		
  		//Calling Login Method
  		LoginTest();
  		//Thread.sleep(6000);
  	    //DashboardPage
  		//DashboardPage dp1 = new DashboardPage(driver);		
		//Username
		dp1.getUserName();
  		//Alarms Page
  		//AlarmsPage ap = new AlarmsPage(driver);
  		//Alarms Tab
  		Thread.sleep(3000);
  		ap.getAlarmsTab();  		
  		//False Alarm
  		ap.falseAlarms();
  		//driver.navigate().refresh();
  		
  	    //7) Alarms-Color
  		System.out.println("Test Case: Alarms Color");
  		log.info("Test Case: Alarms Color");
  	    
  		ExtentTest test7 = extent.createTest("Alarms Color").assignCategory("Alarms");
  		
  		//Calling Login Method
  		//LoginTest();
  		//Thread.sleep(6000);
  		//AlarmsPage ap = new AlarmsPage(driver);
  		//ap.getAlarmsTab();
  		//Thread.sleep(6000);
  		//Crtical  Alrm Text
  		ap.getCriticalAlarms();
  		//Critical Alarms Color
  		ap.getCriticalAlarmsColor();
  	    //Moderate  Alrm Text
  		ap.getModerateAlarms();
  		//Moderate Alarms Color
  		ap.getModerateAlarmsColor();
  	    //High  Alrm Text
  		ap.getHighAlarms();
  		//High Alarms Color
  		ap.getHighAlarmsColor();
  	    //Low  Alrm Text
  		ap.getLowAlarms();
  		//Critical Alarms Color
  		ap.getLowAlarmsColor();
  	    //Calling Logout Method
  		LogoutTest();
  		extent.flush();
        
  		/*Rules Cases*/
  		//Create Rules-sensor
  		System.out.println("Test Case: Rules Create Sensor");
  		log.info("Test Case: Rules Create");
  	    
  		ExtentTest test8 = extent.createTest("Create Rules").assignCategory("Rules");
  		
  		//Calling Login Method
  		LoginTest();
  		Thread.sleep(6000);
  	    //DashboardPage
  		//DashboardPage dp1 = new DashboardPage(driver);		
		//Username
		dp1.getUserName();
  		//RulesTab
  		dp1.getRulesTab();
  		//RulesPage
  		RulesPage rp = new RulesPage(driver);
  		//Create Rule Button
  		rp.getCreateRuleButton();
  		//Rule Name
  		rp.getRuleNameField();
  		//HoldOffTime
  		rp.getRuleHoldOffTime();
  		//Rule Level dropdown
  		rp.getRuleLevelDropdown();
  		//Rule Level dropdown select
  		rp.getRuleLevelDropdownSelect();
  		//Select Device Dropdown
  		rp.getRuleDeviceDropdown();
  		Thread.sleep(2000);
  		// Select Device
  		rp.getRuleDeviceSelect();
  		Thread.sleep(3000);
  		//Sensor Dropdown
  		rp.getRuleSensorDropdown();
  		Thread.sleep(2000);
  		//SensorDropdownSelect
  		rp.getRuleSensorSelect();
  		Thread.sleep(2000);
  		//condition Select
  		rp.getRuleConditionDropdown();
  		//Condition Select
  		rp.getRuleConditionSelect();
  		//Condition Value
  		rp.getRuleConditionValue();
  		//Next Button
  		rp.getNextButton();
  		//Save Rule
  		Thread.sleep(2000);
  		rp.getSaveRuleButton();
  		Thread.sleep(2000);
  	    //Toaster
  		//rp.getToasterMessageRuleCreate();
  	    //Calling Logout Method
  		LogoutTest();
  	    driver.navigate().refresh();
  	    extent.flush();
  		
  	    //Delete Rules - Sensor
  	    System.out.println("Test Case: Rules Delete Sensor");
		log.info("Test Case: Rules Delete");
	    
		ExtentTest test9 = extent.createTest("Delete Rules").assignCategory("Rules");
		
		//Calling Login Method
		LoginTest();
		//Thread.sleep(6000);
		//DashboardPage
		DashboardPage dp = new DashboardPage(driver);		
		//Username
		dp.getUserName();
		//RulesTab
		dp.getRulesTab();
		//RulesPage
		//RulesPage rp = new RulesPage(driver);
        //Ssarch Rule
		rp.getSearchRuleField();
		Thread.sleep(3000);
		//Delete
		rp.getDeleteSearchRule();
		//Apply Button
		rp.getApplyButtonConfirmation();
		Thread.sleep(3000);
		//Toaster
		//rp.getToasterMessageRuleDelete();
	    //Calling Logout Method
		LogoutTest();
		driver.navigate().refresh();
  		extent.flush();
		
  		//Update Rules - Sensor
  		System.out.println("Test Case: Rules Update Sensor");
  		log.info("Test Case: Rules Update");
  		
  		ExtentTest test10 = extent.createTest("Update Rules").assignCategory("Rules");
  	    
  		//Calling Login Method
  		LoginTest();
  		//Thread.sleep(6000);
  		//DashboardPage
  		//DashboardPage dp = new DashboardPage(driver);
  	    //Username
  		dp.getUserName();
  	    //RulesTab
  		dp.getRulesTab();
  		//RulesPage
  		//RulesPage rp = new RulesPage(driver);
  	    //Ssarch Rule
  		rp.getSearchRuleFieldUpdate();
  		Thread.sleep(3000);
        //First Rule Dropdown
  		rp.getSecondRuleDropdowwn();
  		Thread.sleep(4000);
  		//First Rule Dropdown Select
  		rp.getSecondRuleDropdowwnSelect();
  		//Apply Button
  		rp.getApplyButtonConfirmation();
  		//Toaster
  		rp.getToasterMessageRuleUpdate();
  	    //Calling Logout Method
  		LogoutTest();
		
  		/*Devices Cases*/
  		//Device Tab
  		System.out.println("Test Case: Device Tab Click Test");
  		log.info("Test Case: Device Tab Click Test");
  		 		
  		ExtentTest test11 = extent.createTest("Devices Tab Test").assignCategory("Devices");
  		
  		//Calling LoginTest Method
  		LoginTest();
  		//DashboardPage
  	  	//DashboardPage dp1 = new DashboardPage(driver);
  	    //Username
  		dp1.getUserName();
  		//Thread.sleep(8000);
  		//Devices Tab
  		dp1.getDevicesTab();
  		extent.flush();
  		
  		//Update Device
  		System.out.println("Test Case: Update device details Test");
  		log.info("Test Case: Update device details Test");
  	 
  		ExtentTest test12 = extent.createTest("Update Device Test").assignCategory("Devices");
  		
  		//DevicesPage
  		DevicesPage dp2 = new DevicesPage(driver);
  		//First Device
  	    dp2.getFirstDevice();
  	    //DeviceDataPage
  	    DeviceDataPage dp3 = new DeviceDataPage(driver);
  	    //Settings Icon
  	    dp3.getSettingsIcon();
  	    //DeviceSettingsPage
  	    DeviceSettingsPage dp4 = new DeviceSettingsPage(driver);
  	    // DeviceName
  	    dp4.getDeviceName();
  	    //Save
  	    dp4.getDeviceNameSave();
  	    Thread.sleep(3000);
  	    //ToasterMessage
  	    //dp3.getToasterMessage();
  	    //Calling Logout Method
  		LogoutTest();
  	    extent.flush();	
  		
  	    
  	    //Create Device
  	    System.out.println("Test Case: Create Device Test");
		log.info("Test Case: Create Device Test");
				
		ExtentTest test13 = extent.createTest("Create Device Test").assignCategory("Devices");
		driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(90));
		
	    //Calling LoginTest Method
		LoginTest();
		//DashboardPage
	  	//DashboardPage dp1 = new DashboardPage(driver);
	    //Username
		dp1.getUserName();  		
		//Thread.sleep(8000);
		//Devices Tab
		dp1.getDevicesTab();		
		//DevicesPage
		//DevicesPage dp2 = new DevicesPage(driver);
	    //Username
		dp1.getUserName();
		//Thread.sleep(4000);
		//Create Device Button
		dp2.getCreateDeviceButton();
		//Select Device
		dp2.getSelectDeviceCreation();
		//Device Name
		dp2.getDeviceName();
		//Device Type Dropdown
		dp2.getDeviceTypeDropdown();
		//Device Type Icastsense
		dp2.getDeviceTypeDropdownICastSense();
		Thread.sleep(10000);
		//Group
		//dp2.getGroupDropdown();
		//Group AD-Test
		//dp2.getGroupDropdownADTest();
	    //hread.sleep(5000);
		//Save Button
		dp2.getSaveButton();
		//Success Toaster
		//dp2.getSuccessToasterText();
		Thread.sleep(4000);
		driver.navigate().refresh();
	    //Calling Logout Method
		LogoutTest();  		
  	    extent.flush();
  		
  		//OrgSettings
  		/*OrgSettings Tab*/
  		System.out.println("Test Case: Org setting Tab Test");
  		log.info("Test Case: Org setting Tab Test");
  		
  		ExtentTest test14 = extent.createTest("OrgSettings Tab Test").assignCategory("Org Settings");
  	    //Calling LoginTest Method
  		LoginTest();
  		//Thread.sleep(4000);
  		//DashboardPage
  		//DashboardPage dp1 = new DashboardPage(driver);
  	    //Username
  		dp1.getUserName();
  		Thread.sleep(6000);
  		//OrgSettingsPage
  		OrgSettingsPage orgSettings = new OrgSettingsPage(driver);
  		//OrgSettingsTab
  		orgSettings.getOrgSettingsTab();
  		dp1.getUserName();
  	    //Calling Logout Method
  	    LogoutTest();
  	    
  	  extent.flush();
    }
    
    
    @AfterTest
	public void AfterTest()
	{
		driver.quit();
		System.out.println("Browser Closed");
		log.info("Test is Finished and Browser is closed");
		driver=null;	
	}
}
