package Testv2;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import PageObjects.DashboardPage;
import PageObjects.DeviceSettingsPage;
import PageObjects.LoginPage;
import Resources.base;

public class DeviceSettingsTest extends base{
	
    public static Logger log =LogManager.getLogger(DeviceSettingsTest.class.getName());
	
	//Declare WebDriver  Globally
    public WebDriver driver;
    
    /*Login Method*/
    public void LoginTest() throws IOException, InterruptedException
    {
    	//Login Screen
    	LoginPage lp = new LoginPage(driver);
    	//Email Address
    	lp.getEmailAddress();	
    	//RememberMe Checkbox
    	lp.getRememberMeCheckbox();
    	//NextButton
    	lp.getNextButton();
    	//Password
    	lp.getPassword();
    	Thread.sleep(3000);
    	//LoginButton
    	lp.getLoginButton();
    	Thread.sleep(8000);
    }
    
    /*Logout Method*/
    public void LogoutTest() throws InterruptedException
    {
    	//DashboardPage
  		DashboardPage dp1 = new DashboardPage(driver);		
		//Username
		dp1.getUserName().click();
		Thread.sleep(3000);
		//User Logout
		dp1.getUserLogout();
		driver.navigate().refresh();  	
    }
    
    @BeforeTest
	public void BeforeTest() throws IOException, InterruptedException
	{
		System.out.println("Execution started for class 'Device Settings Test'");
		log.info("Execution started for class 'Device Settings Test'");
		
		driver=InitializeDriver();			
		driver.manage().window().maximize();
		System.out.println("Initialised Driver and Maximised the Browser Window");
		log.info("Initialised Driver and Maximised the Browser Window");
		
		//Launch
		driver.get(prop.getProperty("testv2url"));
		System.out.println("Launched Admin Portal:"+driver.getCurrentUrl());
		log.info("Launched Admin Portal:"+driver.getCurrentUrl());

	}
    
    //Test Case: Device Settings: Device Reboot
    @Test(priority=1)
    public void DeviceSettingRebootDeviceTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Device Setting Tab Test");
  		log.info("Test Case: Device Setting Tab Test");
  	    //Calling LoginTest Method
  		LoginTest();
  		//Thread.sleep(4000);
  		DashboardPage dp1 = new DashboardPage(driver);	
  	    //Username
  		dp1.getUserName();
  		Thread.sleep(6000);
  		dp1.getDevicesTab();
  		//Devices Settings Page
  		DeviceSettingsPage dsp = new DeviceSettingsPage(driver);
  		Thread.sleep(3000);
  		//First Device
  		dsp.ClickOnDevice();
  		//Device Settings
  		dsp.ClickOnDeviceSettings();
  		Thread.sleep(8000);
  		//reboot Device
  		dsp.RebootDevice();
  		Thread.sleep(3000);
  		dp1.getUserName();
  	    //Calling Logout Method
  	    LogoutTest();
    }
    
    //Test Case: Cloud Update Interval Test
    @Test(priority=2)
    public void DeviceSettingCloudUpdateIntervalTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Cloud Update Interval Test");
  		log.info("Test Case: Cloud Update IntervalTest");
  	    //Calling LoginTest Method
  		LoginTest();
  		//Thread.sleep(4000);
  		DashboardPage dp1 = new DashboardPage(driver);	
  	    //Username
  		dp1.getUserName();
  		Thread.sleep(6000);
  		dp1.getDevicesTab(); 	
  		//Device Settings Page
  		DeviceSettingsPage dsp = new DeviceSettingsPage(driver);
  		Thread.sleep(4000);
  		//Device
  		dsp.ClickOnDevice();
  		Thread.sleep(2000);
  		//Device Settings
  		dsp.ClickOnDeviceSettings();
  		Thread.sleep(8000);
  		//Cloud Update Interval
  		dsp.CloudUpdateInterval();
  		dp1.getUserName();
  		Thread.sleep(3000);
  	    //Calling Logout Method
  	    LogoutTest();
    }
    
    //Test Case: Cloud Update Interval Same As Battery Test
    @Test(priority=3)
    public void DeviceSettingCloudUpdateIntervalSameAsBatteryTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Cloud Update Interval Same As Battery Test");
  		log.info("Test Case: Cloud Update Interval Same As Battery Test");
  	    //Calling LoginTest Method
  		LoginTest();
  		//Thread.sleep(4000);
  		DashboardPage dp1 = new DashboardPage(driver);	
  	    //Username
  		dp1.getUserName();
  		Thread.sleep(6000);
  		dp1.getDevicesTab();
        //Device Settings Page
  		DeviceSettingsPage dsp = new DeviceSettingsPage(driver);
  		//Device
  		dsp.ClickOnDevice();
  		Thread.sleep(2000);
  		//Device Settings
  		dsp.ClickOnDeviceSettings();
  		Thread.sleep(3000);
  		//Cloud Update Interval Same as Battery 	
  		dsp.CloudUpdateIntervalSameAsBattery();
  		dp1.getUserName();
  		Thread.sleep(3000);
  		//Calling Logout Method
  	    LogoutTest();
    }
    
    //Test Case: Operating Hours Test 
    @Test (priority=4)
    public void DeviceSettingOperatingHourTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Operating Hours Test");
  		log.info("Test Case: Operating Hours Test");
  	    //Calling LoginTest Method
  		LoginTest();
  		//Thread.sleep(4000);
  		DashboardPage dp1 = new DashboardPage(driver);	
  	    //Username
  		dp1.getUserName();
  		Thread.sleep(6000);
  		dp1.getDevicesTab();
  		//Device Settings Page
  		DeviceSettingsPage dsp = new DeviceSettingsPage(driver);
  		//Device
  		dsp.ClickOnDevice();
  		Thread.sleep(2000);
  		//Device Settings
  		dsp.ClickOnDeviceSettings();
  		Thread.sleep(3000);
  		//Operating Hours
  		dsp.operatingHour();
  		dp1.getUserName();
  		Thread.sleep(3000);
  		//Calling Logout Method
  	    LogoutTest();
    }
    /*
    //Test Case: Firmware Test
    @Test (priority=5)
    public void DeviceSettingFirmwareTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Firmware Test");
  		log.info("Test Case: Firmware Test");
  	    //Calling LoginTest Method
  		LoginTest();
  		//Thread.sleep(4000);
  		DashboardPage dp1 = new DashboardPage(driver);	
  	    //Username
  		dp1.getUserName();
  		Thread.sleep(6000);
  		//Devices Tab
  		dp1.getDevicesTab();
  		//Device Settings Page
  		DeviceSettingsPage dsp = new DeviceSettingsPage(driver);
  		//Device
  		dsp.ClickOnDevice();
  		Thread.sleep(2000);
  		//Device Settings
  		dsp.ClickOnDeviceSettings();
  		Thread.sleep(3000);
  		//Upload Firmware
  		dsp.UploadFirmware();
  		dp1.getUserName();
  		Thread.sleep(3000);
  		//Calling Logout Method
  	    LogoutTest();
    }
    */
    //Test Case: Export-Device Settings Export
    @Test(priority=7)
    public void DeviceSettingExportTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Device Settings Export");
  		log.info("Test Case: Device Settings Export");
  	    //Calling LoginTest Method
  		LoginTest();
  		//Thread.sleep(4000);
  		DashboardPage dp1 = new DashboardPage(driver);	
  	    //Username
  		dp1.getUserName();
  		Thread.sleep(6000);
  		//Devices Tab
  		dp1.getDevicesTab();
  		//Device Settings Page
  		DeviceSettingsPage dsp = new DeviceSettingsPage(driver);
  		//Device
  		dsp.ClickOnDevice();
  		Thread.sleep(3000);
  		//Export Download
  		dsp.exportDowload();
  		Thread.sleep(3000);
  		//Export Outside Click
  		//dsp.ExportOutsideClick();
  		//dp1.getUserName();
  		Thread.sleep(3000);
  		//Calling Logout Method
  	    //LogoutTest();
    }
    /*
    //Test Case: Export-Device Settings Export Email
    @Test(priority=8)
    public void DeviceSettingExportEmailTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Export-Email");
  		log.info("Test Case: Export-Email");
  	    //Calling LoginTest Method
  		LoginTest();
  		//Thread.sleep(4000);
  		DashboardPage dp1 = new DashboardPage(driver);	
  	    //Username
  		dp1.getUserName();
  		//Thread.sleep(6000);
  		//Devices Tab
  		dp1.getDevicesTab();
  	    //Device Settings Page
  		DeviceSettingsPage dsp = new DeviceSettingsPage(driver);
  		//Device
  		dsp.ClickOnDevice();
  		Thread.sleep(3000);
  		//Export 
  		dsp.exportDowloadEmail();
  		Thread.sleep(3000);
  	    //Export Outside Click
  		dsp.ExportOutsideClick();
  		//driver.navigate().back();
  		dp1.getUserName();
  		Thread.sleep(3000);
  		//Calling Logout Method
  	    LogoutTest();
    }
    */
    //Test Case: Device Activity Logs
    @Test(priority=6)
    public void DeviceSettingActivityLOgTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Device Activity log Test");
  		log.info("Test Case: Device Activity log Test");
  	    //Calling LoginTest Method
  		LoginTest();
  		//Thread.sleep(4000);
  		DashboardPage dp1 = new DashboardPage(driver);	
  	    //Username
  		dp1.getUserName();
  		Thread.sleep(6000);
  		//Devices Tab
  		dp1.getDevicesTab();
  	    //Device Settings Page
  		DeviceSettingsPage dsp = new DeviceSettingsPage(driver);  		
  		//Device
  		dsp.ClickOnDevice();
  		Thread.sleep(3000);
  		//Device Activity Logs
  		dsp.ActivityLog();
  		dp1.getUserName();
  		Thread.sleep(3000);
  		//Calling Logout Method
  	    LogoutTest();
    }
    
    @AfterTest
	public void AfterTest()
	{
		driver.quit();
		System.out.println("Browser Closed");
		log.info("Test is Finished and Browser is closed");
		driver=null;
			
	}

}
