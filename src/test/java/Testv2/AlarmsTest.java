package Testv2;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import PageObjects.AlarmsPage;
import PageObjects.DashboardPage;
import PageObjects.LoginPage;
import Resources.base;

public class AlarmsTest extends base{
	
    public static Logger log =LogManager.getLogger(AlarmsTest.class.getName());
	
	//Declare WebDriver  Globally
    public WebDriver driver;
    
    /*Login Method*/
    public void LoginTest() throws IOException, InterruptedException
    {
    	//Login Screen
    	LoginPage lp = new LoginPage(driver);
    	//Email Address
    	lp.getEmailAddress();	
    	//RememberMe Checkbox
    	lp.getRememberMeCheckbox();
    	//NextButton
    	lp.getNextButton();
    	//Password
    	lp.getPassword();
    	Thread.sleep(3000);
    	//LoginButton
    	lp.getLoginButton();
    	Thread.sleep(12000);
    }
    
    /*Logout Method*/
    public void LogoutTest() throws InterruptedException
    {
    	//DashboardPage
  		DashboardPage dp1 = new DashboardPage(driver);		
		//Username
		dp1.getUserName().click();
		//User Logout
		dp1.getUserLogout();
		//driver.navigate().refresh();  	
    }
    
    @BeforeTest
	public void BeforeTest() throws IOException, InterruptedException
	{
		System.out.println("Execution started for class 'AlarmsTest'");
		log.info("Execution started for class 'AlarmsTest'");
		
		driver=InitializeDriver();			
		driver.manage().window().maximize();
		System.out.println("Initialised Driver and Maximised the Browser Window");
		log.info("Initialised Driver and Maximised the Browser Window");
		
		//Launch
		driver.get(prop.getProperty("testv2url"));
		System.out.println("Launched Admin Portal:"+driver.getCurrentUrl());
		log.info("Launched Admin Portal:"+driver.getCurrentUrl());

	}
    
    SoftAssert sa = new SoftAssert();
    
    //TestCase: Alarm Tab click
    @Test(priority=1)
    public void AlarmTabTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Alarm Tab click");
  		log.info("Test Case: Alarm Tab click");
  		  		
  	    //Calling LoginTest Method
  		LoginTest();
  	    //DashboardPage
  		DashboardPage dp1 = new DashboardPage(driver);		
		//Username
		dp1.getUserName();	
  		//AlarmsPage
  		AlarmsPage ap = new AlarmsPage(driver);
  		//Alarms Tab
  		ap.getAlarmsTab();  	
    }
    
    //TestCase: Alarms Page Clear filter Test 
    @Test(priority=2)
    public void AlarmClearFilterTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Alarms Page Clear filter Test");
  		log.info("Test Case: Alarms page Clear filter Test"); 		
  	  		
 		//LoginTest();
  		AlarmsPage ap = new AlarmsPage(driver);
  		//ap.getAlarmsTab();  
  		//lp.getAlarmsTab();
  		//Thread.sleep(5000);
  		//Clear All Alarms
  		ap.clearAllAlarms();
    }
    
  
    //TestCase:Alarms Select Severity Filter Test
    @Test(priority=3)
    public void AlarmSelectSeverityFilterTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Alarms Select Severity Filter Test");
  		log.info("Test Case: Alarms Select Severity Filter Test");
  	
  		//LoginTest();
  		//Alarms  Page
  		AlarmsPage ap = new AlarmsPage(driver);
  		//Alarms Tab
  	    //ap.getAlarmsTab();
  	    //Thread.sleep(5000);
  		//Severity Filter
  		ap.SelectSeverityFilter();
  		driver.navigate().refresh();
  		Thread.sleep(5000);
  		//Calling Logout Method
  		//LogoutTest();
  		//driver.navigate().refresh();
    }
   
    @Test(priority=4)
    public void AlarmSearchAlarmTest() throws IOException, InterruptedException 
    {
  	    System.out.println("Test Case: Alarms Page search Test");
		log.info("Test Case: Alarms Page search Test");
			
		//Calling Login Method
		//LoginTest();
		//Thread.sleep(6000);
	    //DashboardPage
		//DashboardPage dp1 = new DashboardPage(driver);		
		//Username
		//dp1.getUserName();
		//Alarms Page
		AlarmsPage ap = new AlarmsPage(driver);
		//Alarms Tab
		//ap.getAlarmsTab();  	
		Thread.sleep(3000);
		//Search Alarm
		ap.alarmsSearch();
	    //Calling Logout Method
		Thread.sleep(2000);
		//LogoutTest();
    }
    
    //TestCase: Alarms Fixed Alarm Test
    @Test(priority=5)
    public void AlarmFixedAlarmTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Alarms Fixed Alarm Test");
  		log.info("Test Case: Alarms Fixed Alarm Test");
  	  		
  		//Calling Login Method
  		//LoginTest();
  		//Thread.sleep(6000);
  		//Alarms Page
  		AlarmsPage ap = new AlarmsPage(driver);
  		//ap.getAlarmsTab();
  		//Alarms Tab
        //Thread.sleep(3000);
    	//ap.getAlarmsTab();
  		//Fixed Alarm
  		ap.fixedAlarms();
  	    //Calling Logout Method
  		//LogoutTest();
    }
    
    //TestCase: Alarms False Alarm Test
    @Test(priority=6)
    public void AlarmFalseAlarmTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Alarms Page False Alarm Test");
  		log.info("Test Case: Alarms Page False Alarm Test");
  		
  		//Calling Login Method
  		//LoginTest();
  		//Alarms Page
  		AlarmsPage ap = new AlarmsPage(driver);
  		//Alarms Tab
  		//ap.getAlarmsTab();  
  		Thread.sleep(5000);
  		//False Alarm
  		ap.falseAlarms();
  		Thread.sleep(6000);
  		driver.navigate().refresh();  		
    }
    
    //TestCase: Alarms Color
    @Test(priority=7)
    public void AlarmColorTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Alarms Color");
  		log.info("Test Case: Alarms Color");
  	      		
  		//Calling Login Method
  		//LoginTest();
  		//Thread.sleep(3000);
  		AlarmsPage ap = new AlarmsPage(driver);
  		//ap.getAlarmsTab();
  		//Thread.sleep(12000);
  		//Crtical  Alrm Text
  		ap.getCriticalAlarms();
  		//Critical Alarms Color
  		ap.getCriticalAlarmsColor();
  	    //Moderate  Alrm Text
  		ap.getModerateAlarms();
  		//Moderate Alarms Color
  		ap.getModerateAlarmsColor();
  	    //High  Alrm Text
  		ap.getHighAlarms();
  		//High Alarms Color
  		ap.getHighAlarmsColor();
  	    //Low  Alrm Text
  		ap.getLowAlarms();
  		//Critical Alarms Color
  		ap.getLowAlarmsColor();
  	    //Calling Logout Method
  		LogoutTest();
    }

    @Test(priority=8)
    public void AlarmAgeSearchAlarmTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Alarms Page Age search Test");
  		log.info("Test Case: Alarms Page Age search Test");
  	  		
  		//Calling Login Method
  		LoginTest();
  		//Thread.sleep(6000);
  	    //DashboardPage
  		DashboardPage dp1 = new DashboardPage(driver);		
		//Username
		dp1.getUserName();
  		//Alarms Page
  		AlarmsPage ap = new AlarmsPage(driver);
  		//Alarms Tab
  		Thread.sleep(3000);
  		ap.getAlarmsTab();  		
  		//Search Alarm
  		ap.AlarmAgeSearch();
  	    //Calling Logout Method
  		Thread.sleep(2000);
  		LogoutTest();
    }
    /*
    @Test(priority=9)
    public void AlarmPathFilterAlarmTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Alarms Page Path Filter Test");
  		log.info("Test Case: Alarms Page Age Path Filter Test");
  		
  	    //Calling Login Method
    	LoginTest();
  		//Thread.sleep(6000);
	    //DashboardPage
  		DashboardPage dp1 = new DashboardPage(driver);		
		//Username
		dp1.getUserName();
  		//Alarms Page
  		AlarmsPage ap = new AlarmsPage(driver);
  		//Alarms Tab
  		Thread.sleep(3000);
  		ap.getAlarmsTab();  		
  		//Search Alarm
  		ap.AlarmPath();
  	    //Calling Logout Method
		Thread.sleep(2000);
  		LogoutTest();
    }
    */
        
    @AfterTest
	public void AfterTest()
	{
		driver.quit();
		System.out.println("Browser Closed");
		log.info("Test is Finished and Browser is closed");
		driver=null;
			
	}
    
}
