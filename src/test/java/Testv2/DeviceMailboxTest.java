	package Testv2;

	import java.io.IOException;

	import org.apache.logging.log4j.LogManager;
	import org.apache.logging.log4j.Logger;
	import org.openqa.selenium.WebDriver;
	import org.testng.annotations.AfterTest;
	import org.testng.annotations.BeforeTest;
	import org.testng.annotations.Test;

	import PageObjects.DashboardPage;
import PageObjects.DeviceMailboxPage;
import PageObjects.LoginPage;
	import PageObjects.OrgSettingsPage;
	import Resources.base;

	public class DeviceMailboxTest extends base{
		
	    public static Logger log =LogManager.getLogger(DeviceMailboxTest.class.getName());
		
		//Declare WebDriver  Globally
	    public WebDriver driver;
	    
	    /*Login Method*/
	    public void LoginTest() throws IOException, InterruptedException
	    {
	    	//Login Screen
	    	LoginPage lp = new LoginPage(driver);
	    	//Email Address
	    	lp.getEmailAddress();	
	    	//RememberMe Checkbox
	    	lp.getRememberMeCheckbox();
	    	//NextButton
	    	lp.getNextButton();
	    	//Password
	    	lp.getPassword();
	    	Thread.sleep(3000);
	    	//LoginButton
	    	lp.getLoginButton();
	    	Thread.sleep(8000);
	    }
	    
	    /*Logout Method*/
	    public void LogoutTest() throws InterruptedException
	    {
	    	//DashboardPage
	  		DashboardPage dp1 = new DashboardPage(driver);		
			//Username
			dp1.getUserName().click();
			//User Logout
			dp1.getUserLogout();
			driver.navigate().refresh();  	
	    }
	    
	    @BeforeTest
		public void BeforeTest() throws IOException, InterruptedException
		{
			System.out.println("Execution started for class 'DeviceMailboxTest'");
			log.info("Execution started for class 'DeviceMailboxTest'");
			
			driver=InitializeDriver();			
			driver.manage().window().maximize();
			System.out.println("Initialised Driver and Maximised the Browser Window");
			log.info("Initialised Driver and Maximised the Browser Window");
			
			//Launch
			driver.get(prop.getProperty("testv2url"));
			System.out.println("Launched Admin Portal:"+driver.getCurrentUrl());
			log.info("Launched Admin Portal:"+driver.getCurrentUrl());

		}
	    
	    //Device Mailbox
	    //Test Case: Device Mailbox Tab Test
	    @Test(priority=1)
	    public void DeviceMailboxTabTest() throws IOException, InterruptedException 
	    {
	    	System.out.println("Test Case: Device Mailbox Tab Test");
	  		log.info("Test Case: Device Mailbox Tab Test");
	  		
	  	    //Calling LoginTest Method
	  		LoginTest();
	  		//DeviceMailboxPage
	  		DeviceMailboxPage mailbox = new DeviceMailboxPage(driver);
	  		Thread.sleep(6000);
	  		//DeviceMailboxTab
	  		mailbox.getDeviceMailboxTab();
	  	    //Calling Logout Method
	  	    LogoutTest(); 	
	    }
//	    @Test(priority=2)
//	    public void DeviceMailboxSearchTest() throws IOException, InterruptedException 
//	    {
//	    	System.out.println("Test Case: Device Mailbox search Test");
//	  		log.info("Test Case: Device Mailbox search Test");
//	  		
//	  	    //Calling LoginTest Method
//	  		LoginTest();
//	  		//DeviceMailboxPage
//	  		DeviceMailboxPage mailbox = new DeviceMailboxPage(driver);
//	  		Thread.sleep(6000);
//	  		//DeviceMailboxTab
//	  		mailbox.getDeviceMailboxTab();
//	  		Thread.sleep(8000);
//	  		mailbox.DeviceMailboxSearch();
//	  	    //Calling Logout Method
//	  	    LogoutTest(); 	
//	    }
	    @Test(priority=2)
	    public void DeviceMailboxColumnTest() throws IOException, InterruptedException 
	    {
	    	System.out.println("Test Case: Device Mailbox search Test");
	  		log.info("Test Case: Device Mailbox search Test");
	  		
	  	    //Calling LoginTest Method
	  		LoginTest();
	  		//DeviceMailboxPage
	  		DeviceMailboxPage mailbox = new DeviceMailboxPage(driver);
	  		Thread.sleep(6000);
	  		//DeviceMailboxTab
	  		mailbox.DeviceColumn();
	  	    //Calling Logout Method
	  	    LogoutTest(); 	
	    }
	    
	    @AfterTest
		public void AfterTest()
		{
			driver.quit();
			System.out.println("Browser Closed");
			log.info("Test is Finished and Browser is closed");
			driver=null;
				
		}

	}

