package Testv2;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import PageObjects.ActivityLogsPage;
import PageObjects.DashboardPage;
import PageObjects.LoginPage;
import PageObjects.OrgSettingsPage;
import Resources.base;

public class ActivityLogTest extends base{
	
public static Logger log =LogManager.getLogger(ActivityLogTest.class.getName());
	
	//Declare WebDriver  Globally
    public WebDriver driver;
    
    /*Login Method*/
    public void LoginTest() throws IOException, InterruptedException
    {
    	//Login Screen
    	LoginPage lp = new LoginPage(driver);
    	//Email Address
    	lp.getEmailAddress();	
    	//RememberMe Checkbox
    	lp.getRememberMeCheckbox();
    	//NextButton
    	lp.getNextButton();
    	//Password
    	lp.getPassword();
    	Thread.sleep(3000);
    	//LoginButton
    	lp.getLoginButton();
    	Thread.sleep(10000);
    }
    
    /*Logout Method*/
    public void LogoutTest() throws InterruptedException
    {
    	//DashboardPage
  		DashboardPage dp1 = new DashboardPage(driver);		
		//Username
		dp1.getUserName().click();
		//User Logout
		dp1.getUserLogout();
		driver.navigate().refresh();  	
    }
    
    @BeforeTest
	public void BeforeTest() throws IOException, InterruptedException
	{
		System.out.println("Execution started for class 'ActivityLogsTest'");
		log.info("Execution started for class 'ActivityLogsTest'");
		
		driver=InitializeDriver();			
		driver.manage().window().maximize();
		System.out.println("Initialised Driver and Maximised the Browser Window");
		log.info("Initialised Driver and Maximised the Browser Window");
		
		//Launch
		driver.get(prop.getProperty("testv2url"));
		System.out.println("Launched Admin Portal:"+driver.getCurrentUrl());
		log.info("Launched Admin Portal:"+driver.getCurrentUrl());

	}
  //Test Case: Activity Logs Tab Test
    @Test(priority=1)
    public void ActivityLogTabTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Activity log Tab Test");
  		log.info("Test Case: Activity log Tab Test");
  		
  	    //Calling LoginTest Method
  		LoginTest();
  		//Thread.sleep(4000);
  		//DashboardPage
  		DashboardPage dp1 = new DashboardPage(driver);
  	    //Username
  		dp1.getUserName();
  		Thread.sleep(6000);
  		//Activity log Page 
  		ActivityLogsPage activityLogsPage = new ActivityLogsPage(driver);
  	    //Activity log Tab
  		activityLogsPage.getActivityLogTab();
  		//Thread.sleep(3000);
  	    //Calling Logout Method
  	    LogoutTest(); 	
  	    
    }
    
  //Test Case: Activity Logs Create search Test
    @Test(priority=2)
    public void ActivityLogCreateSearchTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Activity log Create search Test");
  		log.info("Test Case: Activity log Create search Test");
  	    //Calling LoginTest Method
  		LoginTest();
  		//Thread.sleep(4000);
  		//DashboardPage
  		DashboardPage dp1 = new DashboardPage(driver);
  	    //Username
  		dp1.getUserName();
  		Thread.sleep(6000);
  		//Activity log Page 
  		ActivityLogsPage activityLogsPage = new ActivityLogsPage(driver);
  	    //Activity log search 
  		activityLogsPage.activityLogCreateSearch();
  		//Thread.sleep(3000);
  	    //Calling Logout Method
  	    LogoutTest(); 	
  	    
    }
    
  //Test Case: Activity Logs Update search Test
    @Test(priority=3)
    public void ActivityLogUpdateSearchTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Activity log Update search Test");
  		log.info("Test Case: Activity log Update search Test");
  	    //Calling LoginTest Method
  		LoginTest();
  		//Thread.sleep(4000);
  		//DashboardPage
  		DashboardPage dp1 = new DashboardPage(driver);
  	    //Username
  		dp1.getUserName();
  		Thread.sleep(6000);
  		//Activity log Page 
  		ActivityLogsPage activityLogsPage = new ActivityLogsPage(driver);
  	    //Activity log search 
  		activityLogsPage.activityLogUpdateSearch();
  		//Thread.sleep(3000);
  	    //Calling Logout Method
  	    LogoutTest(); 	
  	    
    }
    
    //Test Case: Activity Logs Rule search Test
    @Test(priority=4)
    public void ActivityLogRuleSearchTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Activity log Rule search Test");
  		log.info("Test Case: Activity log Rule search Test");
  	    //Calling LoginTest Method
  		LoginTest();
  		//Thread.sleep(4000);
  		//DashboardPage
  		DashboardPage dp1 = new DashboardPage(driver);
  	    //Username
  		dp1.getUserName();
  		Thread.sleep(6000);
  		//Activity log Page 
  		ActivityLogsPage activityLogsPage = new ActivityLogsPage(driver);
  	    //Activity log search 
  		activityLogsPage.activityLogRuleSearch();
  		//Thread.sleep(3000);
  	    //Calling Logout Method
  	    LogoutTest(); 	
  	    
    }
    
  //Test Case: Activity Logs Device search Test
    @Test(priority=5)
    public void ActivityLogDeviceSearchTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Activity log Device search Test");
  		log.info("Test Case: Activity log Device search Test");
  	    //Calling LoginTest Method
  		LoginTest();
  		//Thread.sleep(4000);
  		//DashboardPage
  		DashboardPage dp1 = new DashboardPage(driver);
  	    //Username
  		dp1.getUserName();
  		Thread.sleep(6000);
  		//Activity log Page 
  		ActivityLogsPage activityLogsPage = new ActivityLogsPage(driver);
  	    //Activity log search 
  		activityLogsPage.activityLogDeviceSearch();
  		//Thread.sleep(3000);
  	    //Calling Logout Method
  	    LogoutTest(); 	
  	    
    }

	@AfterTest
	public void AfterTest()
	{
		driver.quit();
		System.out.println("Browser Closed");
		log.info("Test is Finished and Browser is closed");
		driver=null;
			
	}
}
