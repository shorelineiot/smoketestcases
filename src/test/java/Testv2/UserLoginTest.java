package Testv2;

import java.io.IOException;
import java.time.Duration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import com.aventstack.extentreports.ExtentTest;

import PageObjects.DashboardPage;
import PageObjects.LoginPage;
import Resources.base;

public class UserLoginTest extends BaseTest{
	
	public static Logger log =LogManager.getLogger(UserLoginTest.class.getName());
	
	//Declare WebDriver  Globally
    public WebDriver driver;
   
    SoftAssert sa = new SoftAssert();
    
    /*Login Method*/
    public void LoginTest() throws IOException, InterruptedException
    {
    	//Login Screen
    	LoginPage lp = new LoginPage(driver);
    	//Email Address
    	lp.getEmailAddress();	
    	//RememberMe Checkbox
    	lp.getRememberMeCheckbox();
    	//NextButton
    	lp.getNextButton();
    	//Password
    	lp.getPassword();
    	Thread.sleep(3000);
    	//LoginButton
    	lp.getLoginButton();
    	Thread.sleep(8000);
    }
    
    /*Logout Method*/
    public void LogoutTest() throws InterruptedException
    {
    	//DashboardPage
  		DashboardPage dp1 = new DashboardPage(driver);		
		//Username
		dp1.getUserName().click();
		//User Logout
		dp1.getUserLogout();
		  	
    }
    
    @BeforeTest
	public void BeforeTest() throws IOException, InterruptedException
	{
		System.out.println("Execution started for class 'UserLoginTest'");
		log.info("Execution started for class 'UserLoginTest'");
		
		driver=InitializeDriver();			
		driver.manage().window().maximize();
		System.out.println("Initialised Driver and Maximised the Browser Window");
		log.info("Initialised Driver and Maximised the Browser Window");
		
		//Launch
		driver.get(prop.getProperty("testv2url"));
		System.out.println("Launched Admin Portal:"+driver.getCurrentUrl());
		log.info("Launched Admin Portal:"+driver.getCurrentUrl());

	}
            
    //User Login Cases   
    //TestCase:  User Login Test
    @Test(priority=1)
    public void UserLogin() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: User Login Test");
  		log.info("Test Case: User Login Test");
  				  		
  		//Calling LoginTest Method
  		LoginTest();
		//DashboardPage
  		DashboardPage dp1 = new DashboardPage(driver);		
		//Username
		dp1.getUserName();
		
	}
    
    //TestCase:  User Logout Test
    @Test(priority=2)
    public void UserLogout() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: User Logout Test");
  		log.info("Test Case: User Logout Test");
	      		
  		//Calling Logout Test Method
  		LogoutTest();
  		
	}
    
    @AfterTest
	public void AfterTest()
	{
		driver.quit();
		System.out.println("Browser Closed");
		log.info("Test is Finished and Browser is closed");
		driver=null;
			
	}
   
}
