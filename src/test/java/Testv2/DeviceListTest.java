package Testv2;

import java.io.IOException;
import java.time.Duration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import PageObjects.DashboardPage;
import PageObjects.DeviceDataPage;
import PageObjects.DeviceSettingsPage;
import PageObjects.DevicesPage;
import PageObjects.LoginPage;
import Resources.base;

public class DeviceListTest extends base{
	
    public static Logger log =LogManager.getLogger(DeviceListTest.class.getName());
	
	//Declare WebDriver  Globally
    public WebDriver driver;
    
    /*Login Method*/
    public void LoginTest() throws IOException, InterruptedException
    {
    	//Login Screen
    	LoginPage lp = new LoginPage(driver);
    	//Email Address
    	lp.getEmailAddress();	
    	//RememberMe Checkbox
    	lp.getRememberMeCheckbox();
    	//NextButton
    	lp.getNextButton();
    	//Password
    	lp.getPassword();
    	Thread.sleep(3000);
    	//LoginButton
    	lp.getLoginButton();
    	Thread.sleep(8000);
    }
    
    /*Logout Method*/
    public void LogoutTest() throws InterruptedException
    {
    	//DashboardPage
  		DashboardPage dp1 = new DashboardPage(driver);		
		//Username
		dp1.getUserName().click();
		//User Logout
		dp1.getUserLogout();
		driver.navigate().refresh();  	
    }
    
    @BeforeTest
	public void BeforeTest() throws IOException, InterruptedException
	{
		System.out.println("Execution started for class 'DeviceListTest'");
		log.info("Execution started for class 'DeviceListTest'");
		
		driver=InitializeDriver();			
		driver.manage().window().maximize();
		System.out.println("Initialised Driver and Maximised the Browser Window");
		log.info("Initialised Driver and Maximised the Browser Window");
		
		//Launch
		driver.get(prop.getProperty("testv2url"));
		System.out.println("Launched Admin Portal:"+driver.getCurrentUrl());
		log.info("Launched Admin Portal:"+driver.getCurrentUrl());

	}
            
    //Device Cases
    //TestCase: Device Tab Click Test
    @Test(priority=1)
    public void DeviceTabClickTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Device Tab Click Test");
  		log.info("Test Case: Device Tab Click Test");
  		
  		//Calling LoginTest Method
  		LoginTest();
  		//DashboardPage
  	  	DashboardPage dp1 = new DashboardPage(driver);
  	    //Username
  		dp1.getUserName();
  		//Thread.sleep(8000);
  		//Devices Tab
  		dp1.getDevicesTab();
  		Thread.sleep(4000);
  		driver.navigate().refresh();
  		Thread.sleep(4000);
    }
    
    //TestCase: UpdateDeviceDetails
    @Test(priority=2)
    public void UpdateDeviceTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Update device details Test");
  		log.info("Test Case: Update device details Test");
  	 
  		//DevicesPage
  		DevicesPage dp1 = new DevicesPage(driver);
  		//First Device
  	    dp1.getFirstDevice();
  	    //DeviceDataPage
  	    DeviceDataPage dp2 = new DeviceDataPage(driver);
  	    //Settings Icon
  	    dp2.getSettingsIcon();
  	    //DeviceSettingsPage
  	    DeviceSettingsPage dp3 = new DeviceSettingsPage(driver);
  	    // DeviceName
  	    dp3.getDeviceName();
  	    //Save
  	    dp3.getDeviceNameSave();
  	    Thread.sleep(3000);
  	    //ToasterMessage
  	    //dp3.getToastMsg();
  	    //Calling Logout Method
  		LogoutTest();
    }
    
    //TestCase: Create Device Test
    @Test(priority=3)
    public void CreateDeviceTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Create Device Test");
  		log.info("Test Case: Create Device Test");
  		
  		driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(90));
  		
  	    //Calling LoginTest Method
  		LoginTest();
  		//DashboardPage
  	  	DashboardPage dp1 = new DashboardPage(driver);
  	    //Username
  		dp1.getUserName();  		
  		//Thread.sleep(8000);
  		//Devices Tab
  		dp1.getDevicesTab();
  		
  		//DevicesPage
  		DevicesPage dp2 = new DevicesPage(driver);
  	    //Username
  		dp1.getUserName();
  		//Thread.sleep(4000);
  		//Create Device Button
  		dp2.getCreateDeviceButton();
  		//Select Device
  		dp2.getSelectDeviceCreation();
  		//Device Name
  		dp2.getDeviceName();
  		//Device Type Dropdown
  		dp2.getDeviceTypeDropdown();
  		//Device Type Icastsense
  		dp2.getDeviceTypeDropdownICastSense();
  		Thread.sleep(10000);
  		//Group
  		//dp2.getGroupDropdown();
  		//Group AD-Test
  		//dp2.getGroupDropdownADTest();
  	    //hread.sleep(5000);
  		//Save Button
  		dp2.getSaveButton();
  		//Success Toaster
  		//dp2.getSuccessToasterText();
  		Thread.sleep(4000);
  		driver.navigate().refresh();
  	    //Calling Logout Method
  		LogoutTest();  		
    }
    
    //TestCase: Device Delete
    @Test(priority=4)
    public void DeviceDelete() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Device Delete");
  		log.info("Test Case: Device Delete");
  	 
  	    //Calling LoginTest Method
  		LoginTest();
  	    //DashboardPage
  	  	DashboardPage dp1 = new DashboardPage(driver);
  	    //Username
  		dp1.getUserName();
  		//Thread.sleep(8000);
  		//Devices Tab
  		dp1.getDevicesTab();
  	    //DevicesPage
  		DevicesPage dp2 = new DevicesPage(driver);
  		//First Device
  	    dp2.getFirstDevice();
  	    //DeviceDataPage
  	    DeviceDataPage dp3 = new DeviceDataPage(driver);
  	    Thread.sleep(4000);
  	    //Settings Icon
  	    dp3.getSettingsIcon();
  		//DeviceSettingsPage
  	    DeviceSettingsPage dp4 = new DeviceSettingsPage(driver);
  	    driver.navigate().refresh();
  	    Thread.sleep(8000);
  	    // DeviceDelete
  	    dp4.getDeviceDelete();
  	    //Delete Button Popup
  	    dp4.getDeleteButtonPopup();
  	    Thread.sleep(8000);
  	    //Toaster Message
  	    //dp3.getToasterMessage();
  	    driver.navigate().refresh();
	    //Calling Logout Method
		LogoutTest();     
    }
    
    
    //TestCase: Group Creation
    @Test(priority=5)
    public void GroupCreationTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Group Creation Test");
  		log.info("Test Case: Group Creation Test");
  		
  	    //Calling LoginTest Method
  		LoginTest();
  	    //DashboardPage
  	  	DashboardPage dp1 = new DashboardPage(driver);
  	    //Username
  		dp1.getUserName();
  	    //Devices Tab
  		dp1.getDevicesTab();
  		//DashboardPage
  	  	DevicesPage dp2 = new DevicesPage(driver);		 		
  		Thread.sleep(8000);
  		//Create Group Tab
  		dp2.getCreateGroup();
  		//Group Name
  		dp2.getGroupName();
  		//Save Group  		
  		Thread.sleep(4000);
  		dp2.getSaveGroupName();
  		Thread.sleep(8000);
  		 //Username
  		dp1.getUserName();
  		//driver.navigate().refresh();
  	    //Calling Logout Method
  	    LogoutTest();
    }
    
    
    //TestCase: Group Update
    @Test(priority=6)
    public void GroupUpdateGroupNameTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Group Updation Test");
  		log.info("Test Case: Group Updation Test");
  		
  	    //Calling LoginTest Method
  		LoginTest();
  		Thread.sleep(4000);
  		//DashboardPage
  		DashboardPage dp1 = new DashboardPage(driver);
  	    //Username
  		dp1.getUserName();
  	  	//Devices Tab
  		dp1.getDevicesTab();
  		Thread.sleep(8000);
  		//Devices Page
  		DevicesPage dp2 = new DevicesPage(driver);		 		
  		//Create Group Tab
  		dp2.getCreateGroup();
  		//Group Name
  		dp2.getGroupName();
  		//Save Group
  		dp2.getSaveGroupName();
  		Thread.sleep(20000);
  		dp2.UpdateGroupName();
    }
    
    //TestCase: Group Delete
    @Test(priority=7)
    public void GroupDeleteGroupTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Group Delete Test");
  		log.info("Test Case: Group Delete Test");
  	    //Calling LoginTest Method
  		//LoginTest();
  		//Thread.sleep(4000);
  		//DashboardPage
  		DashboardPage dp1 = new DashboardPage(driver);
  	    //Username
  		//dp1.getUserName();
  	    //Devices Tab
  		//dp1.getDevicesTab();
  		Thread.sleep(8000);
  		//DevicesPage
  		DevicesPage dp2 = new DevicesPage(driver);		 		  		
  		//Create Group Tab
  		//dp2.getCreateGroup();
  		//Group Name
  		//dp2.getGroupName();
  		//Save Group
  		//dp2.getSaveGroupName();
  		//Thread.sleep(10000);
  		dp2.DeleteGroupName();
  		Thread.sleep(10000);
  	    //Calling Logout Method
  	    LogoutTest();	
    }
    
    /*
    //TestCase: Create Pwwertrain Test
    @Test(priority=8)
    public void CreatePowertrainTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Create Powertrain Test");
  		log.info("Test Case: Create Powertrain Test");
  		
  		driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(90));
  		
  	    //Calling LoginTest Method
  		LoginTest();
  		//DashboardPage
  	  	DashboardPage dp1 = new DashboardPage(driver);
  	    //Username
  		dp1.getUserName();  		
  		//Thread.sleep(8000);
  		//Devices Tab
  		dp1.getDevicesTab();
  	    //Username
  		dp1.getUserName();
  		//DevicesPage
  		DevicesPage dp2 = new DevicesPage(driver);
  		//Thread.sleep(4000);
  		//Create Device Button
  		dp2.getCreateDeviceButton();
  		//Select Powertrain
  		dp2.getSelectPowertrainCreation();
  		//Powertrain Name
  		dp2.getPowertrainName();
  		//Powertrain RPM Min
  		dp2.getPowertrainRPM_min();
  	    //Powertrain RPM Max
  		dp2.getPowertrainRPM_max();
  		//PrimeMover Name
  		dp2.getPrimeMoverName();
  		//Prime Mover Serial No
  		dp2.getPrimeMoverSerialNo();
  		//PrimeMover Component Class Dropdown
  		dp2.getPMComponentClassDropdown();
  		//PrimeMover Component Class Select
  		dp2.getPMComponentClassSelect();
  		//PrimeMover Component Sub Class Dropdown
  		dp2.getPMComponentSubClassDropdown();
  	    //PrimeMover Component Sub Class Select
  		dp2.getPMComponentSubClassSelect();
  		//PrimeMover Component Manufacturer
  		dp2.getPMComponentManufacturerDropdown();
  		//PrimeMover Component Manufacturer Select
  		dp2.getPMComponentManufacturerSelect();
  		//Save Button
  		dp2.getSaveButton();
  		Thread.sleep(4000);
  		driver.navigate().refresh();
  	    //Calling Logout Method
  		LogoutTest();  		
    }
    
    //TestCase: Update Powertrain Test
    @Test(priority=36)
    public void UpdatePowertrainTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Update Powertrain Test");
  		log.info("Test Case: Update Powertrain Test");
  		
  		driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(90));
  		
  	    //Calling LoginTest Method
  		LoginTest();
  		//DashboardPage
  	  	DashboardPage dp1 = new DashboardPage(driver);
  	    //Username
  		dp1.getUserName();  		
  		//Thread.sleep(8000);
  		//Devices Tab
  		dp1.getDevicesTab();
  	    //Username
  		dp1.getUserName();
  		//DevicesPage
  		DevicesPage dp2 = new DevicesPage(driver);
  		//Thread.sleep(4000);
  		//Create Device Button
  		dp2.getCreateDeviceButton();
  		//Select Powertrain
  		dp2.getSelectPowertrainCreation();
  		//Powertrain Name
  		dp2.getPowertrainName_Update();
  		//Powertrain RPM Min
  		dp2.getPowertrainRPM_min_Update();  	    
  		//Save Button
  		dp2.getSaveButton();
  		Thread.sleep(4000);
  		driver.navigate().refresh();
  	    //Calling Logout Method
  		LogoutTest();  		
    }
    */    
    @AfterTest
	public void AfterTest()
	{
		driver.quit();
		System.out.println("Browser Closed");
		log.info("Test is Finished and Browser is closed");
		driver=null;
			
	}
}
