package Testv2;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import PageObjects.DashboardPage;
import PageObjects.LoginPage;
import PageObjects.OrgSettingsPage;
import Resources.base;

public class CustomizationTest extends base{
	
	public static Logger log =LogManager.getLogger(OrgSettingsTest.class.getName());
	
	//Declare WebDriver  Globally
    public WebDriver driver;
    
    /*Login Method*/
    public void LoginTest() throws IOException, InterruptedException
    {
    	//Login Screen
    	LoginPage lp = new LoginPage(driver);
    	//Email Address
    	lp.getEmailAddress();	
    	//RememberMe Checkbox
    	lp.getRememberMeCheckbox();
    	//NextButton
    	lp.getNextButton();
    	//Password
    	lp.getPassword();
    	Thread.sleep(3000);
    	//LoginButton
    	lp.getLoginButton();
    	Thread.sleep(8000);
    }
    
    /*Logout Method*/
    public void LogoutTest() throws InterruptedException
    {
    	//DashboardPage
  		DashboardPage dp1 = new DashboardPage(driver);		
		//Username
		dp1.getUserName().click();
		//User Logout
		dp1.getUserLogout();
		driver.navigate().refresh();  	
    }
    
    @BeforeTest
	public void BeforeTest() throws IOException, InterruptedException
	{
		System.out.println("Execution started for class 'OrgSettingsTest'");
		log.info("Execution started for class 'OrgSettingsTest'");
		
		driver=InitializeDriver();			
		driver.manage().window().maximize();
		System.out.println("Initialised Driver and Maximised the Browser Window");
		log.info("Initialised Driver and Maximised the Browser Window");
		
		//Launch
		driver.get(prop.getProperty("testv2url"));
		System.out.println("Launched Admin Portal:"+driver.getCurrentUrl());
		log.info("Launched Admin Portal:"+driver.getCurrentUrl());

	}
    
  //Test Case: Org Settings: Customization Page
    @Test(priority=1)
    public void OrgSettingCustomizationTabTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Org setting Customization tab Test");
  		log.info("Test Case: Org setting Customization tab Test");
  	    //Calling LoginTest Method
  		LoginTest();
  		//DashboardPage
  		DashboardPage dp1 = new DashboardPage(driver);
  		Thread.sleep(6000);
  		//Org Settings
  		OrgSettingsPage orgSettings = new OrgSettingsPage(driver);
  		//Customization Tab
  		orgSettings.getCustomizationTab();
  		Thread.sleep(2000);
  		dp1.getUserName();
  	    //Calling Logout Method
  	    LogoutTest();
    }
    
    //Test Case: Org Settings: org name change
    @Test(priority=2)
    public void OrgSettingOrgNameChangeTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Org setting org name change Test");
  		log.info("Test Case: Org setting org name change Test");
  	    //Calling LoginTest Method
  		LoginTest();
  		//DashboardPage
  		DashboardPage dp1 = new DashboardPage(driver);
  		Thread.sleep(6000);
  		//Org Settings
  		OrgSettingsPage orgSettings = new OrgSettingsPage(driver);
  		//Customization Tab
  		orgSettings.ChangeOrgNameTab();
  		Thread.sleep(2000);
  		dp1.getUserName();
  	    //Calling Logout Method
  	    LogoutTest();
    }
    
    
	@AfterTest
	public void AfterTest()
	{
		driver.quit();
		System.out.println("Browser Closed");
		log.info("Test is Finished and Browser is closed");
		driver=null;
			
	}
}
