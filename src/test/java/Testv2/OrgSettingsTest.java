package Testv2;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import PageObjects.DashboardPage;
import PageObjects.LoginPage;
import PageObjects.OrgSettingsPage;
import Resources.base;

public class OrgSettingsTest extends base{
	
    public static Logger log =LogManager.getLogger(OrgSettingsTest.class.getName());
	
	//Declare WebDriver  Globally
    public WebDriver driver;
    
    /*Login Method*/
    public void LoginTest() throws IOException, InterruptedException
    {
    	//Login Screen
    	LoginPage lp = new LoginPage(driver);
    	//Email Address
    	lp.getEmailAddress();	
    	//RememberMe Checkbox
    	lp.getRememberMeCheckbox();
    	//NextButton
    	lp.getNextButton();
    	//Password
    	lp.getPassword();
    	Thread.sleep(3000);
    	//LoginButton
    	lp.getLoginButton();
    	Thread.sleep(8000);
    }
    
    /*Logout Method*/
    public void LogoutTest() throws InterruptedException
    {
    	//DashboardPage
  		DashboardPage dp1 = new DashboardPage(driver);		
		//Username
		dp1.getUserName().click();
		//User Logout
		dp1.getUserLogout();
		driver.navigate().refresh();  	
    }
    
    @BeforeTest
	public void BeforeTest() throws IOException, InterruptedException
	{
		System.out.println("Execution started for class 'OrgSettingsTest'");
		log.info("Execution started for class 'OrgSettingsTest'");
		
		driver=InitializeDriver();			
		driver.manage().window().maximize();
		System.out.println("Initialised Driver and Maximised the Browser Window");
		log.info("Initialised Driver and Maximised the Browser Window");
		
		//Launch
		driver.get(prop.getProperty("testv2url"));
		System.out.println("Launched Admin Portal:"+driver.getCurrentUrl());
		log.info("Launched Admin Portal:"+driver.getCurrentUrl());

	}
    
    
    //Test Case: OrgSettings Tab Test
    @Test(priority=1)
    public void OrgSettingTabTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Org setting Tab Test");
  		log.info("Test Case: Org setting Tab Test");
  		
  	    //Calling LoginTest Method
  		LoginTest();
  		//Thread.sleep(4000);
  		//DashboardPage
  		DashboardPage dp1 = new DashboardPage(driver);
  	    //Username
  		dp1.getUserName();
  		Thread.sleep(6000);
  		//OrgSettingsPage
  		OrgSettingsPage orgSettings = new OrgSettingsPage(driver);
  		//OrgSettingsTab
  		orgSettings.getOrgSettingsTab();
  		dp1.getUserName();
  	    //Calling Logout Method
  	    LogoutTest(); 	
    }
    
    //Test Case: Users Tab Click Test
    @Test(priority=2)
    public void OrgSettingUserTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Users Tab Click Test");
  		log.info("Test Case: Users Tab Click Test");
  	    //Calling LoginTest Method
  		LoginTest();
  		//Thread.sleep(4000);
  		//DashboardPage
  		DashboardPage dp1 = new DashboardPage(driver);
  	    //Username
  		dp1.getUserName();
  		Thread.sleep(6000);
  		//Org Settings
  		OrgSettingsPage orgSettings = new OrgSettingsPage(driver);
  	    //OrgSettingsTab
//  		orgSettings.getOrgSettingsTab();
  		//Thread.sleep(5000);
  		//Users Tab
//  		Thread.sleep(5000);
  		orgSettings.getUsersTab();
  		
//  		dp1.getUserName();
  	    //Calling Logout Method
  	    LogoutTest();
    }
    
    //User Invite
    //Test Case: User Invite
    @Test(priority=3)
    public void OrgSettingInviteUserTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: User Invite");
  		log.info("Test Case:User Invite");
  	    //Calling LoginTest Method
  		LoginTest();
  		//Thread.sleep(4000);
  		//DashboardPage
  		DashboardPage dp1 = new DashboardPage(driver);
  	    //Username
  		dp1.getUserName();
  		Thread.sleep(6000);
  	    //Org Settings  	
  		OrgSettingsPage orgSettings = new OrgSettingsPage(driver);
  		//Invite User
  		orgSettings.Inviteuser();
  		Thread.sleep(2000);
  		dp1.getUserName();
  	    //Calling Logout Method
  	    LogoutTest();
    }
    
  //Test Case: User resend invite
    @Test(priority=4)
    public void OrgSettingResendInviteTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Org setting Block User Test");
  		log.info("Test Case: Org setting Block User Test");
  	    //Calling LoginTest Method
  		LoginTest();
  		//Thread.sleep(4000);
  		//DashboardPage
  		DashboardPage dp1 = new DashboardPage(driver);
  	    //Username
  		dp1.getUserName();
  		Thread.sleep(6000);
  	    //Org Settings
  		OrgSettingsPage orgSettings = new OrgSettingsPage(driver);
  		//Block User
  		orgSettings.resendInvite();
  		Thread.sleep(3000);
  		dp1.getUserName();
  	    //Calling Logout Method
  	    LogoutTest();
    }
    
    //Test Case: User Block
    @Test(priority=5)
    public void OrgSettingBlockUserTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Org setting Block User Test");
  		log.info("Test Case: Org setting Block User Test");
  	    //Calling LoginTest Method
  		LoginTest();
  		//Thread.sleep(4000);
  		//DashboardPage
  		DashboardPage dp1 = new DashboardPage(driver);
  	    //Username
  		dp1.getUserName();
  		Thread.sleep(6000);
  	    //Org Settings
  		OrgSettingsPage orgSettings = new OrgSettingsPage(driver);
  		//Block User
  		orgSettings.blockUser();
  		Thread.sleep(3000);
  		dp1.getUserName();
  	    //Calling Logout Method
  	    LogoutTest();
    }
    
  //Test Case: User Unblock
    @Test(priority=6)
    public void OrgSettingUnblockUserTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Org setting Block User Test");
  		log.info("Test Case: Org setting Block User Test");
  	    //Calling LoginTest Method
  		LoginTest();
  		//Thread.sleep(4000);
  		//DashboardPage
  		DashboardPage dp1 = new DashboardPage(driver);
  	    //Username
  		dp1.getUserName();
  		Thread.sleep(6000);
  	    //Org Settings
  		OrgSettingsPage orgSettings = new OrgSettingsPage(driver);
  		//Block User
  		orgSettings.unblockUser();
  		Thread.sleep(3000);
  		dp1.getUserName();
  	    //Calling Logout Method
  	    LogoutTest();
    }
    
    //Test Case: Org Settings: Role Option
    @Test(priority=7)
    public void OrgSettingRoleOptionTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Org setting Role Option Test");
  		log.info("Test Case: Org setting Role Option Test");
  	    //Calling LoginTest Method
  		LoginTest();
  		//Thread.sleep(4000);
  		//DashboardPage
  		DashboardPage dp1 = new DashboardPage(driver);
  	    //Username
  		dp1.getUserName();
  		Thread.sleep(6000);
  		//OrgSettings
  		OrgSettingsPage orgSettings = new OrgSettingsPage(driver);
  		//Role Option
  		orgSettings.roleOption();
  		dp1.getUserName();
  	    //Calling Logout Method
  	    LogoutTest();
    }

    
    //Test Case: User Delete
    @Test(priority=8)
    public void OrgSettingDeleteUserTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Org setting Delete User Test");
  		log.info("Test Case: Org setting Delete User Test");
  	    //Calling LoginTest Method
  		LoginTest();
  		//Thread.sleep(4000);
  		//DashboardPage
  		DashboardPage dp1 = new DashboardPage(driver);
  	    //Username
  		dp1.getUserName();
  		Thread.sleep(6000);
  		//OrgSettings
  		OrgSettingsPage orgSettings = new OrgSettingsPage(driver);
  	    //OrgSettingsTab
//  		orgSettings.getOrgSettingsTab();
//  		Thread.sleep(3000);
  		//Delete User
  		orgSettings.deleteUser();
  		Thread.sleep(3000);
  		dp1.getUserName();
  	    //Calling Logout Method
  	    LogoutTest();
    }
    
    
    //Test Case: Org Settings Alert Option
    @Test(priority=9)
    public void OrgSettingAlertOptionTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Org setting Alert option Test");
  		log.info("Test Case: Org setting Alert option Test");
  	    //Calling LoginTest Method
  		LoginTest();
  		//Thread.sleep(4000);
  		//DashboardPage
  		DashboardPage dp1 = new DashboardPage(driver);
  	    //Username
  		dp1.getUserName();
  		Thread.sleep(6000);
  		//OrgSettings
  		OrgSettingsPage orgSettings = new OrgSettingsPage(driver);
  		//Alert
  		orgSettings.alertOption();
  		dp1.getUserName();
  	    //Calling Logout Method
  	    LogoutTest();
    }

    //Test Case: Org Settings: Billing Option
    @Test(priority=10)
    public void OrgSettingBillingOptionTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Org setting Billing Option Test");
  		log.info("Test Case: Org setting Billing Option Test");
  	    //Calling LoginTest Method
  		LoginTest();
  		//Thread.sleep(4000);
  		//DashboardPage
  		DashboardPage dp1 = new DashboardPage(driver);
  	    //Username
  		dp1.getUserName();
  		Thread.sleep(6000);
  		//Org Settings
  		OrgSettingsPage orgSettings = new OrgSettingsPage(driver);
  		//Billing Options
  		orgSettings.billingOption();
  		dp1.getUserName();
  	    //Calling Logout Method
  	    LogoutTest();
    }
    
    
    @AfterTest
	public void AfterTest()
	{
		driver.quit();
		System.out.println("Browser Closed");
		log.info("Test is Finished and Browser is closed");
		driver=null;
			
	}

}
