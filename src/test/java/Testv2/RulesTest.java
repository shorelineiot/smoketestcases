package Testv2;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import PageObjects.DashboardPage;
import PageObjects.LoginPage;
import PageObjects.RulesPage;
import Resources.base;

public class RulesTest extends base{
	
    public static Logger log =LogManager.getLogger(RulesTest.class.getName());
	
	//Declare WebDriver  Globally
    public WebDriver driver;
    
    /*Login Method*/
    public void LoginTest() throws IOException, InterruptedException
    {
    	//Login Screen
    	LoginPage lp = new LoginPage(driver);
    	//Email Address
    	lp.getEmailAddress();	
    	//RememberMe Checkbox
    	lp.getRememberMeCheckbox();
    	//NextButton
    	lp.getNextButton();
    	//Password
    	lp.getPassword();
    	Thread.sleep(3000);
    	//LoginButton
    	lp.getLoginButton();
    	Thread.sleep(8000);
    }
    
    /*Logout Method*/
    public void LogoutTest() throws InterruptedException
    {
    	//DashboardPage
  		DashboardPage dp1 = new DashboardPage(driver);		
		//Username
		dp1.getUserName().click();
		Thread.sleep(3000);
		//User Logout
		dp1.getUserLogout();
		driver.navigate().refresh();  	
    }
    
    @BeforeTest
	public void BeforeTest() throws IOException, InterruptedException
	{
		System.out.println("Execution started for class 'RulesTest'");
		log.info("Execution started for class 'RulesTest'");
		
		driver=InitializeDriver();			
		driver.manage().window().maximize();
		System.out.println("Initialised Driver and Maximised the Browser Window");
		log.info("Initialised Driver and Maximised the Browser Window");
		
		//Launch
		driver.get(prop.getProperty("testv2url"));
		System.out.println("Launched Admin Portal:"+driver.getCurrentUrl());
		log.info("Launched Admin Portal:"+driver.getCurrentUrl());

	}
    
    //RulesCases
    //TestCase:  Sensor Rules Create
    @Test(priority=1)
    public void SensorRulesCreateTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case:Sensor Rules Create");
  		log.info("Test Case:Sensor Rules Create");
  	    //Calling Login Method
  		LoginTest();
  		Thread.sleep(6000);
  	    //DashboardPage
  		DashboardPage dp1 = new DashboardPage(driver);		
		//Username
		dp1.getUserName();
  		//RulesTab
  		dp1.getRulesTab();
  		//RulesPage
  		RulesPage rp = new RulesPage(driver);
  		//Create Rule Button
  		rp.getCreateRuleButton();
  		//Rule Name
  		rp.getRuleNameField();
  		//HoldOffTime
  		rp.getRuleHoldOffTime();
  		//Rule Level dropdown
  		rp.getRuleLevelDropdown();
  		//Rule Level dropdown select
  		rp.getRuleLevelDropdownSelect();
  		//Select Device Dropdown
  		rp.getRuleDeviceDropdown();
  		Thread.sleep(4000);
  		// Select Device
  		rp.getRuleDeviceSelect();
  		Thread.sleep(3000);
  		//Sensor Dropdown
  		rp.getRuleSensorDropdown();
  		Thread.sleep(2000);
  		//SensorDropdownSelect
  		rp.getRuleSensorSelect();
  		Thread.sleep(2000);
  		//condition Select
  		rp.getRuleConditionDropdown();
  		//Condition Select
  		rp.getRuleConditionSelect();
  		//Condition Value
  		rp.getRuleConditionValue();
  		//Next Button
  		rp.getNextButton();
  		//Save Rule
  		Thread.sleep(2000);
  		rp.getSaveRuleButton();
  		Thread.sleep(2000);
  	    //Toaster
  		//rp.getToasterMessageRuleCreate();
  	    //Calling Logout Method
  		LogoutTest();
  		Thread.sleep(3000);
  	    driver.navigate().refresh();
    }
    
    //TestCase:Sensor  Rules Delete
    @Test(priority=2)
    public void SensorRulesDeleteTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case:Sensor Rules Delete");
  		log.info("Test Case: Sensor Rules Delete");
  	    
  		//Calling Login Method
  		LoginTest();
  		//Thread.sleep(6000);
  		//DashboardPage
  		DashboardPage dp = new DashboardPage(driver);		
		//Username
		dp.getUserName();
  		//RulesTab
  		dp.getRulesTab();
  		Thread.sleep(3000);
  		//RulesPage
  		RulesPage rp = new RulesPage(driver);
        //Ssarch Rule
  		//rp.getSearchRuleField();
  		//Thread.sleep(3000);
  		//Delete
  		rp.getDeleteSearchRule();
  		
  		//Apply Button
  		rp.getApplyButtonConfirmation();
  		Thread.sleep(3000);
  		//Toaster
  		//rp.getToasterMessageRuleDelete();
  	    //Calling Logout Method
  		LogoutTest();
  		driver.navigate().refresh();
    }
    
    //TestCase:Sensor Rules Update
    @Test(priority=3)
    public void  SensorRulesUpdateTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Rules Update");
  		log.info("Test Case: Sensor Rules Update");
  	    //Calling Login Method
  		LoginTest();
  		//Thread.sleep(6000);
  		//DashboardPage
  		DashboardPage dp = new DashboardPage(driver);
  	    //Username
  		dp.getUserName();
  	    //RulesTab
  		dp.getRulesTab();
  		//RulesPage
  		RulesPage rp = new RulesPage(driver);
  	    //Ssarch Rule
  		//rp.getSearchRuleFieldUpdate();
  		Thread.sleep(3000);
        //First Rule Dropdown
  		rp.getSecondRuleDropdowwn();
  		Thread.sleep(4000);
  		//First Rule Dropdown Select
  		rp.getSecondRuleDropdowwnSelect();
  		//Apply Button
  		rp.getApplyButtonConfirmation();
  		//Toaster
  		//rp.getToasterMessageRuleUpdate();
  	    //Calling Logout Method
  		LogoutTest();
    }
    
    @AfterTest
	public void AfterTest()
	{
		driver.quit();
		System.out.println("Browser Closed");
		log.info("Test is Finished and Browser is closed");
		driver=null;
			
	}

}
