package Testv2;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import PageObjects.DashboardPage;
import PageObjects.FirmwarePage;
import PageObjects.LoginPage;
import Resources.base;

public class FirmwareTest extends base{
	
public static Logger log =LogManager.getLogger(DeviceListTest.class.getName());
	
	//Declare WebDriver  Globally
    public WebDriver driver;
    
    /*Login Method*/
    public void LoginTest() throws IOException, InterruptedException
    {
    	//Login Screen
    	LoginPage lp = new LoginPage(driver);
    	//Email Address
    	lp.getEmailAddress();	
    	//RememberMe Checkbox
    	lp.getRememberMeCheckbox();
    	//NextButton
    	lp.getNextButton();
    	//Password
    	lp.getPassword();
    	Thread.sleep(3000);
    	//LoginButton
    	lp.getLoginButton();
    	Thread.sleep(8000);
    }
    
    /*Logout Method*/
    public void LogoutTest() throws InterruptedException
    {
    	//DashboardPage
  		DashboardPage dp1 = new DashboardPage(driver);		
		//Username
		dp1.getUserName().click();
		//User Logout
		dp1.getUserLogout();
		driver.navigate().refresh();  	
    }
    
    
    
    @BeforeTest
	public void BeforeTest() throws IOException, InterruptedException
	{
		System.out.println("Execution started for class 'FirmwareTest'");
		log.info("Execution started for class 'FirmwareTest'");
		
		driver=InitializeDriver();			
		driver.manage().window().maximize();
		System.out.println("Initialised Driver and Maximised the Browser Window");
		log.info("Initialised Driver and Maximised the Browser Window");
		
		//Launch
		driver.get(prop.getProperty("testv2url"));
		System.out.println("Launched Admin Portal:"+driver.getCurrentUrl());
		log.info("Launched Admin Portal:"+driver.getCurrentUrl());

	}
    
    //Firmware Cases
    //TestCase: Firmware Tab Click Test
    @Test(priority=1)
    public void FirmwareTabClickTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Firmware Tab Click Test");
  		log.info("Test Case: Firmware Tab Click Test");
  		
  		//Calling LoginTest Method
  		LoginTest();
  		//DashboardPage
  	  	DashboardPage dp1 = new DashboardPage(driver);
  	    //Username
  		dp1.getUserName();
  		//Thread.sleep(8000);
  		//Devices Tab
  		dp1.getFirmwareTab();
  		Thread.sleep(4000);
  		//driver.navigate().refresh();
  		//Thread.sleep(4000);
  	    //Calling Logout Method
  		LogoutTest();
    }
    
    //TestCase: Upload Firmware Test
    @Test(priority=2 , enabled =false)
    public void UploadFirmwareTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Upload Firmware Test");
  		log.info("Test Case: Upload Firmware Test");
  		
  		//FirmwarePage
  		FirmwarePage fp = new FirmwarePage(driver);
  		//Upload Firmware Button
  		fp.getUploadFirmwareButton();
  		Thread.sleep(4000);
  		//ChooseFileFirmware
  		fp.getChooseFileFirmware();
  	    //Release Notes
  		//fp.getChooseFileReleaseNotes(downloadFilePath);
  		//Firmware Name
  		fp.getFirmwareName();
  		//Manifest Version
  		fp.getManifestVersion();
  		//Desciption
  		fp.getFirmwareDescription();
  		//ModelDropdown
  		fp.getModelDropdown();
  		//Select Model
  		fp.getSelectModelICastSense();
  		//Upload Button
  		fp.getUploadButton();
  	    //Calling Logout Method
  		LogoutTest();
    }
    
    //TestCase: Attach Firmware through firmware Page Test
    @Test(priority=3)
    public void AttachFirmwareFirmwarePageTest() throws IOException, InterruptedException 
    {
    	System.out.println("Test Case: Attach Firmware through firmware Page Test");
  		log.info("Test Case: Attach Firmware through firmware Page Test");
  		
  		//Calling LoginTest Method
  		LoginTest();
  		//DashboardPage
  	  	DashboardPage dp1 = new DashboardPage(driver);
  	    //Username
  		dp1.getUserName();
  		//Thread.sleep(8000);
  		//Firmware Tab
  		dp1.getFirmwareTab();
  		Thread.sleep(4000);
  		driver.navigate().refresh();
  		Thread.sleep(4000);
  		//FirmwarePage
  		FirmwarePage fp = new FirmwarePage(driver);
  		//Action + Icon 1st firmware
  		fp.getActionPlusIcon();
  		driver.navigate().refresh();
  		Thread.sleep(3000);  		
  		//Checkbox 1st Device
  		fp.getSelectDeviceCheckbox();
  		//Update Firmware Button
  		fp.getUpdateFirmwareButton();
  		Thread.sleep(3000);
  	    //ToasterMessage
  	    //dp3.getToastMsg();
  	    //Calling Logout Method
  		LogoutTest();
    }
    
    
    
    /*
    @AfterTest
   	public void AfterTest()
   	{
   		driver.quit();
   		System.out.println("Browser Closed");
   		log.info("Test is Finished and Browser is closed");
   		driver=null;
   			
   	}
	*/
	

}
