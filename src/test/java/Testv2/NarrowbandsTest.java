package Testv2;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import PageObjects.DashboardPage;
import PageObjects.DeviceDataPage;
import PageObjects.DevicesPage;
import PageObjects.LoginPage;
import Resources.base;

public class NarrowbandsTest extends base{
	
public static Logger log =LogManager.getLogger(NarrowbandsTest.class.getName());
	
	//Declare WebDriver  Globally
    public WebDriver driver;
    
    /*Login Method*/
    public void LoginTest() throws IOException, InterruptedException
    {
    	//Login Screen
    	LoginPage lp = new LoginPage(driver);
    	//Email Address
    	lp.getEmailAddress();	
    	//RememberMe Checkbox
    	lp.getRememberMeCheckbox();
    	//NextButton
    	lp.getNextButton();
    	//Password
    	lp.getPassword();
    	Thread.sleep(3000);
    	//LoginButton
    	lp.getLoginButton();
    	Thread.sleep(8000);
    }
    
    /*Logout Method*/
    public void LogoutTest() throws InterruptedException
    {
    	//DashboardPage
  		DashboardPage dp1 = new DashboardPage(driver);		
		//Username
		dp1.getUserName().click();
		//User Logout
		dp1.getUserLogout();
		driver.navigate().refresh();  	
    }
    
    
    @BeforeTest
	public void BeforeTest() throws IOException, InterruptedException
	{
		System.out.println("Execution started for class 'NarrowbandsTest'");
		log.info("Execution started for class 'OrgSettingsTest'");
		
		driver=InitializeDriver();			
		driver.manage().window().maximize();
		System.out.println("Initialised Driver and Maximised the Browser Window");
		log.info("Initialised Driver and Maximised the Browser Window");
		
		//Launch
		driver.get(prop.getProperty("testv2url"));
		System.out.println("Launched Admin Portal:"+driver.getCurrentUrl());
		log.info("Launched Admin Portal:"+driver.getCurrentUrl());

	}
    /*
    //Test Case: Autoconfig: Create Narrowbands
    @Test
    public void CreateNarrowbands() throws IOException, InterruptedException
    {
    	System.out.println("Test Case: Autoconfig: Create Narrowbands");
  		log.info("Test Case: Autoconfig: Create Narrowbands ");
  		
  	    //Calling LoginTest Method
  		LoginTest();
  		//DashboardPage
  	  	DashboardPage dp1 = new DashboardPage(driver);
  	    //Username
  		dp1.getUserName();
  		//Thread.sleep(8000);
  		//Devices Tab
  		dp1.getDevicesTab();
  		Thread.sleep(4000);
  		driver.navigate().refresh();
  		Thread.sleep(4000);
  		
  	    //DevicesPage
  		DevicesPage dp2 = new DevicesPage(driver);
  		//First Device
  	    dp2.getFirstDevice();
  	    
  	    //Device Data Page
  	    DeviceDataPage ddp = new DeviceDataPage(driver);
  	    //Add SensorButton
  	    ddp.getAddSensorButton();
  	    //Narrowbands Option
  	    ddp.getAddSensorNarrowbandsOption();
  	    
  	    //Narrowband Settings
  	    //Class
  	    ddp.getComponentClass();
  	    //Select Class 
  	    ddp.getElectricMotors_Generators();
  	    //Subclass
  	    ddp.getComponentSubClass();
  	    //Select Subclass
  	    ddp.getACMotor();
  	    //Manufacturer
  	    ddp.getSelectManufacturer();
  	    //Manufacturer GE_Select
  	    ddp.getManufacturer_GE();
  	    //Model
  	    ddp.getSelectModel();
  	    //Model-5KS449BL308ARP
  	    ddp.getModel_GE_DPO_5KS449BL308ARP();
  	    //Next Button
  	    ddp.getNextButton();
  	    Thread.sleep(3000);
  	    //Next Button
  	    ddp.getNextButton();
  	    Thread.sleep(3000);
	    //Save Button
	    ddp.getSaveButton();
	    Thread.sleep(15000);
	    driver.navigate().refresh();
  	    //Calling Logout Method
  		LogoutTest();   	    
    }
    */
    /*
    //Test Case: Autoconfig: Edit Narrowbands
    @Test
    public void EditNarrowbands() throws IOException, InterruptedException
    {
    	System.out.println("Test Case: Autoconfig: Edit Narrowbands");
  		log.info("Test Case: Autoconfig: Edit Narrowbands ");
  		
  	    //Calling LoginTest Method
  		LoginTest();
  		//DashboardPage
  	  	DashboardPage dp1 = new DashboardPage(driver);
  	    //Username
  		dp1.getUserName();
  		//Thread.sleep(8000);
  		//Devices Tab
  		dp1.getDevicesTab();
  		Thread.sleep(4000);
  		driver.navigate().refresh();
  		Thread.sleep(4000);
  		
  	    //DevicesPage
  		DevicesPage dp2 = new DevicesPage(driver);
  		//First Device
  	    dp2.getFirstDevice();
  	    
  	    //Device Data Page
  	    DeviceDataPage ddp = new DeviceDataPage(driver);
  	    //Add SensorButton
  	    ddp.getAddSensorButton();
  	    //Narrowbands Option
  	    ddp.getAddSensorNarrowbandsOption();
  	    
  	    //Narrowband Settings
  	    //Edit Icon of First Narrowband
  	    ddp.getEditIconFirstNarrowband();
  	    //Edit Low Threshold
  	    ddp.getEditLowValueFirstNarrowband();
  	    //Edit Moderate Threshold
  	    ddp.getEditModerateValueFirstNarrowband();
  	    //Edit High Threshold
  	    ddp.getEditHighValueFirstNarrowband();
  	    //Edit Critical Threshold
  	    ddp.getEditCriticalValueFirstNarrowband();
  	    //Save edit
  	    ddp.getSaveEditOption();
  	    //Save Button
	    ddp.getSaveButton();
	    Thread.sleep(15000);
	    driver.navigate().refresh();
  	    //Calling Logout Method
  		LogoutTest(); 
  	    
    }
    
    //Test Case: Autoconfig: Delete Narrowbands
    @Test
    public void DeleteNarrowbands() throws IOException, InterruptedException
    {
    	System.out.println("Test Case: Autoconfig: Delete Narrowbands");
  		log.info("Test Case: Autoconfig: delete Narrowbands ");
  		
  	    //Calling LoginTest Method
  		LoginTest();
  		//DashboardPage
  	  	DashboardPage dp1 = new DashboardPage(driver);
  	    //Username
  		dp1.getUserName();
  		//Thread.sleep(8000);
  		//Devices Tab
  		dp1.getDevicesTab();
  		Thread.sleep(4000);
  		driver.navigate().refresh();
  		Thread.sleep(4000);
  		
  	    //DevicesPage
  		DevicesPage dp2 = new DevicesPage(driver);
  		//First Device
  	    dp2.getFirstDevice();
  	    
  	    //Device Data Page
  	    DeviceDataPage ddp = new DeviceDataPage(driver);
  	    //Add SensorButton
  	    ddp.getAddSensorButton();
  	    //Narrowbands Option
  	    ddp.getAddSensorNarrowbandsOption();
  	    
  	    //Narrowband Settings
  	    //Delete Icon of First Narrowband
  	    ddp.getDeleteNarrowbandOption();
  	    //Delete Button
  	    ddp.getDeleteButton();
  	    //Save Button
	    ddp.getSaveButton();
	    Thread.sleep(15000);
	    driver.navigate().refresh();
  	    //Calling Logout Method
  		LogoutTest();  	    
    }    
    */
    //Test Case: Manual Narrowbands:Create
    @Test
    public void ManualNarrowbandsCreate() throws IOException, InterruptedException
    {
    	System.out.println("Test Case: Manual Narrowbands:Create");
  		log.info("Test Case: Manual Narrowbands:Create");
  		
  	    //Calling LoginTest Method
  		LoginTest();
  		//DashboardPage
  	  	DashboardPage dp1 = new DashboardPage(driver);
  	    //Username
  		dp1.getUserName();
  		//Thread.sleep(8000);
  		//Devices Tab
  		dp1.getDevicesTab();
  		Thread.sleep(4000);
  		driver.navigate().refresh();
  		Thread.sleep(4000);
  		
  	    //DevicesPage
  		DevicesPage dp2 = new DevicesPage(driver);
  		//First Device
  	    dp2.getFirstDevice();
  	    
  	    //Device Data Page
  	    DeviceDataPage ddp = new DeviceDataPage(driver);
  	    //Add SensorButton
  	    ddp.getAddSensorButton();
  	    //Narrowbands Option
  	    ddp.getAddSensorNarrowbandsOption();
  	    
  	    //Narrowband Settings
  	    //Manual Radio Button
  	    ddp.getManualRadioButton();
  	    //Manual NB Name
  	    ddp.getManualNarrowbandName();
  	    //Manual NB Min Frequency
  	    ddp.getManualNarrowbandMinFrequency();
  	    //Manual NB Max Frequency
  	    ddp.getManualNarrowbandMaxFrequency();
  	    //Save Button
	    ddp.getSaveButton();
	    Thread.sleep(15000);
	    driver.navigate().refresh();
  	    //Calling Logout Method
  		LogoutTest();  	    
    }
    


    /*
    @AfterTest
	public void AfterTest()
	{
		driver.quit();
		System.out.println("Browser Closed");
		log.info("Test is Finished and Browser is closed");
		driver=null;
			
	}
    */
}
