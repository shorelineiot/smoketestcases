package Testv2;

import java.io.IOException;
import java.time.Duration;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.testng.annotations.AfterTest;

import PageObjects.DashboardPage;
import PageObjects.DeviceDataPage;
import PageObjects.DeviceSettingsPage;
import PageObjects.DevicesPage;
import PageObjects.LoginPage;
import Resources.base;

public class PowertrainTest extends base{
	
public static Logger log =LogManager.getLogger(PowertrainTest.class.getName());
	
	//Declare WebDriver  Globally
    public WebDriver driver;
    
    /*Login Method*/
    public void LoginTest() throws IOException, InterruptedException
    {
    	//Login Screen
    	LoginPage lp = new LoginPage(driver);
    	//Email Address
    	lp.getEmailAddress();	
    	//RememberMe Checkbox
    	lp.getRememberMeCheckbox();
    	//NextButton
    	lp.getNextButton();
    	//Password
    	lp.getPassword();
    	Thread.sleep(3000);
    	//LoginButton
    	lp.getLoginButton();
    	Thread.sleep(8000);
    }
    
    /*Logout Method*/
    public void LogoutTest() throws InterruptedException
    {
    	//DashboardPage
  		DashboardPage dp1 = new DashboardPage(driver);		
		//Username
		dp1.getUserName().click();
		//User Logout
		dp1.getUserLogout();
		driver.navigate().refresh();  	
    }
    
    @BeforeTest
	public void BeforeTest() throws IOException, InterruptedException
	{
		System.out.println("Execution started for class 'PowertrainTest'");
		log.info("Execution started for class 'PowertrainTest'");
		
		driver=InitializeDriver();			
		driver.manage().window().maximize();
		System.out.println("Initialised Driver and Maximised the Browser Window");
		log.info("Initialised Driver and Maximised the Browser Window");
		
		//Launch
		driver.get(prop.getProperty("testv2url"));
		System.out.println("Launched Admin Portal:"+driver.getCurrentUrl());
		log.info("Launched Admin Portal:"+driver.getCurrentUrl());

	}
   
	//TestCase: Powertrain Create Test
    @Test(priority=1)
    public void PowertrainCreateTest() throws IOException, InterruptedException
    {
    	System.out.println("Test Case: Powertrain Create Test");
  		log.info("Test Case: Powertrain Create Test");
  		
         driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(90));
  		
  	    //Calling LoginTest Method
  		LoginTest();
  		//DashboardPage
  	  	DashboardPage dp1 = new DashboardPage(driver);
  	    //Username
  		dp1.getUserName();  		
  		//Thread.sleep(8000);
  		//Devices Tab
  		dp1.getDevicesTab();
  		
  		//DevicesPage
  		DevicesPage dp2 = new DevicesPage(driver);
  	    //Username
  		dp1.getUserName();
  		//Thread.sleep(4000);
  		//Create Device Button
  		dp2.getCreateDeviceButton();
  	    //Select Powertrain
  		dp2.getSelectPowertrainCreation();
  		//Powertrain Name
  		dp2.getPowertrainName();
  		//Powertrain RPM Min
  		dp2.getPowertrainRPM_min();
  	    //Powertrain RPM Max
  		dp2.getPowertrainRPM_max();
  		//PrimeMover Name
  		dp2.getPrimeMoverName();
  		//Prime Mover Serial No
  		dp2.getPrimeMoverSerialNo();
  		//PrimeMover Component Class Dropdown
  		dp2.getPMComponentClassDropdown();
  		//PrimeMover Component Class Select
  		dp2.getPMComponentClassSelect();
  		//PrimeMover Component Sub Class Dropdown
  		dp2.getPMComponentSubClassDropdown();
  	    //PrimeMover Component Sub Class Select
  		dp2.getPMComponentSubClassSelect();
  		//PrimeMover Component Manufacturer
  		dp2.getPMComponentManufacturerDropdown();
  		//PrimeMover Component Manufacturer Select
  		dp2.getPMComponentManufacturerSelect();
  	    //PrimeMover Component Model
  		dp2.getPMComponentModelDropdown();
  		//PrimeMover Component Model Select
  		dp2.getPMComponentModelSelect();
  		Thread.sleep(4000);
  		//Save Button
  		dp2.getSaveButton();
  		Thread.sleep(15000);
  		driver.navigate().refresh();
  	    //Calling Logout Method
  		LogoutTest();  		 		
    }
    
    //Test Case: Powertrain Edit Test
    @Test(priority=2)
    public void PowertrainEditTest() throws IOException, InterruptedException
    {
    	System.out.println("Test Case: Powertrain Edit Test");
  		log.info("Test Case: Powertrain Edit Test");
  		
         driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(90));
  		
  	    //Calling LoginTest Method
  		LoginTest();
  		//DashboardPage
  	  	DashboardPage dp1 = new DashboardPage(driver);
  	    //Username
  		dp1.getUserName();  		
  		//Thread.sleep(8000);
  		//Devices Tab
  		dp1.getDevicesTab();
  		
  		//DevicesPage
  		DevicesPage dp2 = new DevicesPage(driver);
  	    //Username
  		dp1.getUserName();
  		//Thread.sleep(4000);
  		//Powertrain Search
  		dp2.getPowertrainSearch();
  		Thread.sleep(3000);
  		//PowertrainEdit
  		dp2.getPowertrainEdit();
  		//Powertrain  Name Update
  		dp2.getPowertrainName_Update();
  		//Powertrain Min Update
  		dp2.getPowertrainRPM_min_Update();
  		//Powertrain Max Update
  		dp2.getPowertrainRPM_max_Update();
  		Thread.sleep(4000);
  		//Save Button
  		dp2.getSaveButton();
  		Thread.sleep(15000);
  		driver.navigate().refresh();
  	    //Calling Logout Method
  		LogoutTest(); 
    }
       
    //TestCase: Powertrain Delete Option Test
    @Test(priority=3)
    public void PowertrainDeleteTest() throws IOException, InterruptedException
    {
    	System.out.println("Test Case: Powertrain Delete Option Disable Test");
  		log.info("Test Case: Powertrain Edit Test");
  		
         driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(90));
  		
  	    //Calling LoginTest Method
  		LoginTest();
  		//DashboardPage
  	  	DashboardPage dp1 = new DashboardPage(driver);
  	    //Username
  		dp1.getUserName();  		
  		//Thread.sleep(8000);
  		//Devices Tab
  		dp1.getDevicesTab();
  		
  		//DevicesPage
  		DevicesPage dp2 = new DevicesPage(driver);
  	    //Username
  		dp1.getUserName();
  		//Thread.sleep(4000);
  		//Powertrain Search
  		dp2.getPowertrainDeviceSearch();
  		Thread.sleep(3000);
  		//Powertrain Device
  		dp2.getPowertrainDevice();
  	    //DeviceDataPage
  	    DeviceDataPage dp3 = new DeviceDataPage(driver);
  	    //Settings Icon
  	    dp3.getSettingsIcon();
  	    //Device Settings Page
  	    DeviceSettingsPage dp4 = new DeviceSettingsPage(driver);
  	    //Powertrain Device Delete
  	    dp4.getPowertrainDeviceDelete();
  	    Thread.sleep(3000);
  	    //Calling Logout Method
  		LogoutTest(); 
    }
  
    //TestCase: Powertrain Device Autoconfig Test
    @Test(priority=4)
    public void PowertrainDeviceAutoconfigTest() throws IOException, InterruptedException
    {
    	System.out.println("Test Case: Powertrain Device Autoconfig Test");
  		log.info("Test Case: Powertrain Device Autoconfig Test");
  		
         driver.manage().timeouts().pageLoadTimeout(Duration.ofSeconds(90));
  		
  	    //Calling LoginTest Method
  		LoginTest();
  		//DashboardPage
  	  	DashboardPage dp1 = new DashboardPage(driver);
  	    //Username
  		dp1.getUserName();  		
  		//Thread.sleep(8000);
  		//Devices Tab
  		dp1.getDevicesTab();
  		
  		//DevicesPage
  		DevicesPage dp2 = new DevicesPage(driver);
  	    //Username
  		dp1.getUserName();
  		//Thread.sleep(4000);
  		//Powertrain Search
  		dp2.getPowertrainDeviceSearch();
  		Thread.sleep(3000);
  		//Powertrain Device
  		dp2.getPowertrainDevice();
  	    //DeviceDataPage
  	    DeviceDataPage dp3 = new DeviceDataPage(driver);
  	    //Add SensorButton
  	    dp3.getAddSensorButton();
  	    //Narrowbands Option
  	    dp3.getAddSensorNarrowbandsOption();
  	    
  	    //Narrowband Settings
  	    //Edit Icon of First Narrowband
  	    dp3.getEditIconFirstNarrowband();
  	    //Edit Low Threshold
  	    dp3.getEditLowValueFirstNarrowband();
  	    //Edit Moderate Threshold
  	    dp3.getEditModerateValueFirstNarrowband();
  	    //Edit High Threshold
  	    dp3.getEditHighValueFirstNarrowband();
  	    //Edit Critical Threshold
  	    dp3.getEditCriticalValueFirstNarrowband();
  	    //Save edit
  	    dp3.getSaveEditOption();
  	    //Save Button
	    dp3.getSaveButton();
	    Thread.sleep(15000);
	    driver.navigate().refresh();
  	    //Calling Logout Method
  		LogoutTest();
  	     
    }
    	
    @AfterTest
	public void AfterTest()
	{
		driver.quit();
		System.out.println("Browser Closed");
		log.info("Test is Finished and Browser is closed");
		driver=null;
			
	}

}
