package Testv2;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;

import Resources.ExtentReporterNG;
import Resources.base;

public class BaseTest extends base{
	
	//Declare WebDriver  Globally
    public  WebDriver driver;
	
	// Extent Report variables.
	public static ExtentTest test;
	public static ExtentReports extent;
	

	
	@BeforeTest
	public void BeforeTest() throws IOException, InterruptedException
	{
		//System.out.println("Execution started for class 'UserLoginTest'");
		//log.info("Execution started for class 'UserLoginTest'");
		
		driver=InitializeDriver();			
		driver.manage().window().maximize();
		System.out.println("Initialised Driver and Maximised the Browser Window");
		//log.info("Initialised Driver and Maximised the Browser Window");
		
		//Launch
		driver.get(prop.getProperty("testv2url"));
		System.out.println("Launched Admin Portal:"+driver.getCurrentUrl());
		//log.info("Launched Admin Portal:"+driver.getCurrentUrl());

	}	
	
	@AfterTest
	public void AfterTest()
	{
		driver.quit();
		System.out.println("Browser Closed");
		//log.info("Test is Finished and Browser is closed");
		driver=null;
			
	}
    /*
	public void setup(String testName) {
		extent = ExtentReporterNG.getReportObject();
		parentTest = extent.createTest(testName);
	}
    */
}
