package PageObjects;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import Resources.DataFile;
import Resources.base;

public class DeviceDataPage extends base{
	
    public static Logger log =LogManager.getLogger(DeviceDataPage.class.getName());
	
	//Declare WebDriver Globally
	public WebDriver driver;
		
	//Constructor
	public DeviceDataPage(WebDriver driver)
	{
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}

	//Page Objects
	@FindBy(how = How.XPATH, using="//*[@id='root']/div[2]/div/div[2]/div[2]/div[1]/div/div/div[4]/a/button")
	WebElement SettingsIcon;
	
	@FindBy(how = How.XPATH, using="//span[normalize-space()='Add Sensor']")
	WebElement AddSensorButton;
	
	@FindBy(how = How.XPATH, using="//div[contains(text(),'Narrowband')]")
	WebElement AddSensorNarrowbandsOption;
	
	@FindBy(how = How.XPATH, using="//div[@id='mui-component-select-class']")
	WebElement ComponentClass;
	
	@FindBy(how = How.XPATH, using="//li[normalize-space()='Electric motors/generators']")
	WebElement ElectricMotors_Generators;
	
	@FindBy(how = How.XPATH, using="//div[@id='mui-component-select-subclass']")
	WebElement ComponentSubClass;
	
	@FindBy(how = How.XPATH, using="//li[normalize-space()='AC motor']")
	WebElement ACMotor;
	
	@FindBy(how = How.XPATH, using="//div[@id='mui-component-select-manufacturer']")
	WebElement SelectManufacturer;
	
	@FindBy(how = How.XPATH, using="//li[normalize-space()='General Electric']")
	WebElement Manufacturer_GE;
	
	@FindBy(how = How.XPATH, using="//div[@id='mui-component-select-model']")
	WebElement SelectModel;
	
	@FindBy(how = How.XPATH, using="//li[normalize-space()='5KS449BL308ARP']")
	WebElement Model_GE_DPO_5KS449BL308ARP;
	
	@FindBy(how = How.XPATH, using="//span[normalize-space()='Next']")
	WebElement NextButton;
	
	@FindBy(how = How.XPATH, using="//span[normalize-space()='Save']")
	WebElement SaveButton;	
	
	@FindBy(how = How.XPATH, using="//tbody/tr[1]/td[7]/div[1]/button[1]/span[1]/div[1]//*[name()='svg']")
	WebElement EditIconFirstNarrowband;
	
	@FindBy(how = How.XPATH, using="//input[@placeholder='Low']")
	WebElement EditLowValueFirstNarrowband;
	
	@FindBy(how = How.XPATH, using="//input[@placeholder='Moderate']")
	WebElement EditModerateValueFirstNarrowband;
	
	@FindBy(how = How.XPATH, using="//input[@placeholder='High']")
	WebElement EditHighValueFirstNarrowband;
	
	@FindBy(how = How.XPATH, using="//input[@placeholder='Critical']")
	WebElement EditCriticalValueFirstNarrowband;
	
	@FindBy(how = How.XPATH, using="//*[name()='g' and @id='Maps:_ok']")
	WebElement SaveEditOption;
	
	@FindBy(how = How.XPATH, using="//tbody/tr[1]/td[7]/div[1]/button[2]/span[1]")
	WebElement DeleteNarrowbandOption;
	
	@FindBy(how = How.XPATH, using="//span[normalize-space()='Delete']")
	WebElement DeleteButton;
	
	//@FindBy(how = How.XPATH, using="/html/body/div[74]/div[3]/div[2]/div[1]/div/div[1]/span/span[1]")
	@FindBy(how = How.XPATH, using="//*[@type='radio'][@name='config_type'][@value='manual']")
	//@FindBy(how = How.XPATH, using="/html[1]/body[1]/div[72]/div[3]/div[2]/div[1]/div[1]/div[1]/span[1]/span[1]/div[1]/*[name()='svg'][1]")
	WebElement ManualRadioButton;
	
	@FindBy(how = How.XPATH, using="//input[@aria-label='Narrowband Name:']")
	WebElement ManualNarrowbandName;
	
	@FindBy(how = How.XPATH, using="//input[@aria-label='Min Frequency(Hz)']")
	WebElement ManualNarrowbandMinFrequency;
	
	@FindBy(how = How.XPATH, using="//input[@aria-label='Max Frequency(Hz)']")
	WebElement ManualNarrowbandMaxFrequency;
	
	//Page Methods
	public WebElement getSettingsIcon()
	{
		waitForElementToBeVisible(SettingsIcon, 30, 2);
		SettingsIcon.click();
		System.out.println("Clicked on SettingsIcon");
		log.info("Clicked on SettingsIcon");
		return SettingsIcon;		
	}

	public WebElement getAddSensorButton()
	{
		waitForElementToBeVisible(AddSensorButton, 30, 2);
		AddSensorButton.click();
		System.out.println("Clicked on AddSensorButton");
		log.info("Clicked on AddSensorButton");
		return AddSensorButton;				
	}
	
	public WebElement getAddSensorNarrowbandsOption()
	{
		waitForElementToBeVisible(AddSensorNarrowbandsOption, 30, 2);
		AddSensorNarrowbandsOption.click();
		System.out.println("Clicked on AddSensorNarrowbandsOption");
		log.info("Clicked on AddSensorNarrowbandsOption");
		return AddSensorNarrowbandsOption;				
	}
	
	public WebElement getComponentClass()
	{
		waitForElementToBeVisible(ComponentClass, 30, 2);
		ComponentClass.click();
		System.out.println("Clicked on ComponentClass");
		log.info("Clicked on ComponentClass");
		return ComponentClass;				
	}
	
	public WebElement getElectricMotors_Generators()
	{
		waitForElementToBeVisible(ElectricMotors_Generators, 30, 2);
		ElectricMotors_Generators.click();
		System.out.println("Clicked on ElectricMotors_Generators");
		log.info("Clicked on ElectricMotors_Generators");
		return ElectricMotors_Generators;				
	}
	
	public WebElement getComponentSubClass()
	{
		waitForElementToBeVisible(ComponentSubClass, 30, 2);
		ComponentSubClass.click();
		System.out.println("Clicked on ComponentSubClass");
		log.info("Clicked on ComponentSubClass");
		return ComponentSubClass;				
	}
	
	public WebElement getSelectManufacturer()
	{
		waitForElementToBeVisible(SelectManufacturer, 30, 2);
		SelectManufacturer.click();
		System.out.println("Clicked on SelectManufacturer");
		log.info("Clicked on SelectManufacturer");
		return SelectManufacturer;				
	}
	
	public WebElement getACMotor()
	{
		waitForElementToBeVisible(ACMotor, 30, 2);
		ACMotor.click();
		System.out.println("Clicked on ACMotor");
		log.info("Clicked on ACMotor");
		return ACMotor;				
	}
	
	public WebElement getManufacturer_GE()
	{
		waitForElementToBeVisible(Manufacturer_GE, 30, 2);
		Manufacturer_GE.click();
		System.out.println("Clicked on Manufacturer_GE");
		log.info("Clicked on Manufacturer_GE");
		return Manufacturer_GE;				
	}
	
	public WebElement getSelectModel()
	{
		waitForElementToBeVisible(SelectModel, 30, 2);
		SelectModel.click();
		System.out.println("Clicked on SelectModel");
		log.info("Clicked on SelectModel");
		return SelectModel;				
	}
	
	public WebElement getModel_GE_DPO_5KS449BL308ARP()
	{
		waitForElementToBeVisible(Model_GE_DPO_5KS449BL308ARP, 30, 2);
		Model_GE_DPO_5KS449BL308ARP.click();
		System.out.println("Clicked on Model_GE_DPO_5KS449BL308ARP");
		log.info("Clicked on Model_GE_DPO_5KS449BL308ARP");
		return Model_GE_DPO_5KS449BL308ARP;				
	}
	
	public WebElement getNextButton()
	{
		waitForElementToBeVisible(NextButton, 30, 2);
		NextButton.click();
		System.out.println("Clicked on NextButton");
		log.info("Clicked on NextButton");
		return NextButton;				
	}
	
	public WebElement getSaveButton()
	{
		waitForElementToBeVisible(SaveButton, 30, 2);
		SaveButton.click();
		System.out.println("Clicked on SaveButton");
		log.info("Clicked on SaveButton");
		return SaveButton;				
	}
	
	public WebElement getEditIconFirstNarrowband()
	{
		waitForElementToBeVisible(EditIconFirstNarrowband, 30, 2);
		EditIconFirstNarrowband.click();
		System.out.println("Clicked on EditIconFirstNarrowband");
		log.info("Clicked on EditIconFirstNarrowband");
		return EditIconFirstNarrowband;				
	}
	
	public WebElement getEditLowValueFirstNarrowband() throws IOException
	{
		EditLowValueFirstNarrowband.click();
		EditLowValueFirstNarrowband.sendKeys(Keys.chord(Keys.CONTROL,"a", Keys.DELETE));
		DataFile df = new DataFile();
		ArrayList<String> data =df.getData("LowValueFirstNarrowband");		
		EditLowValueFirstNarrowband.sendKeys(data.get(1));
		System.out.println("Entered EditLowValueFirstNarrowband:"+EditLowValueFirstNarrowband.getAttribute("value"));
		log.info("Entered EditLowValueFirstNarrowband:"+EditLowValueFirstNarrowband.getAttribute("value"));
		return EditLowValueFirstNarrowband;
	}
	
	public WebElement getEditModerateValueFirstNarrowband() throws IOException
	{
		EditModerateValueFirstNarrowband.click();
		EditModerateValueFirstNarrowband.sendKeys(Keys.chord(Keys.CONTROL,"a", Keys.DELETE));
		DataFile df = new DataFile();		
		ArrayList<String> data =df.getData("ModerateValueFirstNarrowband");				
		EditModerateValueFirstNarrowband.sendKeys(data.get(1));
		System.out.println("Entered EditModerateValueFirstNarrowband:"+EditModerateValueFirstNarrowband.getAttribute("value"));
		log.info("Entered EditModerateValueFirstNarrowband:"+EditModerateValueFirstNarrowband.getAttribute("value"));
		return EditModerateValueFirstNarrowband;
	}
	
	public WebElement getEditHighValueFirstNarrowband() throws IOException
	{
		EditHighValueFirstNarrowband.click();
		EditHighValueFirstNarrowband.sendKeys(Keys.chord(Keys.CONTROL,"a", Keys.DELETE));
		DataFile df = new DataFile();
		ArrayList<String> data =df.getData("HighValueFirstNarrowband");		
		EditHighValueFirstNarrowband.sendKeys(data.get(1));
		System.out.println("Entered EditHighValueFirstNarrowband:"+EditHighValueFirstNarrowband.getAttribute("value"));
		log.info("Entered EditHighValueFirstNarrowband:"+EditHighValueFirstNarrowband.getAttribute("value"));
		return EditHighValueFirstNarrowband;
	}
	
	public WebElement getEditCriticalValueFirstNarrowband() throws IOException
	{
		EditCriticalValueFirstNarrowband.click();
		EditCriticalValueFirstNarrowband.sendKeys(Keys.chord(Keys.CONTROL,"a", Keys.DELETE));
		DataFile df = new DataFile();
		ArrayList<String> data =df.getData("CriticalValueFirstNarrowband");		
		EditCriticalValueFirstNarrowband.sendKeys(data.get(1));
		System.out.println("Entered EditCriticalValueFirstNarrowband:"+EditCriticalValueFirstNarrowband.getAttribute("value"));
		log.info("Entered EditCriticalValueFirstNarrowband:"+EditCriticalValueFirstNarrowband.getAttribute("value"));
		return EditCriticalValueFirstNarrowband;
	}
	
	public WebElement getSaveEditOption()
	{
		waitForElementToBeVisible(SaveEditOption, 30, 2);
		SaveEditOption.click();
		System.out.println("Clicked on SaveEditOption");
		log.info("Clicked on SaveEditOption");
		return SaveEditOption;				
	}
	
	public WebElement getDeleteNarrowbandOption()
	{
		waitForElementToBeVisible(DeleteNarrowbandOption, 30, 2);
		DeleteNarrowbandOption.click();
		System.out.println("Clicked on Delete Narrowband Option");
		log.info("Clicked on Delete Narrowband Option");
		return DeleteNarrowbandOption;				
	}
	
	public WebElement getDeleteButton()
	{
		waitForElementToBeVisible(DeleteButton, 30, 2);
		DeleteButton.click();
		System.out.println("Clicked on DeleteButton");
		log.info("Clicked on DeleteButton");
		return DeleteButton;				
	}
	
	public WebElement getManualRadioButton()
	{
		//JavascriptExecutor executor = (JavascriptExecutor) driver;
		Actions a = new Actions(driver);
		waitForElementToBeVisible(ManualRadioButton, 30, 2);
		//executor.executeScript("arguments[0].click();", ManualRadioButton);
		a.moveToElement(ManualRadioButton).click().build().perform();
		//ManualRadioButton.click();
		System.out.println("Clicked on ManualRadioButton");
		log.info("Clicked on ManualRadioButton");
		return ManualRadioButton;				
	}
	
	public WebElement getManualNarrowbandName() throws IOException
	{
		DataFile df = new DataFile();
		ArrayList<String> data =df.getData("ManualNarrowbandName");		
		ManualNarrowbandName.sendKeys(data.get(1));
		System.out.println("Entered ManualNarrowbandName:"+ManualNarrowbandName.getAttribute("value"));
		log.info("Entered ManualNarrowbandName:"+ManualNarrowbandName.getAttribute("value"));
		return ManualNarrowbandName;
	}
	
	public WebElement getManualNarrowbandMinFrequency() throws IOException
	{
		DataFile df = new DataFile();
		ArrayList<String> data =df.getData("ManualNarrowbandMinFrequency");		
		ManualNarrowbandMinFrequency.sendKeys(data.get(1));
		System.out.println("Entered ManualNarrowbandMinFrequency:"+ManualNarrowbandMinFrequency.getAttribute("value"));
		log.info("Entered ManualNarrowbandMinFrequency:"+ManualNarrowbandMinFrequency.getAttribute("value"));
		return ManualNarrowbandMinFrequency;
	}
	
	public WebElement getManualNarrowbandMaxFrequency() throws IOException
	{
		DataFile df = new DataFile();
		ArrayList<String> data =df.getData("ManualNarrowbandMaxFrequency");		
		ManualNarrowbandMaxFrequency.sendKeys(data.get(1));
		System.out.println("Entered ManualNarrowbandMaxFrequency:"+ManualNarrowbandMaxFrequency.getAttribute("value"));
		log.info("Entered ManualNarrowbandMaxFrequency:"+ManualNarrowbandMaxFrequency.getAttribute("value"));
		return ManualNarrowbandMaxFrequency;
	}
}
