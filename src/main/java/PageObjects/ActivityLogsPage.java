package PageObjects;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.asserts.SoftAssert;

import Resources.base;

public class ActivityLogsPage extends base{
    public static Logger log =LogManager.getLogger(ActivityLogsPage.class.getName());
	
	//Declare WebDriver Globally
	public WebDriver driver;
		
	//Constructor
	public ActivityLogsPage(WebDriver driver)
	{
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	//Page Objects
	@FindBy(how = How.XPATH, using="//span[contains(text(),'Activity Logs')]")
	public WebElement ActivityLog;
	
	@FindBy(how = How.XPATH, using="//body/div[@id='root']/div[2]/div[1]/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/input[1]")
	public WebElement ActivityLogSearch;
	
	@FindBy(how = How.XPATH, using="//tbody/tr[1]")
	public WebElement ActivityLogFirstRow;
	
	@FindBy(how = How.XPATH, using="//body/div[@id='root']/div[2]/div[1]/div[2]/div[2]/div[2]/div[1]/div[3]/div[1]/div[1]/span[1]/button[1]/span[1]/form[1]/div[1]/div[1]/div[1]/div[1]/input[1]")
	public WebElement ActivityLogCalenderComponent;
	
	@FindBy(how = How.XPATH, using="//select[@id='MonthSelector_start']")
	public WebElement ActivityLogFromMonth;
	
	@FindBy(how = How.XPATH, using="//body[1]/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[1]/div[3]/div[1]/div[1]/span[1]/button[1]/span[1]/form[1]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[2]/select[1]/option[12]")
	public WebElement ActivityLogFromDec;
	
	@FindBy(how = How.XPATH, using="//select[@id='YearSelector_start']")
	public WebElement ActivityLogFromYear;
	
	@FindBy(how = How.XPATH, using="//body[1]/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[1]/div[3]/div[1]/div[1]/span[1]/button[1]/span[1]/form[1]/div[1]/div[2]/div[1]/div[1]/div[2]/div[1]/div[3]/select[1]/option[122]")
	public WebElement ActivityLogFromYear2021;
	
	@FindBy(how = How.XPATH, using="//div[@id='row_0_cell_6_start']")
	public WebElement ActivityLogFromDate5;
	
	@FindBy(how = How.XPATH, using="//div[contains(text(),'Apply')]")
	public WebElement ActivityLogCalApplyButton; 
	
	@FindBy(how = How.XPATH, using="//thead/tr[1]")
	public WebElement ActivityLogHeader;
	
	@FindBy(how = How.XPATH, using="//tbody/tr[1]/td[4]")
	public WebElement ActivityLogActionColumn;
	
	@FindBy(how = How.XPATH, using="//tbody/tr[1]/td[2]")
	public WebElement ActivityLogTypeColumn;
	
	//Page Methods
	SoftAssert sa = new SoftAssert();
		public WebElement getActivityLogTab() throws InterruptedException
		{
			waitForElementToBeVisible(ActivityLog,30,2);		
			ActivityLog.click();
			System.out.println("Clicked on ActivityLog tab");
			log.info("Clicked on ActivityLog tab"); 
			Thread.sleep(4000);
			System.out.println("ActivityLogHeader :" + ActivityLogHeader.getText());
			sa.assertEquals(ActivityLogHeader.getText(), "Severity\n" + 
					"Type\n" + 
					"Name\n" + 
					"Action\n" + 
					"Description\n" + 
					"User Name\n" + 
					"Role\n" + 
					"Timestamp");
			sa.assertAll();
			return ActivityLog;		
		}
		
		public int CountRows() 
		{
			List<WebElement> rows = driver.findElements(By.xpath("/html[1]/body[1]/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[2]/div[1]/div[1]/div[1]/table[1]/tbody[1]/tr"));
			System.out.println("row Size : "+rows.size());
			return rows.size();
		} 
	    public WebElement activityLogCreateSearch() throws InterruptedException {
	    	getActivityLogTab();
	    	Thread.sleep(5000);
	    	ActivityLogSearch.click();
	    	System.out.println("Clicked on ActivityLog search");
			log.info("Clicked on ActivityLog search");
			ActivityLogSearch.sendKeys(Keys.CONTROL + "a");
			ActivityLogSearch.sendKeys(Keys.DELETE);
	    	ActivityLogSearch.sendKeys("create");
	    	System.out.println("Enter Create ActivityLog search");
			log.info("Enter Create ActivityLog search");
	    	Thread.sleep(2000); 
	    	ActivityLogActionColumn.getText();
    		System.out.println(" Activity Log :" + ActivityLogActionColumn.getText());
			log.info(" Activity Log :" + ActivityLogActionColumn.getText());
//			sa.assertEquals(ActivityLogActionColumn.getText(), "create");
			sa.assertEquals(ActivityLogFirstRow.getText().contains("create"), true);
			sa.assertAll();
	    	return ActivityLogSearch;
	    	
	    }
	    public WebElement activityLogUpdateSearch() throws InterruptedException {
	    	getActivityLogTab();
	    	Thread.sleep(3000);
	    	ActivityLogSearch.click();
	    	System.out.println("Clicked on ActivityLog search");
			log.info("Clicked on ActivityLog search");
			ActivityLogSearch.sendKeys(Keys.CONTROL + "a");
			ActivityLogSearch.sendKeys(Keys.DELETE);
	    	ActivityLogSearch.sendKeys("update");
	    	System.out.println("Enter update ActivityLog search");
			log.info("Enter update ActivityLog search");
	    	Thread.sleep(2000); 
	    	ActivityLogActionColumn.getText();
    		System.out.println(" Activity Log :" + ActivityLogActionColumn.getText());
			log.info(" Activity Log :" + ActivityLogActionColumn.getText());
//			sa.assertEquals(ActivityLogActionColumn.getText(), "create");
			sa.assertEquals(ActivityLogFirstRow.getText().contains("update"), true);
			sa.assertAll();
	    	return ActivityLogSearch;
	    	
	    }
	    public WebElement activityLogDeleteSearch() throws InterruptedException {
	    	getActivityLogTab();
	    	Thread.sleep(5000);
	    	ActivityLogSearch.click();
	    	System.out.println("Clicked on ActivityLog delete search");
			log.info("Clicked on ActivityLog delete search");
	    	ActivityLogSearch.sendKeys("delete");
	    	Thread.sleep(2000); 
    		ActivityLogFirstRow.getText();
    		System.out.println(" Activity Log :" + ActivityLogFirstRow.getText());
			log.info(" Activity Log :" + ActivityLogFirstRow.getText());
	    	return ActivityLogSearch;
	    	
	    }
	    public WebElement activityLogRuleSearch() throws InterruptedException {
	    	getActivityLogTab();
	    	Thread.sleep(3000);
	    	ActivityLogSearch.click();
	    	System.out.println("Clicked on ActivityLog rule search");
			log.info("Clicked on ActivityLog rule search");
			ActivityLogSearch.sendKeys(Keys.CONTROL + "a");
			ActivityLogSearch.sendKeys(Keys.DELETE);
	    	ActivityLogSearch.sendKeys("alert");
	    	Thread.sleep(2000); 
	    	ActivityLogTypeColumn.getText();
    		System.out.println(" Activity Log :" + ActivityLogTypeColumn.getText());
			log.info(" Activity Log :" + ActivityLogTypeColumn.getText());
			sa.assertEquals(ActivityLogFirstRow.getText().contains("alert") || ActivityLogFirstRow.getText().contains("Alert"), true);
			sa.assertAll();
	    	return ActivityLogSearch;
	    	
	    }
	    public WebElement activityLogDeviceSearch() throws InterruptedException {
	    	getActivityLogTab();
	    	Thread.sleep(3000);
	    	ActivityLogSearch.click();
	    	System.out.println("Clicked on ActivityLog device search");
			log.info("Clicked on ActivityLog device search");
			ActivityLogSearch.sendKeys(Keys.CONTROL + "a");
			ActivityLogSearch.sendKeys(Keys.DELETE);
	    	ActivityLogSearch.sendKeys("device");
	    	Thread.sleep(2000); 
	    	ActivityLogTypeColumn.getText();
    		System.out.println(" Activity Log :" + ActivityLogTypeColumn.getText());
			log.info(" Activity Log :" + ActivityLogTypeColumn.getText());
			sa.assertEquals(ActivityLogFirstRow.getText().contains("Device") || ActivityLogFirstRow.getText().contains("device"), true);
			sa.assertAll();
	    	return ActivityLogSearch;
	    	
	    }
	    
	    public WebElement activityLogCalenderSearch() throws InterruptedException {
	    	getActivityLogTab();
	    	Thread.sleep(8000);
	    	ActivityLogCalenderComponent.click();
	    	System.out.println("Clicked on ActivityLog calender");
			log.info("Clicked on ActivityLog calender");
			Thread.sleep(3000);
			ActivityLogFromMonth.click();
			base.scrollTillElement(ActivityLogFromDec);
			ActivityLogFromDec.click();
			Thread.sleep(2000); 
			ActivityLogFromYear.click();
			base.scrollTillElement(ActivityLogFromYear2021);
			ActivityLogFromYear2021.click();
			Thread.sleep(2000); 
			ActivityLogFromDate5.click();
	    	Thread.sleep(2000); 
	    	ActivityLogCalApplyButton.click();
	    	Thread.sleep(2000);
    		System.out.println(" Activity Log calender :" + ActivityLogCalenderComponent.getText());
			log.info(" Activity Log calender:" + ActivityLogCalenderComponent.getText());
			Thread.sleep(6000);
			System.out.println(" Activity Log :" + ActivityLogFirstRow.getText());
			log.info(" Activity Log :" + ActivityLogFirstRow.getText());
	    	return ActivityLogCalenderComponent;
	    	
	    }
}
