package PageObjects;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import Resources.DataFile;
import Resources.base;

public class DevicesPage extends base{
	
public static Logger log =LogManager.getLogger(DevicesPage.class.getName());
	
	//Declare WebDriver Globally
	public WebDriver driver;
		
	//Constructor
	public DevicesPage(WebDriver driver)
	{
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}

	//Page Objects
    @FindBy(how = How.XPATH, using="//span[contains(text(),'Create')]")
	WebElement CreateDeviceButton;
    
    @FindBy(how = How.XPATH, using="//li[normalize-space()='Device']")
	WebElement SelectDeviceCreation;
    
    @FindBy(how = How.XPATH, using="//li[normalize-space()='Power Train']")
	WebElement SelectPowertrainCreation;
    
    @FindBy(how = How.XPATH, using="//input[@name='deviceName']")
	WebElement DeviceName;
    
    @FindBy(how = How.XPATH, using="//div[@id='mui-component-select-deviceType']")
	WebElement DeviceTypeDropdown;
    
    @FindBy(how = How.XPATH, using="//li[normalize-space()='iCastSense']")
	WebElement DeviceTypeDropdownICastSense;
    
    @FindBy(how = How.XPATH, using="//input[@role='combobox']")
	WebElement GroupDropdown;
       
    @FindBy(how = How.XPATH, using="//span[contains(text(),'AD-Test')]")
	WebElement GroupDropdownADTest;
    
    @FindBy(how = How.XPATH, using="//span[normalize-space()='Save']")
	WebElement SaveButton;
    
    @FindBy(how = How.XPATH, using="//p[@class='MuiTypography-root jss452 jss453 MuiTypography-body2']")
	WebElement SuccessToasterText;
    
    @FindBy(how = How.XPATH, using="//span[contains(text(),'Create Group')]")
	WebElement CreateGroup;
    
    @FindBy(how = How.XPATH, using="(//input[@role='textbox'])[2]")
	WebElement GroupName;
    
    @FindBy(how = How.XPATH, using="(//a[@title='Save'])[1]")
	WebElement SaveGroupName;
    								
    //@FindBy(how = How.XPATH, using="/html[1]/body[1]/div[1]/div[2]/div[1]/div[2]/div[2]/div[3]/div[2]/div[1]/div[6]/div[1]/div[1]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/td[1]/div[2]/span[2]")
    @FindBy(how = How.XPATH, using="/html[1]/body[1]/div[1]/div[2]/div[1]/div[2]/div[2]/div[3]/div[2]/div[1]/div[6]/div[1]/div[1]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/td[1]/div[2]/div[1]")
    WebElement FirstDevice;
    
//    @FindBy(how = How.XPATH, using="//span[contains(text(),'AD-101')]")
//	WebElement FirstDevice;
//    
    @FindBy(how = How.XPATH, using="//body/div[@id='root']/div[2]/div[1]/div[2]/div[2]/div[3]/div[2]/div[1]/div[4]/div[1]/div[1]/div[3]/div[2]/div[1]/div[1]/div[1]/div[1]/input[1]")
    WebElement DeviceSearchField;
    
    @FindBy(how = How.XPATH, using="//span[contains(text(),'Delete')]")
    WebElement DeleteDialogBoxButton;
    
    @FindBy(how = How.XPATH, using="//input[@name='name']")
	WebElement PowertrainName;
    
    @FindBy(how = How.XPATH, using="//input[@name='rpm_min']")
   	WebElement PowertrainRPM_min;
    
    @FindBy(how = How.XPATH, using="//input[@name='rpm_max']")
   	WebElement PowertrainRPM_max;
    
    @FindBy(how = How.XPATH, using="//input[@name='assets[0].name']")
   	WebElement PrimeMoverName;
    
    @FindBy(how = How.XPATH, using="//input[@name='assets[0].serial_number']")
   	WebElement PrimeMoverSerialNo;
    
    @FindBy(how = How.XPATH, using="//*[@id='mui-component-select-assets[0].component_class']")
   	WebElement PMComponentClassDropdown;
    
    @FindBy(how = How.XPATH, using="//li[normalize-space()='Electric motors/generators']")
   	WebElement PMComponentClassSelect;
    
    @FindBy(how = How.XPATH, using="//*[@id='mui-component-select-assets[0].component_subclass']")
   	WebElement PMComponentSubClassDropdown;
    
    @FindBy(how = How.XPATH, using="//li[normalize-space()='AC motor']")
   	WebElement PMComponentSubClassSelect;
    
    @FindBy(how = How.XPATH, using="//*[@id='mui-component-select-assets[0].manufacturer']")
   	WebElement PMComponentManufacturerDropdown;
    
    @FindBy(how = How.XPATH, using="//li[normalize-space()='Baldor Reliance']")
   	WebElement PMComponentManufacturerSelect;
    
    @FindBy(how = How.XPATH, using="//*[@id='mui-component-select-assets[0].model']")
   	WebElement PMComponentModelDropdown;
    
    @FindBy(how = How.XPATH, using="//li[@role='option']")
   	WebElement PMComponentModelSelect;
    
    @FindBy(how = How.XPATH, using="//a[@title='Edit']")
   	WebElement PowertrainEdit;
    
    @FindBy(how = How.XPATH, using="(//span[contains(text(),'100-PM-A')])[1]")
   	WebElement PowertrainDevice;
        
    //Page Methods
    /*
    public WebElement getToastMsg()
	{
	 if (base.isElementExists(toastWebElmnt)) {
			System.out.println("Toast message : "+toastWebElmnt.getText());
			log.info("Toast message : "+toastWebElmnt.getText());
		} else {
			System.out.println("Toast message not found ");
			log.info("Toast message not found ");
		}
		return toastWebElmnt;		
	}
    */
    public WebElement getCreateDeviceButton()
	{
		waitForElementToBeClickable(CreateDeviceButton, 60, 2);
		Actions a  = new Actions(driver);
		a.moveToElement(CreateDeviceButton).click().build().perform();
		//CreateDeviceButton.click();
		System.out.println("Clicked on CreateDevice Button");
		log.info("Clicked on CreateDevice Button");
		return CreateDeviceButton;		
	}
    public WebElement getSelectDeviceCreation()
	{
		waitForElementToBeClickable(SelectDeviceCreation, 60, 2);
		Actions a  = new Actions(driver);
		a.moveToElement(SelectDeviceCreation).click().build().perform();
		//CreateDeviceButton.click();
		System.out.println("Selected Device");
		log.info("Selected Device");
		return SelectDeviceCreation;		
	}
    public WebElement getSelectPowertrainCreation()
	{
		waitForElementToBeClickable(SelectPowertrainCreation, 60, 2);
		Actions a  = new Actions(driver);
		a.moveToElement(SelectPowertrainCreation).click().build().perform();
		//CreateDeviceButton.click();
		System.out.println("Selected Power Train");
		log.info("Selected Power Train");
		return SelectPowertrainCreation;		
	}
    public WebElement getDeviceName() throws IOException
	{
		DataFile df = new DataFile();
		ArrayList<String> data =df.getData("DeviceName");		
		DeviceName.click();
		DeviceName.sendKeys(data.get(1));
		System.out.println("Entered DeviceName:"+DeviceName.getAttribute("value"));
		log.info("Entered DeviceName:"+DeviceName.getAttribute("value"));
		return DeviceName;
	}
    
    public WebElement getDeviceTypeDropdown()
	{
		waitForElementToBeVisible(DeviceTypeDropdown, 30, 2);
		DeviceTypeDropdown.click();
		System.out.println("Clicked on DeviceType Dropdown ");
		log.info("Clicked on DeviceType Dropdown");
		return DeviceTypeDropdown;		
	}
    public WebElement getDeviceTypeDropdownICastSense()
	{
		waitForElementToBeVisible(DeviceTypeDropdownICastSense, 30, 2);
		DeviceTypeDropdownICastSense.click();
		System.out.println("Clicked on DeviceTypeDropdownICastSense");
		log.info("Clicked on DeviceTypeDropdownICastSense");
		return DeviceTypeDropdownICastSense;		
	}
    public WebElement getGroupDropdown()
	{
		waitForElementToBeVisible(GroupDropdown, 30, 2);
		GroupDropdown.click();
		System.out.println("Clicked on GroupDropdown");
		log.info("Clicked on GroupDropdown");
		return GroupDropdown;		
	}
    public WebElement getGroupDropdownADTest()
	{
		waitForElementToBeVisible(GroupDropdownADTest, 30, 2);
		GroupDropdownADTest.click();
		System.out.println("Clicked on GroupDropdownADTest");
		log.info("Clicked on GroupDropdownADTest");
		return GroupDropdownADTest;		
	}
    public WebElement getSaveButton()
	{
		waitForElementToBeClickable(SaveButton, 30, 2);
		SaveButton.click();
		System.out.println("Clicked on Save Button");
		log.info("Clicked on Save Button");
		return SaveButton;		
	}
    public WebElement getSuccessToasterText()
    {
    	waitForElementToBeClickable(SuccessToasterText, 30, 2);
    	System.out.println("Toaster Text:"+SuccessToasterText.getText());
    	log.info("Toaster Text:"+SuccessToasterText.getText());
    	return SuccessToasterText;
    }
    public WebElement getCreateGroup()
	{
		waitForElementToBeClickable(CreateGroup, 30, 2);
		CreateGroup.click();
		System.out.println("Clicked on CreateGroup");
		log.info("Clicked on CreateGroup");
		return SaveButton;		
	}
    public WebElement getGroupName() throws IOException
	{
		DataFile df = new DataFile();
		ArrayList<String> data =df.getData("GroupName");		
		GroupName.sendKeys(data.get(1));
		System.out.println("Entered GroupName:"+GroupName.getAttribute("value"));
		log.info("Entered GroupName:"+GroupName.getAttribute("value"));
		return GroupName;
	}
    public WebElement getSaveGroupName()
	{
		waitForElementToBeClickable(SaveGroupName, 30, 2);
		//SaveGroupName.click();
		SaveGroupName.sendKeys(Keys.ENTER);
		System.out.println("Clicked on SaveGroupName");
		log.info("Clicked on SaveGroupName");
		return SaveGroupName;		
	}
    public WebElement getFirstDevice()
	{
		waitForElementToBeClickable(FirstDevice, 30, 2);
		System.out.println("Device Name:"+FirstDevice.getText());
		log.info("Device Name:"+FirstDevice.getText());
		FirstDevice.click();
		System.out.println("Clicked on First Device");
		log.info("Clicked on First Device");
		return FirstDevice;		
	}
    public WebElement UpdateGroupName() throws IOException, InterruptedException 
    {
    	DataFile df = new DataFile();
   		ArrayList<String> data =df.getData("GroupName");	
   		DeviceSearchField.click();
   		DeviceSearchField.sendKeys(Keys.CONTROL + "a");
   		DeviceSearchField.sendKeys(Keys.DELETE);
   		DeviceSearchField.sendKeys(data.get(1));
   		Thread.sleep(4000);
   		driver.findElement(By.xpath("//a[@title='Edit']")).click();
   		GroupName.sendKeys(Keys.CONTROL + "a");
   		GroupName.sendKeys(Keys.DELETE);
        //DeviceSearchField.sendKeys(data.get(1));
   		GroupName.sendKeys("Automation");
		getSaveGroupName();
		return GroupName; 	
    }
    public WebElement DeleteGroupName() throws IOException, InterruptedException 
    {
    	DataFile df = new DataFile();
   		ArrayList<String> data =df.getData("GroupName");	
   		System.out.println("Testcase: delete group");
   		Thread.sleep(4000);
   		DeviceSearchField.click();
   		DeviceSearchField.sendKeys(Keys.CONTROL + "a");
   		DeviceSearchField.sendKeys(Keys.DELETE);
   		DeviceSearchField.sendKeys(data.get(1));
   		Thread.sleep(4000);
   		driver.findElement(By.xpath("//a[@title='Delete']")).click();
   		DeleteDialogBoxButton.click();
		return GroupName; 	
    }
    public WebElement getPowertrainName() throws IOException
	{
		DataFile df = new DataFile();
		ArrayList<String> data =df.getData("PowertrainName");		
		PowertrainName.click();
		PowertrainName.sendKeys(data.get(1));
		System.out.println("Entered PowertrainName:"+PowertrainName.getAttribute("value"));
		log.info("Entered PowertrainName:"+PowertrainName.getAttribute("value"));
		return PowertrainName;
	}
    public WebElement getPowertrainRPM_min() throws IOException
	{
		DataFile df = new DataFile();
		ArrayList<String> data =df.getData("PowertrainRPM_min");		
		PowertrainRPM_min.click();
		PowertrainRPM_min.sendKeys(data.get(1));
		System.out.println("Entered PowertrainRPM_min:"+PowertrainRPM_min.getAttribute("value"));
		log.info("Entered PowertrainRPM_min:"+PowertrainRPM_min.getAttribute("value"));
		return PowertrainRPM_min;
	}
    public WebElement getPowertrainRPM_max() throws IOException
	{
		DataFile df = new DataFile();
		ArrayList<String> data =df.getData("PowertrainRPM_max");		
		PowertrainRPM_max.click();
		PowertrainRPM_max.sendKeys(data.get(1));
		System.out.println("Entered PowertrainRPM_max:"+PowertrainRPM_max.getAttribute("value"));
		log.info("Entered PowertrainRPM_max:"+PowertrainRPM_max.getAttribute("value"));
		return PowertrainRPM_max;
	}
    public WebElement getPrimeMoverName() throws IOException
	{
		DataFile df = new DataFile();
		ArrayList<String> data =df.getData("PrimeMoverName");		
		PrimeMoverName.click();
		PrimeMoverName.sendKeys(data.get(1));
		System.out.println("Entered PrimeMoverName:"+PrimeMoverName.getAttribute("value"));
		log.info("Entered PrimeMoverName:"+PrimeMoverName.getAttribute("value"));
		return PrimeMoverName;
	}
    public WebElement getPrimeMoverSerialNo() throws IOException
	{
		DataFile df = new DataFile();
		ArrayList<String> data =df.getData("PrimeMoverSerialNo");		
		PrimeMoverSerialNo.click();
		PrimeMoverSerialNo.sendKeys(data.get(1));
		System.out.println("Entered PrimeMoverSerialNo:"+PrimeMoverSerialNo.getAttribute("value"));
		log.info("Entered PrimeMoverSerialNo:"+PrimeMoverSerialNo.getAttribute("value"));
		return PrimeMoverSerialNo;
	}
    public WebElement getPMComponentClassDropdown()
	{
		waitForElementToBeClickable(PMComponentClassDropdown, 30, 2);
		PMComponentClassDropdown.click();
		System.out.println("Clicked on PMComponentClassDropdown");
		log.info("Clicked on PMComponentClassDropdown");
		return PMComponentClassDropdown;		
	}
    public WebElement getPMComponentClassSelect()
	{
		waitForElementToBeClickable(PMComponentClassSelect, 30, 2);
		PMComponentClassSelect.click();
		System.out.println("Clicked on PMComponentClassSelect");
		log.info("Clicked on PMComponentClassSelect");
		return PMComponentClassSelect;		
	}
    public WebElement getPMComponentSubClassDropdown()
	{
		waitForElementToBeClickable(PMComponentSubClassDropdown, 30, 2);
		PMComponentSubClassDropdown.click();
		System.out.println("Clicked on PMComponentSubClassDropdown");
		log.info("Clicked on PMComponentSubClassDropdown");
		return PMComponentSubClassDropdown;		
	}
    public WebElement getPMComponentSubClassSelect()
	{
		waitForElementToBeClickable(PMComponentSubClassSelect, 30, 2);
		PMComponentSubClassSelect.click();
		System.out.println("Clicked on PMComponentSubClassSelect");
		log.info("Clicked on PMComponentSubClassSelect");
		return PMComponentSubClassSelect;		
	}
    public WebElement getPMComponentManufacturerDropdown()
	{
		waitForElementToBeClickable(PMComponentManufacturerDropdown, 30, 2);
		PMComponentManufacturerDropdown.click();
		System.out.println("Clicked on PMComponentManufacturerDropdown");
		log.info("Clicked on PMComponentManufacturerDropdown");
		return PMComponentManufacturerDropdown;		
	}
    public WebElement getPMComponentManufacturerSelect()
	{
		waitForElementToBeClickable(PMComponentManufacturerSelect, 30, 2);
		PMComponentManufacturerSelect.click();
		System.out.println("Clicked on PMComponentManufacturerSelect");
		log.info("Clicked on PMComponentManufacturerSelect");
		return PMComponentManufacturerSelect;		
	}
    public WebElement getPMComponentModelDropdown()
	{
		waitForElementToBeClickable(PMComponentModelDropdown, 30, 2);
		PMComponentModelDropdown.click();
		System.out.println("Clicked on PMComponentModelDropdown");
		log.info("Clicked on PMComponentModelDropdown");
		return PMComponentModelDropdown;		
	}
    public WebElement getPMComponentModelSelect()
	{
		waitForElementToBeClickable(PMComponentModelSelect, 30, 2);
		PMComponentModelSelect.click();
		System.out.println("Clicked on PMComponentModelSelect");
		log.info("Clicked on PMComponentModelSelect");
		return PMComponentModelSelect;		
	}
    public WebElement getPowertrainName_Update() throws IOException
	{
    	PowertrainName.sendKeys(Keys.chord(Keys.CONTROL,"a", Keys.DELETE));
		DataFile df = new DataFile();
		ArrayList<String> data =df.getData("PowertrainName_Update");				
		PowertrainName.sendKeys(data.get(1));
		System.out.println("Entered PowertrainName:"+PowertrainName.getAttribute("value"));
		log.info("Entered PowertrainName:"+PowertrainName.getAttribute("value"));
		return PowertrainName;
	}
    public WebElement getPowertrainRPM_max_Update() throws IOException
	{
    	PowertrainRPM_max.sendKeys(Keys.chord(Keys.CONTROL,"a", Keys.DELETE));
		DataFile df = new DataFile();
		ArrayList<String> data =df.getData("PowertrainRPM_max_Update");				
		PowertrainRPM_max.sendKeys(data.get(1));
		System.out.println("Entered PowertrainRPM_max:"+PowertrainRPM_max.getAttribute("value"));
		log.info("Entered PowertrainRPM_max:"+PowertrainRPM_max.getAttribute("value"));
		return PowertrainRPM_max;
	}
    public WebElement getPowertrainRPM_min_Update() throws IOException
	{
    	PowertrainRPM_min.sendKeys(Keys.chord(Keys.CONTROL,"a", Keys.DELETE));
		DataFile df = new DataFile();
		ArrayList<String> data =df.getData("PowertrainRPM_min_Update");		
		PowertrainRPM_min.sendKeys(data.get(1));
		System.out.println("Entered PowertrainRPM_min:"+PowertrainRPM_min.getAttribute("value"));
		log.info("Entered PowertrainRPM_min:"+PowertrainRPM_min.getAttribute("value"));
		return PowertrainRPM_min;
	}
    public WebElement getPowertrainSearch() throws IOException
	{
    	DeviceSearchField.click();
		DataFile df = new DataFile();
		ArrayList<String> data =df.getData("PowertrainName");		
		DeviceSearchField.sendKeys(data.get(1));
		System.out.println("Entered PowertrainName in Search:"+DeviceSearchField.getAttribute("value"));
		log.info("Entered PowertrainName in Search:"+DeviceSearchField.getAttribute("value"));
		return DeviceSearchField;
	}
    public WebElement getPowertrainEdit()
	{
		waitForElementToBeClickable(PowertrainEdit, 30, 2);
		PowertrainEdit.click();
		System.out.println("Clicked on PowertrainEdit");
		log.info("Clicked on PowertrainEdit");
		return PowertrainEdit;		
	}
    public WebElement getPowertrainDeviceSearch() throws IOException
	{
    	DeviceSearchField.click();
		DataFile df = new DataFile();
		ArrayList<String> data =df.getData("PowertrainDeviceName");		
		DeviceSearchField.sendKeys(data.get(1));
		System.out.println("Entered PowertrainName in Search:"+DeviceSearchField.getAttribute("value"));
		log.info("Entered PowertrainName in Search:"+DeviceSearchField.getAttribute("value"));
		return DeviceSearchField;
	}
    public WebElement getPowertrainDevice()
	{
		waitForElementToBeClickable(PowertrainDevice, 30, 2);
		System.out.println("Device Name:"+PowertrainDevice.getText());
		log.info("Device Name:"+PowertrainDevice.getText());
		PowertrainDevice.click();
		System.out.println("Clicked on Powertrain Device");
		log.info("Clicked on Powertrain Device");
		return PowertrainDevice;		
	}
    
}
