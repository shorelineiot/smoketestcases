package PageObjects;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.asserts.SoftAssert;

import Resources.DataFile;
import Resources.base;

public class OrgSettingsPage extends base{

	public static Logger log =LogManager.getLogger(OrgSettingsPage.class.getName());
	
	//Declare WebDriver Globally
	public WebDriver driver;
		
	//Constructor
	public OrgSettingsPage(WebDriver driver)
	{
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	//Page Objects
	@FindBy(how = How.XPATH, using="//span[contains(text(),'Org Settings')]")
	public WebElement OrgSettings;
	 
	@FindBy(how = How.XPATH, using="//body/div[@id='root']/div[2]/div[1]/div[2]/div[1]/div[1]/ul[1]/div[4]/div[1]/div[1]/div[1]/a[1]/div[1]/div[1]/span[1]")
	public WebElement UsersButton;
	 
	@FindBy(how = How.XPATH, using="//body/div[@id='root']/div[2]/div[1]/div[2]/div[1]/div[1]/ul[1]/div[4]/div[1]/div[1]/div[1]/a[2]/div[1]/div[1]/span[1]")
	public WebElement RolesButton;
	
	@FindBy(how = How.XPATH, using="//body/div[@id='root']/div[2]/div[1]/div[2]/div[1]/div[1]/ul[1]/div[4]/div[1]/div[1]/div[1]/a[3]/div[1]/div[1]/span[1]")
	public WebElement CustomizationButton;
	
	@FindBy(how = How.XPATH, using="//body/div[@id='root']/div[2]/div[1]/div[2]/div[1]/div[1]/ul[1]/div[4]/div[1]/div[1]/div[1]/a[4]/div[1]/div[1]/span[1]")
	public WebElement ApiTokenButton;
	 
	@FindBy(how = How.XPATH, using="//span[contains(text(),'Invite User')]")
	public WebElement InviteUserButton;
	 
	@FindBy(how = How.XPATH, using="//input[@name='name']")
	public WebElement UserNameField;
	 
	@FindBy(how = How.XPATH, using="//input[@name='email']")
	public WebElement UserEmailField;
	 
	@FindBy(how = How.XPATH, using="//div[@id='mui-component-select-userRole']")
	public WebElement UserRoleField;
	 
	@FindBy(how = How.XPATH, using="//body/div[@id='menu-userRole']/div[3]/ul[1]/li[2]")
	public WebElement RoleOption;
	 
	@FindBy(how = How.XPATH, using="//div[@id='mui-component-select-orgs']")
	public WebElement OrgSelectField;
	 
	@FindBy(how = How.XPATH, using="//body/div[@id='menu-orgs']/div[3]/ul[1]/li[1]/span[1]/span[1]/input[1]")
	public WebElement InviteUserOrg;
	 
	@FindBy(how = How.XPATH, using="//span[normalize-space()='Invite']")
	public WebElement InviteButton;
	 
	@FindBy(how = How.XPATH, using="//tbody/tr[1]/td[7]/div[1]/button[1]/span[1]/*[1]")
	public WebElement BlockUser;
	 
	@FindBy(how = How.XPATH, using="//p[contains(text(),'Are you sure you want to block the user')]")
	public WebElement BlockUserMessage;
	 
	@FindBy(how = How.XPATH, using="//tbody/tr[1]/td[7]/div[1]/button[2]/span[1]/*[1]")
	public WebElement DeleteUser;
	 
	@FindBy(how = How.XPATH, using="//body/div[27]/div[3]/div[1]/div[2]/div[1]")
	public WebElement DeleteUserMessage;
	 
	@FindBy(how = How.XPATH, using="//body/div[@id='menu-orgs']/div[1]")
	public WebElement ExternalClick;
	 
	@FindBy(how = How.XPATH, using="//body/div[@id='menu-']/div[1]")
	public WebElement ExternalClick2;
	 							
	@FindBy(how = How.XPATH, using = "//body/div[@id='root']/div[3]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]")
	public WebElement toastWebElmnt;
	 	 
	@FindBy(how = How.XPATH, using = "//tbody/tr[1]/td[5]/div[1]/h3[1]/div[1]/div[1]/div[1]")
	public WebElement AlertField;
	 
	@FindBy(how = How.XPATH, using = "//body/div[@id='menu-']/div[3]/ul[1]/li[1]/div[1]/span[1]")
	public WebElement EmailField;
	 
	@FindBy(how = How.XPATH, using ="//tbody/tr[1]/td[6]/div[1]/h3[1]/div[1]/div[1]")
	public WebElement BillingField;
	 
	@FindBy(how = How.XPATH, using = "//tbody/tr[1]/td[3]/button[1]/span[1]/div[1]")
	public WebElement RoleField;
	 
	@FindBy(how = How.XPATH, using = "//body/div[@id='simple-menu']/div[3]/ul[1]/li[1]/span[1]")
	public WebElement SuperAdminRole;
	 
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Delete')]")
	public WebElement DeleteButton;
	 
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Apply')]")
	public WebElement ApplyButton;
	 
	@FindBy(how = How.XPATH, using = "//body/div[@id='root']/div[2]/div[1]/div[2]/div[2]/div[1]/nav[1]/ol[1]/li[1]/span[1]")
	public WebElement RolesHeader;
	
	@FindBy(how = How.XPATH, using = "//body/div[@id='root']/div[2]/div[1]/div[2]/div[2]/div[2]/div[1]/div[1]")
	public WebElement UserHeader; 
		
	@FindBy(how = How.XPATH, using = "//body/div[@id='root']/div[2]/div[1]/div[2]/div[2]/div[2]/div[2]/div[1]/div[1]/div[2]/div[1]/input[1]")
	public WebElement UsersSearch; 
	
	@FindBy(how = How.XPATH, using = "//tbody/tr[1]/td[2]")
	public WebElement SearchedEmail;  
	
	@FindBy(how = How.XPATH, using = "//tbody/tr[1]/td[7]/div[1]/span[1]/button[1]")
	public WebElement BlockButton; 
	
	@FindBy(how = How.XPATH, using = "//tbody/tr[1]/td[7]/div[1]/span[2]/button[1]")
	public WebElement DeleteUserButton;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Resend Invite')]")
	public WebElement ResendInviteButton;
	
	@FindBy(how = How.XPATH, using = "//body/div[@id='root']/div[2]/div[1]/div[2]/div[2]/div[1]/nav[1]")
	public WebElement CustomizationHeader;  
	 
	@FindBy(how = How.XPATH, using = "//body/div[@id='root']/div[2]/div[1]/div[2]/div[2]/div[2]/form[1]/div[1]/div[1]/div[1]/div[1]/input[1]")
	public WebElement OrgNameField; 
	
	@FindBy(how = How.XPATH, using = "//body/div[@id='popperMenu']/div[1]/button[1]")
	public WebElement OrgNameApply;
	
	//API Token
	@FindBy(how = How.XPATH, using = "//body/div[@id='root']/div[2]/div[1]/div[2]/div[2]/div[1]/nav[1]")
	public WebElement ApiTokenHeader;  
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Create Token')]")
	public WebElement CreateApiTokenButton;
	
	@FindBy(how = How.XPATH, using = "//body/div[7]/div[3]/div[2]/div[3]/form[1]/div[1]/div[1]/div[1]/input[1]")
	public WebElement APITokenLabelField;
	
	@FindBy(how = How.XPATH, using = "//body/div[7]/div[3]/div[2]/div[3]/form[1]/div[2]/div[1]/label[1]/span[1]")
	public WebElement APITokenCheckBox;
	
	@FindBy(how = How.XPATH, using = "//span[contains(text(),'Ok')]")
	public WebElement APITokenOkButton;
	
	@FindBy(how = How.XPATH, using = "//body/div[@id='root']/div[2]/div[1]/div[2]/div[2]/div[3]/div[1]/div[1]/div[2]/div[1]/input[1]")
	public WebElement SearchAPI;
	
	@FindBy(how = How.XPATH, using = "//tbody/tr[1]/td[6]/div[1]/button[1]")
	public WebElement EditAPIToken;
	
	@FindBy(how = How.XPATH, using = "//tbody/tr[1]/td[6]/div[1]/button[2]")
	public WebElement DeleteAPIToken; 
	
	@FindBy(how = How.XPATH, using = "//tbody/tr[1]/td[4]")
	public WebElement labelcolumn;
	
	@FindBy(how = How.XPATH, using = "//tbody/tr[1]/td[4]/div[1]/div[1]/input[1]")
	public WebElement labelUpdateField;
	
	//Page Methods
	SoftAssert sa = new SoftAssert();
	public WebElement getOrgSettingsTab()
	{
		waitForElementToBeVisible(OrgSettings,30,2);		
		OrgSettings.click();
		System.out.println("Clicked on OrgSettings tab");
		log.info("Clicked on OrgSettings tab");
		return OrgSettings;		
	}
	public WebElement getUsersTab() throws InterruptedException
	{
//		waitForElementToBeVisible(UsersButton,30,2);
		Thread.sleep(5000);
		getOrgSettingsTab();
		Thread.sleep(2000);
		UsersButton.click();
	    System.out.println("Clicked on Users tab" + UserHeader.getText());
		log.info("Clicked on Users tab" +  UserHeader.getText());
		sa.assertEquals(UserHeader.getText(),"Invite User");
		sa.assertAll();
		return UsersButton;		
	}
	public List<WebElement> CountRows() 
	{
		List<WebElement> rows = driver.findElements(By.xpath("/html[1]/body[1]/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/table[1]/tbody[1]/tr"));
		System.out.println("row Size : "+rows.size());
		return rows;
	} 
	public String RowCountOfelement(String name) 
	{
		 List<WebElement> rows=CountRows();
		 String rowNo="";
		 for(int i=0;i<rows.size();i++)
		 {
			 
		   WebElement row = driver.findElement(By.xpath("/html[1]/body[1]/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/div[2]/div[1]/div[1]/div[1]/div[1]/div[1]/table[1]/tbody[1]/tr["+(i+1)+"]"));
		   if(row.getText().trim().contains(name))
		   {
		      rowNo=Integer.toString(i+1); 
		      break;
		   }

		  }
		  return rowNo;
	}
	public WebElement getToastMsg()
	{		
	   if (base.isElementExists(toastWebElmnt)) 
	   {
			System.out.println("Toast message : "+toastWebElmnt.getText());
			log.info("Toast message : "+toastWebElmnt.getText());
		} else 
		{
			System.out.println("Toast message not found ");
			log.info("Toast message not found ");
		}
		return toastWebElmnt;		
	}
	public WebElement Inviteuser() throws InterruptedException, IOException
	{
		getUsersTab();
		Thread.sleep(2000);
		InviteUserButton.click();
		System.out.println("Clicked on Invite User Button");
		log.info("Clicked on Invite User Button");
		Thread.sleep(2000);
		UserNameField.click();
		System.out.println("Clicked on User Name Field");
		log.info("Clicked on User Name Field");
		Thread.sleep(2000);
		DataFile df = new DataFile();
	    ArrayList<String> data =df.getData("User_Invite_name");
		UserNameField.sendKeys(data.get(1));
		System.out.println("enter " + data.get(1) + "in User name" );
		log.info("enter " + data.get(1) + "in User name" );
		Thread.sleep(1000);
		UserEmailField.click();
		System.out.println("Clicked on User email Field");
		log.info("Clicked on User email Field");
		DataFile df2 = new DataFile();
	    ArrayList<String> data2 =df2.getData("User_Invite_Email");
		UserEmailField.sendKeys(data2.get(1));
		System.out.println("enter " + data.get(1) + "in User email" );
		log.info("enter " + data.get(1) + "in User email" );
		Thread.sleep(1000);
		UserRoleField.click();
		System.out.println("Clicked on User Role Field");
		log.info("Clicked on User Role Field");
		RoleOption.click();
		System.out.println("Clicked on User role Option");
		log.info("Clicked on User role Option");
		Thread.sleep(1000);
		OrgSelectField.click();
		System.out.println("Clicked on User org select Field");
		log.info("Clicked on User org select Field");
		InviteUserOrg.click();
		Thread.sleep(1000);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
	    executor.executeScript("arguments[0].click();", ExternalClick);
	    Thread.sleep(2000);
		InviteButton.click();
		System.out.println("Clicked on User Invite Button");
		log.info("Clicked on User Invite Button");
		Thread.sleep(5000);
		getToastMsg();
		UsersSearch.click();
		UsersSearch.clear();
		System.out.println("Clicked on User search and clear");
		log.info("Clicked on User search and clear");
		UsersSearch.sendKeys(data2.get(1));
		Thread.sleep(2000);
		sa.assertEquals(SearchedEmail.getText(), data2.get(1));
		sa.assertAll();
	    return UsersButton;		
	}	
	public WebElement resendInvite() throws InterruptedException, IOException
	{
		getUsersTab();
	    DataFile df = new DataFile();
		ArrayList<String> data =df.getData("User_Invite_Email");
		UsersSearch.click();
		UsersSearch.clear();
		System.out.println("Clicked on User search and clear");
		log.info("Clicked on User search and clear");
		UsersSearch.sendKeys(data.get(1));
		Thread.sleep(2000);
		ResendInviteButton.click();
		System.out.println("Clicked on Resend Invite Button ");
		log.info("Clicked on Resend Invite Button");
	    Thread.sleep(5000);
	    getToastMsg();
	    sa.assertEquals(getToastMsg().getText(), "Success\n" + 
	    		"User Invite sent Successfully!");
	    sa.assertAll();
		return ResendInviteButton;		
	}
	public WebElement blockUser() throws InterruptedException, IOException
	{
		getUsersTab();
	    DataFile df = new DataFile();
		ArrayList<String> data =df.getData("User_Invite_Email");
		UsersSearch.click();
		UsersSearch.clear();
		System.out.println("Clicked on User search and clear");
		log.info("Clicked on User search and clear");
		UsersSearch.sendKeys(data.get(1));
		Thread.sleep(2000);
		BlockButton.click();
		System.out.println("Clicked on Block Button");
		log.info("Clicked on Block Button");
        Thread.sleep(2000);
        System.out.println("User block message :"+BlockUserMessage.getText());
        ApplyButton.click();
        System.out.println("Clicked on Block Button apply");
		log.info("Clicked on Block Button apply");
	    Thread.sleep(5000);
	    getToastMsg();
	    sa.assertEquals(getToastMsg().getText(), "Success\n" + 
	    		"User blocked successfully!");
	    sa.assertAll();
		return BlockUser;		
	}
	public WebElement unblockUser() throws InterruptedException, IOException
	{
		getUsersTab();
	    DataFile df = new DataFile();
		ArrayList<String> data =df.getData("User_Invite_Email");
		UsersSearch.click();
		UsersSearch.clear();
		System.out.println("Clicked on User search and clear");
		log.info("Clicked on User search and clear");
		UsersSearch.sendKeys(data.get(1));
		Thread.sleep(2000);
		BlockButton.click();
		System.out.println("Clicked on unblock Button");
		log.info("Clicked on unblock Button");
        Thread.sleep(2000);
        ApplyButton.click();
        System.out.println("Clicked on unblock Button apply");
		log.info("Clicked on unblock Button apply");
	    Thread.sleep(5000);
	    getToastMsg();
	    sa.assertEquals(getToastMsg().getText(), "Success\n" + 
	    		"User unblocked successfully!");
	    sa.assertAll();
		return BlockUser;		
	}
	public WebElement deleteUser() throws IOException, InterruptedException
	{
		getUsersTab();
	    DataFile df = new DataFile();
		ArrayList<String> data =df.getData("User_Invite_Email");
		UsersSearch.click();
		UsersSearch.clear();
		System.out.println("Clicked on User search and clear");
		log.info("Clicked on User search and clear");
		UsersSearch.sendKeys(data.get(1));
		Thread.sleep(2000);
		DeleteUserButton.click();
        Thread.sleep(2000);
   		DeleteButton.click();
		Thread.sleep(3000);
		getToastMsg();
		sa.assertEquals(getToastMsg().getText(),"Success\n" + 
				"User Removed Successfully!");
	    sa.assertAll();
		return DeleteUser;		
	}
	public WebElement alertOption() throws InterruptedException, IOException
	{
		getUsersTab();
		driver.findElement(By.xpath("//tbody/tr[1]/td[5]/div[1]/h3[1]/div[1]/div[1]/div[1]")).click();
		System.out.println("Clicked on Alert option");
		log.info("Clicked on Alert option");
		Thread.sleep(1000);
		EmailField.click();
		System.out.println("Clicked on Alert email option");
		log.info("Clicked on Alert email option");
		Thread.sleep(1000);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", ExternalClick2);
		Thread.sleep(3000);
		getToastMsg();
		sa.assertEquals(getToastMsg().getText(), "Success\n" + 
				"User notifications updated successfully!");
	    sa.assertAll();
		return EmailField;		
	}
	public WebElement billingOption() throws InterruptedException, IOException
	{
		getUsersTab();
		driver.findElement(By.xpath("//tbody/tr[1]/td[6]/div[1]/h3[1]/div[1]/div[1]")).click();
		System.out.println("Clicked on Biliing option");
		log.info("Clicked on Biliing option");
		Thread.sleep(1000);
		EmailField.click();
		System.out.println("Clicked on Biliing email option");
		log.info("Clicked on Biliing email option");
		Thread.sleep(1000);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
		executor.executeScript("arguments[0].click();", ExternalClick2);
		getToastMsg();
		sa.assertEquals(getToastMsg().getText(), "Success\n" + 
				"User notifications updated successfully!");
	    sa.assertAll();
		return EmailField;		
	}
	public WebElement roleOption() throws InterruptedException, IOException
	{
		getUsersTab();
		 DataFile df = new DataFile();
			ArrayList<String> data =df.getData("User_Invite_Email");
			UsersSearch.click();
			UsersSearch.clear();
			System.out.println("Clicked on User search and clear");
			log.info("Clicked on User search and clear");
			UsersSearch.sendKeys(data.get(1));
			Thread.sleep(2000);
		driver.findElement(By.xpath("//tbody/tr[1]/td[3]/button[1]/span[1]/div[1]")).click();
		System.out.println("Clicked on Role option");
		log.info("Clicked on Role option");
		Thread.sleep(1000);
		SuperAdminRole.click();
		System.out.println("Clicked on Super Admin Role option");
		log.info("Clicked on Super Admin Role option");
		Thread.sleep(3000);
		getToastMsg();
		sa.assertEquals(getToastMsg().getText(), "Success\n" + 
				"User role changed successfully!");
	    sa.assertAll();
		return SuperAdminRole;		
	}
	//Roles
	public WebElement getRolesTab() throws InterruptedException
	{
		getOrgSettingsTab();
		RolesButton.click();
		Thread.sleep(2000);
		System.out.println("Clicked on Roles tab :" + RolesHeader.getText());
		log.info("Clicked on Roles tab :" + RolesHeader.getText());
		sa.assertEquals(RolesHeader.getText(), "Roles");
		sa.assertAll();
		return RolesButton;		
	}
	
	//Customization
		public WebElement getCustomizationTab() throws InterruptedException
		{
			getOrgSettingsTab();
			CustomizationButton.click();
			System.out.println("Clicked on Customization tab " );
			log.info("Clicked on Customization tab " );
			Thread.sleep(2000);
			System.out.println("Clicked on CustomizationTab tab :" + CustomizationHeader.getText());
			log.info("Clicked on CustomizationTab tab :" + CustomizationHeader.getText());
			sa.assertEquals(CustomizationHeader.getText(), "Customization");
			sa.assertAll();
			return CustomizationHeader;		
		}
		
		public WebElement ChangeOrgNameTab() throws InterruptedException
		{
			getCustomizationTab();
			Thread.sleep(5000);
			String orgName = OrgNameField.getText();
			System.out.println("Org Name " + OrgNameField.getText());
			Thread.sleep(1000);
			OrgNameField.click();
			System.out.println("Clicked on OrgNameField");
			log.info("Clicked on OrgNameField");
			Thread.sleep(1000);
			OrgNameField.clear();
			System.out.println("clear OrgNameField");
			log.info("clear OrgNameField");
			Thread.sleep(1000);
			OrgNameField.sendKeys(orgName);
			System.out.println("Updated org name send");
			log.info("Updated org name send");
			Thread.sleep(2000);
			OrgNameApply.click();
			System.out.println("Updated org name save");
			log.info("Updated org name save");
			Thread.sleep(3000);
			getToastMsg();
			sa.assertEquals(getToastMsg().getText(),"Success\n" + 		
					"Organization Updated Successfully!");
		    sa.assertAll();
			return OrgNameField;
				
		}
		//API token ApiTokenButton
		public WebElement getApiTokenTab() throws InterruptedException
		{
			getOrgSettingsTab();
			ApiTokenButton.click();
			System.out.println("Clicked on API Token tab " );
			log.info("Clicked on API Token tab " );
			Thread.sleep(2000);
			System.out.println("Clicked on API Token tab :" + ApiTokenHeader.getText());
			log.info("Clicked on API Token tab :" + ApiTokenHeader.getText());
			sa.assertEquals(ApiTokenHeader.getText(), "API Tokens");
			sa.assertAll();
			return ApiTokenHeader;		
		}
		
		public WebElement createApiToken() throws InterruptedException, IOException
		{
			getApiTokenTab();	
			CreateApiTokenButton.click();
			System.out.println("Clicked on API Token create button " );
			log.info("Clicked on API Token create button " );
			Thread.sleep(4000);
			getToastMsg();
			sa.assertEquals(getToastMsg().getText(),"Success\n" + 		
					"API Token Created Successfully!");
			 DataFile df = new DataFile();
				ArrayList<String> data =df.getData("API_Token_Label");
				APITokenLabelField.click();
				APITokenLabelField.clear();
				System.out.println("Clicked on API Token Label Field");
				log.info("Clicked on API Token Label Field");
				APITokenLabelField.sendKeys(data.get(1));
				System.out.println("API Token label added" );
				log.info("API Token label added" );
				Thread.sleep(2000);
				APITokenCheckBox.click();
				System.out.println("API Token Checkbox Clicked" );
				log.info("API Token Checkbox Clicked" );
				Thread.sleep(1000);
				APITokenOkButton.click();
				System.out.println("API Token ok Clicked" );
				log.info("API Token ok Clicked" );
				Thread.sleep(3000);
				getToastMsg();
				sa.assertEquals(getToastMsg().getText(),"Success\n" + 		
						"API Token Updated Successfully!");
		    sa.assertAll();
		    return CreateApiTokenButton;
		}
		
		public WebElement searchApiToken() throws InterruptedException, IOException
		{
			getApiTokenTab();
			Thread.sleep(3000);
			SearchAPI.click();
			System.out.println("API Token search Clicked" );
			log.info("API Token search Clicked" );
			DataFile df = new DataFile();
			ArrayList<String> data =df.getData("API_Token_Label");
			SearchAPI.click();
			SearchAPI.clear();
			SearchAPI.sendKeys(data.get(1));
			System.out.println("Clicked on API  Token to search  :" +data.get(1));
			log.info("Clicked on API  Token to search  :" +data.get(1));
			Thread.sleep(2000);
			sa.assertEquals(labelcolumn.getText(), data.get(1));
			sa.assertAll();
			return SearchAPI;		
		}
		
		public WebElement editApiToken() throws InterruptedException, IOException
		{
			searchApiToken();
			EditAPIToken.click();
			System.out.println("Clicked API Token edit" );
			log.info("Clicked API Token edit" );
			Thread.sleep(2000);
			DataFile df = new DataFile();
			ArrayList<String> data =df.getData("Updated_API_Token_Label");
			labelUpdateField.click();
			Thread.sleep(2000);
			labelUpdateField.sendKeys(Keys.CONTROL + "a");
			labelUpdateField.sendKeys(Keys.DELETE);
			Thread.sleep(1000);
			labelUpdateField.sendKeys(data.get(1));
			System.out.println("Send updated API Token label :" + data.get(1) );
			log.info("Send updated API Token label :" + data.get(1) );
			Thread.sleep(1000);
			EditAPIToken.click();
			System.out.println("Clicked API Token edit save" );
			log.info("Clicked API Token edit save" );
			Thread.sleep(3000);
			getToastMsg();
			sa.assertEquals(getToastMsg().getText(),"Success\n" + 		
					"API Token Updated Successfully!");
			sa.assertAll();
			return EditAPIToken;		
		}
		
		public WebElement deleteApiToken() throws InterruptedException, IOException
		{
			getApiTokenTab();
			Thread.sleep(3000);
			SearchAPI.click();
			System.out.println("API Token search Clicked" );
			log.info("API Token search Clicked" );
			DataFile df = new DataFile();
			ArrayList<String> data =df.getData("API_Token_Label");
			SearchAPI.click();
			SearchAPI.clear();
			SearchAPI.sendKeys(data.get(1));
			System.out.println("Clicked on API  Token to search  :" +data.get(1));
			log.info("Clicked on API  Token to search  :" +data.get(1));
			Thread.sleep(2000);
			DeleteAPIToken.click();
			System.out.println("Clicked API Token delete" );
			log.info("Clicked API Token delete" );
			Thread.sleep(2000);
			ApplyButton.click();
			System.out.println("Clicked API Token delete apply" );
			log.info("Clicked API Token delete apply" );
			Thread.sleep(3000);
			getToastMsg();
			sa.assertEquals(getToastMsg().getText(),"Success\n" + 		
					"API Token Deleted Successfully!");
			sa.assertAll();
			return EditAPIToken;		
		}
		
}
