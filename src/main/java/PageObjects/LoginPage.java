package PageObjects;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.asserts.SoftAssert;

import Resources.DataFile;
import Resources.base;

public class LoginPage extends base{
	
	public static Logger log =LogManager.getLogger(LoginPage.class.getName());
	
	//Declare WebDriver Globally
	public WebDriver driver;
	
	SoftAssert sa = new SoftAssert();	
	
	//Constructor
	public LoginPage(WebDriver driver)
	{
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	//Page Objects
	@FindBy(how = How.XPATH, using="//input[@name='email']")
    WebElement EmailAddress;
	
	@FindBy(how = How.XPATH, using="//input[@name='password']")
    WebElement Password;
	
	@FindBy(how = How.XPATH, using="//span[normalize-space()='Login']")
    WebElement LoginButton;
	
	@FindBy(how = How.XPATH, using="//span[normalize-space()='Next']")
    WebElement NextButton;
	
	@FindBy(how = How.XPATH, using="/html/body/div[1]/div[2]/div/div[2]/div/div/form/div[2]/div[1]/label/span[1]/span[1]/input")
	WebElement RememberMeCheckbox;
	
	//Page Methods
	
	public WebElement getEmailAddress() throws IOException
	{
		DataFile df = new DataFile();
		ArrayList<String> data =df.getData("testv2_login_email_correct");		
		EmailAddress.clear();
		//EmailAddress.click();
		EmailAddress.sendKeys(data.get(1));
		System.out.println("Entered Email Address:"+EmailAddress.getAttribute("value"));
		log.info("Entered Email Address:"+EmailAddress.getAttribute("value"));
		return EmailAddress;
	}
	
	public WebElement getPassword() throws IOException
	{
		DataFile df = new DataFile();
		ArrayList<String> data =df.getData("testv2_login_password_correct");		
		Password.clear();
		//Password.click();
		Password.sendKeys(data.get(1));
		System.out.println("Entered Password:"+Password.getAttribute("value"));
		log.info("Entered Password:"+Password.getAttribute("value"));
		return Password;
	}	
	public WebElement getLoginButton()
	{
		waitForElementToBeVisible(LoginButton, 30, 2);
		LoginButton.click();
		System.out.println("Clicked on Login Button");
		log.info("Clicked on Login Button");
		return LoginButton;		
	}
	
	public WebElement getNextButton()
	{
		waitForElementToBeVisible(NextButton, 30, 2);
		NextButton.click();
		System.out.println("Clicked on Next Button");
		//log.info("Clicked on Login Button");
		return NextButton;		
	}
	
	public WebElement getRememberMeCheckbox()
	{
		//waitForElementToBeVisible(RememberMeCheckbox, 30, 2);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
	    executor.executeScript("arguments[0].click();", RememberMeCheckbox);
		//RememberMeCheckbox.click();
		System.out.println("Clicked on RememberMeCheckbox");	
		return LoginButton;		
	}
	
}
