package PageObjects;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import Resources.DataFile;
import Resources.base;

public class AlarmsPage extends base{
	 
	public static Logger log =LogManager.getLogger(AlarmsPage.class.getName());
	
	//Declare WebDriver Globally
	public WebDriver driver;
			
	//Constructor
	public AlarmsPage(WebDriver driver)
	{
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}

	//Page Objects
	@FindBy(how = How.XPATH, using="//span[contains(text(),'Alarms')]")
	public WebElement AlarmsTab;
	
	@FindBy(how = How.XPATH, using="//div[contains(text(),'Active Alarms')]")
	public WebElement ActiveAlarms;
	
	@FindBy(how = How.XPATH, using="//div[contains(text(),'Predictive Maintenance')]")
	public WebElement PredictiveMaintainance;
	
	@FindBy(how = How.XPATH, using="//div[contains(text(),'Performance Optimization')]")
	public WebElement PerformanceOptimization;
	
	@FindBy(how = How.XPATH, using="//body/div[@id='root']/div[2]/div[1]/div[2]/div[2]/div[4]/div[1]/div[3]/div[1]/button[6]/span[1]/div[1]/button[1]")
	public WebElement ClearAllFilterButton;
		
	@FindBy(how = How.XPATH, using="//span[contains(text(),'Clear All filters')]")
	public WebElement ClearAllFilter;
		
	@FindBy(how = How.XPATH, using="//body/div[@id='root']/div[2]/div[1]/div[2]/div[2]/div[4]/div[1]/div[3]/div[1]/button[1]/span[1]/button[1]")
	public WebElement SeverityFilter;
		
	@FindBy(how = How.XPATH, using="//tbody/tr[1]/td[1]/span[1]/span[1]/input[1]")
	public WebElement CriticalSeverityFilter;
		
	@FindBy(how = How.XPATH, using="//tbody/tr[1]/td[2]/span[1]/span[1]/input[1]")
	public WebElement AlarmFirstCheckbox;
		
	@FindBy(how = How.XPATH, using="//body/div[@id='root']/div[2]/div[1]/div[2]/div[2]/div[5]/div[1]/div[1]/div[1]/div[1]/div[1]/table[1]/div[1]/button[1]")
	public WebElement FixedButton;
		
	@FindBy(how = How.XPATH, using="//body/div[@id='root']/div[2]/div[1]/div[2]/div[2]/div[5]/div[1]/div[1]/div[1]/div[1]/div[1]/table[1]/div[1]/button[2]")
	public WebElement FalseAlarmButton;
		
	@FindBy(how = How.XPATH, using="//h4[normalize-space()='Critical']")
	public WebElement CriticalAlarm;
	
	@FindBy(how = How.XPATH, using="/html/body/div[1]/div[2]/div/div[2]/div[2]/div[3]/div[2]/div/div[1]/div/div[1]")
	public WebElement CriticalAlarmColor;
	
	@FindBy(how = How.XPATH, using="//h4[normalize-space()='High']")
	public WebElement HighAlarm;
	
	@FindBy(how = How.XPATH, using="/html/body/div[1]/div[2]/div/div[2]/div[2]/div[3]/div[2]/div/div[2]/div/div[1]")
	public WebElement HighAlarmColor;
	
	@FindBy(how = How.XPATH, using="//h4[normalize-space()='Moderate']")
	public WebElement ModerateAlarm;
	
	@FindBy(how = How.XPATH, using="/html/body/div[1]/div[2]/div/div[2]/div[2]/div[3]/div[3]/div/div[1]/div/div[1]")
	public WebElement ModerateAlarmColor;
	
	@FindBy(how = How.XPATH, using="//h4[normalize-space()='Low']")
	public WebElement LowAlarm;
	
	@FindBy(how = How.XPATH, using="/html/body/div[1]/div[2]/div/div[2]/div[2]/div[3]/div[3]/div/div[2]/div/div[1]")
	public WebElement LowAlarmColor;
	
	@FindBy(how = How.XPATH, using="//span[contains(text(),'Tap to search alarms')]")
	public WebElement AlarmSearch;
	
	@FindBy(how = How.XPATH, using="//input[@id='name']")
	public WebElement AlarmSearchField;
	
	@FindBy(how = How.XPATH, using="//tbody/tr[1]/td[4]/div[1]")
	public WebElement Alarm1stName;
	
	@FindBy(how = How.XPATH, using="//span[contains(text(),'Last ___ Days')]")
	public WebElement AlarmAgeSearch;
	
	@FindBy(how = How.XPATH, using="//tbody/tr[1]")
	public WebElement AlarmFirstRow;
	
	@FindBy(how = How.XPATH, using="//body/div[@id='root']/div[2]/div[1]/div[2]/div[2]/div[5]/div[1]/button[1]")
	public WebElement PathFilter;
	
	@FindBy(how = How.XPATH, using="//tbody/tr[1]/td[1]/div[1]/div[2]")
	public WebElement PathFilter1stOption;
	
	@FindBy(how = How.XPATH, using="//body/div[@id='simple-popover']/div[1]")
	public WebElement ExternalClick;
	
	@FindBy(how = How.XPATH, using="//body/div[@id='root']/div[2]/div[1]/div[2]/div[2]/div[6]")
	public WebElement NoAlarmText;
	
	@FindBy(how = How.XPATH, using = "//body/div[@id='root']/div[3]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]")
	public WebElement toastWebElmnt;
	
	@FindBy(how = How.XPATH, using = "//body/div[@id='root']/div[2]/div[1]/div[2]/div[2]/div[4]/div[1]/div[4]/*[1]")
	public WebElement RightFilterScroll;
	
	@FindBy(how = How.XPATH, using = "//body/div[@id='root']/div[2]/div[1]/div[2]/div[2]/div[4]/div[1]/div[1]")
	public WebElement LeftFilterScroll;
		
	//Page Methods	
	SoftAssert sa = new SoftAssert();
	public WebElement getToastMsg()
	{		
	   if (base.isElementExists(toastWebElmnt)) 
	   {
			System.out.println("Toast message : "+toastWebElmnt.getText());
			log.info("Toast message : "+toastWebElmnt.getText());
		} else 
		{
			System.out.println("Toast message not found ");
			log.info("Toast message not found ");
		}
		return toastWebElmnt;		
	}
	public WebElement getAlarmsTab() throws InterruptedException
	{
		waitForElementToBeVisible(AlarmsTab, 30, 2);
		Thread.sleep(3000);
		AlarmsTab.click();
		System.out.println("Clicked on Alarms tab");
		log.info("Clicked on Alarms tab");

		Thread.sleep(4000);
		String actualurl = driver.getCurrentUrl();
		System.out.println(actualurl);
		log.info(actualurl);
		String expectedurl = "https://testv2.shorelineiot.com/testautomation/app/alarms?severity=ALERT_FATAL,ALERT_ERROR";
		Assert.assertEquals(actualurl,expectedurl);

		Thread.sleep(12000);
		sa.assertEquals(ActiveAlarms.getText(), "Active Alarms");
		sa.assertEquals(PredictiveMaintainance.getText(), "Predictive Maintenance");
		sa.assertEquals(PerformanceOptimization.getText(), "Performance Optimization");
		sa.assertAll();

		return AlarmsTab;		
	}		 
	public WebElement clearAllAlarms() throws InterruptedException
	{
		RightFilterScroll.click();	
		Thread.sleep(1000);
		if(RightFilterScroll.isDisplayed())
		{
			RightFilterScroll.click();	
		}
		Thread.sleep(2000);
		waitForElementToBeVisible(ClearAllFilter, 30, 2);
		if(ClearAllFilterButton.isEnabled()) {
		ClearAllFilter.click(); 
		Thread.sleep(3000);
		}
		else {
			LeftFilterScroll.click();
			SeverityFilter.click();
			System.out.println("Clicked on  Severity filter");
			log.info("Clicked on Severity filter");
			Thread.sleep(3000);
			CriticalSeverityFilter.click();	
			driver.navigate().refresh();
	  		Thread.sleep(5000);
			ClearAllFilter.click();
		}
		System.out.println("Clicked on clear all filter2"+ ClearAllFilterButton.isEnabled());
		log.info("Clicked on clear all filter");
		sa.assertFalse(ClearAllFilterButton.isEnabled());
		sa.assertAll();
		return ClearAllFilter;
	}
	public WebElement SelectSeverityFilter() throws InterruptedException 
	{

		LeftFilterScroll.click();
		Thread.sleep(1000);

		SeverityFilter.click();
		System.out.println("Clicked on  Severity filter");
		log.info("Clicked on Severity filter");
		Thread.sleep(3000);
		CriticalSeverityFilter.click();		
		System.out.println("Selected SeverityFilter: "+SeverityFilter.getText());
		log.info("Selected SeverityFilter: "+SeverityFilter.getText());
		sa.assertEquals(SeverityFilter.getText(), "Critical");
		sa.assertAll();
		return CriticalSeverityFilter;
	}
    public WebElement fixedAlarms() throws InterruptedException 
    {

    	RightFilterScroll.click();	
		Thread.sleep(1000);
		if(RightFilterScroll.isDisplayed())
		{
			RightFilterScroll.click();	
		}
		Thread.sleep(2000);
		if(ClearAllFilterButton.isEnabled()) {
		ClearAllFilter.click(); 
		Thread.sleep(2000);
		}
		LeftFilterScroll.click();

		Thread.sleep(1000);
		AlarmFirstCheckbox.click();
		System.out.println("Alarm First Checkbox Clicked");
		log.info("Alarm First Checkbox Clicked");
		FixedButton.click();
		System.out.println("Alarm Fixed button Clicked");
		log.info("Alarm Fixed button Clicked");
		Thread.sleep(4000);
		sa.assertEquals(toastWebElmnt.getText(), "Success\n" + 
				"Alarm fixed successfully!");
		sa.assertAll();
		return FixedButton;		 
	}
	public WebElement falseAlarms() throws InterruptedException 
	{

		Thread.sleep(4000);
		AlarmFirstCheckbox.click();
		System.out.println("Alarm First Checkbox Clicked");
		log.info("Alarm First Checkbox Clicked");
		FalseAlarmButton.click();
		System.out.println("Alarm False Alarm button Clicked");
		log.info("Alarm False Alarm button Clicked");
		Thread.sleep(4000);
		sa.assertEquals(toastWebElmnt.getText(), "Success\n" + 
				"Successfully marked as false alarm");
		sa.assertAll();
	    return FalseAlarmButton;		 
	}
	public WebElement getCriticalAlarms()
	{
		waitForElementToBeVisible(CriticalAlarm, 30, 2);
		System.out.println("Alarm:"+CriticalAlarm.getText());		
		log.info("Alarm:"+CriticalAlarm.getText());
		return CriticalAlarm;		
	}
	public WebElement getCriticalAlarmsColor()
	{
		String colorCode= CriticalAlarmColor.getCssValue("background-color");
		System.out.println("Color Code in RGB:"+colorCode);
		String ExpectedColorCodeInRGB= "rgba(206, 0, 0, 1)";
		Assert.assertEquals(colorCode,ExpectedColorCodeInRGB);
		return CriticalAlarmColor;		
	}
	public WebElement getHighAlarms()
	{
		waitForElementToBeVisible(HighAlarm, 30, 2);
		System.out.println("Alarm:"+HighAlarm.getText());		
		log.info("Alarm:"+HighAlarm.getText());
		return HighAlarm;		
	}
	public WebElement getHighAlarmsColor()
	{
		String colorCode= HighAlarmColor.getCssValue("background-color");
		System.out.println("Color Code in RGB:"+colorCode);
		String ExpectedColorCodeInRGB= "rgba(253, 140, 0, 1)";
		Assert.assertEquals(colorCode,ExpectedColorCodeInRGB);
		return HighAlarmColor;		
	}
	public WebElement getModerateAlarms()
	{
		waitForElementToBeVisible(ModerateAlarm, 30, 2);
		System.out.println("Alarm:"+ModerateAlarm.getText());		
		log.info("Alarm:"+ModerateAlarm.getText());
		return ModerateAlarm;		
	}
	public WebElement getModerateAlarmsColor()
	{
		String colorCode= ModerateAlarmColor.getCssValue("background-color");
		System.out.println("Color Code in RGB:"+colorCode);
		String ExpectedColorCodeInRGB= "rgba(253, 197, 0, 1)";
		Assert.assertEquals(colorCode,ExpectedColorCodeInRGB);
		return HighAlarmColor;		
	}
	public WebElement getLowAlarms()
	{
		waitForElementToBeVisible(LowAlarm, 30, 2);
		System.out.println("Alarm:"+LowAlarm.getText());		
		log.info("Alarm:"+LowAlarm.getText());
		return LowAlarm;		
	}
	public WebElement getLowAlarmsColor()
	{
		String colorCode= LowAlarmColor.getCssValue("background-color");
		System.out.println("Color Code in RGB:"+colorCode);
		String ExpectedColorCodeInRGB= "rgba(129, 173, 232, 1)";
		Assert.assertEquals(colorCode,ExpectedColorCodeInRGB);
		return LowAlarmColor;		
	}
	public WebElement alarmsSearch() throws InterruptedException {

		
		RightFilterScroll.click();	
		Thread.sleep(1000);
		if(RightFilterScroll.isDisplayed())
		{
			RightFilterScroll.click();	
		}
		Thread.sleep(2000);

		if(ClearAllFilterButton.isEnabled()) {
		ClearAllFilter.click(); 
		Thread.sleep(2000);
		}

		LeftFilterScroll.click();

		String AlarmName = Alarm1stName.getText();
		AlarmSearch.click();
		AlarmSearchField.sendKeys(Keys.CONTROL + "a");
		AlarmSearchField.sendKeys(Keys.DELETE);
		AlarmSearchField.sendKeys(AlarmName);
		System.out.println("Alarm search result:"+Alarm1stName.getText());		
		log.info("Alarm search result :"+Alarm1stName.getText());
		//Assert.assertEquals(AlarmName, Alarm1stName.getText());
		//Thread.sleep(2000);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
	    executor.executeScript("arguments[0].click();", ExternalClick);
        //sa.assertAll();
		return Alarm1stName;
	}
	public WebElement AlarmAgeSearch() throws IOException, InterruptedException {
		AlarmAgeSearch.click();
		AlarmSearchField.sendKeys(Keys.CONTROL + "a");
		AlarmSearchField.sendKeys(Keys.DELETE);
		DataFile df = new DataFile();
		ArrayList<String> data =df.getData("AlarmAge");		
		AlarmSearchField.sendKeys(data.get(1));
		//System.out.println("Alarm search result:"+AlarmFirstRow.getText());		
		//log.info("Alarm search result :"+AlarmFirstRow.getText());
		Thread.sleep(2000);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
	    executor.executeScript("arguments[0].click();", ExternalClick);
		return AlarmAgeSearch;
		
	}
	public WebElement AlarmPath() throws InterruptedException {
		PathFilter.click();
		System.out.println("Clicked on Path Filter");
		log.info("Clicked on Path Filter");
		PathFilter1stOption.click();
		System.out.println("Clicked on Path Filter 1st option");
		log.info("Clicked on Path Filter 1st option");
		if((!"All".equals(AlarmFirstRow.getText()))) {
		System.out.println("Alarm search result:"+AlarmFirstRow.getText());		
		log.info("Alarm search result :"+AlarmFirstRow.getText());
		}
		else {
			System.out.println("Alarm search result:"+NoAlarmText.getText());		
			log.info("Alarm search result :"+NoAlarmText.getText());
		}                                                   		
		Thread.sleep(2000);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
	    executor.executeScript("arguments[0].click();", ExternalClick);
		return PathFilter;
	}
}