package PageObjects;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import Resources.DataFile;
import Resources.base;

public class NarrowbandPage extends base{
	
	public static Logger log =LogManager.getLogger(DevicesPage.class.getName());
	
	//Declare WebDriver Globally
	public WebDriver driver;
			
	//Constructor
	public NarrowbandPage(WebDriver driver)
	{
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	//Page Objects
	@FindBy(how = How.XPATH, using="//span[contains(text(),'Add Sensor')]")
	public WebElement AddSensor;
	
	@FindBy(how = How.XPATH, using="//div[contains(text(),'Narrowband')]")
	public WebElement Narrowband;
	
	@FindBy(how = How.XPATH, using="//*[@value='manual']")
	public WebElement ManualNarrowbandOption;
	
	@FindBy(how = How.XPATH, using="//input[@id='sq_118i']")
	public WebElement ManualNarrowbandName;
	
	@FindBy(how = How.XPATH, using="//input[@id='sq_122i']")
	public WebElement ManualNarrowbandMinFreq;
	
	@FindBy(how = How.XPATH, using="//input[@id='sq_123i']")
	public WebElement ManualNarrowbandMaxFreq;
	
	@FindBy(how = How.XPATH, using="//*[@class=' sv_complete_btn']")
	public WebElement ManualNarrowbandCompleteButton;
	
	@FindBy(how = How.XPATH, using="//a[contains(text(),'TestManual, X')]")
	public WebElement TestManualManualNarrowband;
	
	@FindBy(how = How.XPATH, using="//body/div[@id='root']/div[2]/div[1]/div[2]/div[2]/div[4]/div[14]/div[2]/div[2]/button[1]/span[1]")
	public WebElement TestManualManualNarrowbandMenu;
	
	@FindBy(how = How.XPATH, using="//body/div[@id='simple-menu']/div[3]/ul[1]/li[3]")
	public WebElement RemoveNarrowband;
	
	@FindBy(how = How.XPATH, using="//span[contains(text(),'Delete')]")
	public WebElement Delete;
	
	//Automatic narrowband	
	@FindBy(how = How.XPATH, using="//div[@id='mui-component-select-class']")
	public WebElement NarrowbandClassDropdown;
	
	@FindBy(how = How.XPATH, using="//body/div[@id='menu-class']/div[3]/ul[1]/li[5]")
	public WebElement NarrowbandElectricMotorGenerator;
	
	@FindBy(how = How.XPATH, using="//div[@id='mui-component-select-subclass']")
	public WebElement NarrowbandSubclassDropdown;
	
	@FindBy(how = How.XPATH, using="//body/div[@id='menu-subclass']/div[3]/ul[1]/li[1]")
	public WebElement NarrowbandACMotor;

	@FindBy(how = How.XPATH, using="//div[@id='mui-component-select-manufacturer']")
	public WebElement NarrowbandManufactureDropdown;
	
	@FindBy(how = How.XPATH, using="//body/div[@id='menu-manufacturer']/div[3]/ul[1]/li[3]")
	public WebElement NarrowbandBaldorRelience;
	
	@FindBy(how = How.XPATH, using="//div[@id='mui-component-select-model']")
	public WebElement NarrowbandModelDropdown;
	
	@FindBy(how = How.XPATH, using="//body/div[@id='menu-model']/div[3]/ul[1]/li[1]")
	public WebElement NarrowbandModelName;
	
	@FindBy(how = How.XPATH, using="//span[contains(text(),'Next')]")
	public WebElement NarrowbandNextButton;
	
	@FindBy(how = How.XPATH, using="//span[contains(text(),'Complete')]")
	public WebElement NarrowbandCompleteButton;
	
	@FindBy(how = How.XPATH, using="//span[contains(text(),'Save')]")
	public WebElement Narrowbandsave;
	
	@FindBy(how = How.XPATH, using="//tbody/tr[1]/td[7]/div[1]/button[1]/span[1]/div[1]/*[1]")
	public WebElement AutoNarrowbandEditIcon;
	
	@FindBy(how = How.XPATH, using="//tbody/tr[1]/td[6]/div[1]/div[1]/input[1]")
	public WebElement AutoNarrowbandCriticalEdit;
	
	@FindBy(how = How.XPATH, using="//tbody/tr[1]/td[7]/div[1]/button[2]/span[1]/div[1]/*[1]")
	public WebElement AutoNarrowbandEditSave;
	
	@FindBy(how = How.XPATH, using="//span[contains(text(),'Back')]")
	public WebElement AutoNarrowbandBackButton;
	
	@FindBy(how = How.XPATH, using="//span[contains(text(),'Previous')]")
	public WebElement AutoNarrowbandPreviousButton;
	
	@FindBy(how = How.XPATH, using="//body/div[@id='menu-model']/div[3]/ul[1]/li[2]")
	public WebElement AutoNarrowband2ndModel;
	
	@FindBy(how = How.XPATH, using="//tbody/tr[1]/td[7]/div[1]/button[2]/span[1]/*[1]")
	public WebElement AutoNarrowbandDelete;
	
	@FindBy(how = How.XPATH, using = "//body/div[@id='root']/div[3]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]")
	public WebElement toastWebElmnt;
	
	//Page Methods
	public WebElement getToastMsg()
	{		
	   if (base.isElementExists(toastWebElmnt))
	   {
			System.out.println("Toast message : "+toastWebElmnt.getText());
			log.info("Toast message : "+toastWebElmnt.getText());
		} else
		{
			System.out.println("Toast message not found ");
			log.info("Toast message not found ");
		}
		return toastWebElmnt;		
	}
	
	public WebElement AddSensor()
	{
		waitForElementToBeVisible(AddSensor, 30, 2);
		AddSensor.click();
		System.out.println("Clicked on AddSensor");
		log.info("Clicked on AddSensor");
		return AddSensor;		
	}
	public WebElement selectNarrowband() throws InterruptedException
	{
		Thread.sleep(3000);
		waitForElementToBeVisible(Narrowband, 30, 2);
		Narrowband.click();
		System.out.println("Clicked on Narrowband");
		log.info("Clicked on Narrowband");
		return Narrowband;		
	}
	public WebElement selectManualNarrowband() throws InterruptedException
	{
		Thread.sleep(3000);
		//waitForElementToBeVisible(ManualNarrowbandOption, 30, 2);
		ManualNarrowbandOption.click();
		System.out.println("Clicked on ManualNarrowbandOption");
		log.info("Clicked on ManualNarrowbandOption");
		return ManualNarrowbandOption;		
	}
	public WebElement ManualNarrowbandForm() throws InterruptedException, IOException
	{
		selectManualNarrowband();
		Thread.sleep(3000);
		DataFile df = new DataFile();
		ArrayList<String> data =df.getData("ManualNarrowbandName");		
		ManualNarrowbandName.click();
		ManualNarrowbandName.sendKeys(data.get(1));
		DataFile df2 = new DataFile();
		ArrayList<String> data2 =df2.getData("ManualNarrowbandFmin");	
		ManualNarrowbandMinFreq.click();
		ManualNarrowbandMinFreq.sendKeys(data2.get(1));
		DataFile df3 = new DataFile();
		ArrayList<String> data3 =df3.getData("ManualNarrowbandFmax");	
		ManualNarrowbandMaxFreq.click();
		ManualNarrowbandMaxFreq.sendKeys(data3.get(1));
		base.scrollTillElement(ManualNarrowbandCompleteButton);
		Thread.sleep(3000);
		ManualNarrowbandCompleteButton.click();
		Thread.sleep(3000);
		System.out.println("Clicked on Manual Narrowband Complete Button");
		log.info("Clicked on Manual Narrowband Complete Button");
		getToastMsg();
		return ManualNarrowbandName;		
	}
	public WebElement deleteNarrowband() throws InterruptedException 
	{
		base.scrollTillElement(TestManualManualNarrowband);
		Thread.sleep(3000);
		TestManualManualNarrowbandMenu.click();
		System.out.println("Clicked on Manual Narrowband Menu Button");
		log.info("Clicked on Manual Narrowband Menu Button");
		Thread.sleep(2000);
		RemoveNarrowband.click();
		System.out.println("Clicked on remove Manual Narrowband ");
		log.info("Clicked on remove Manual Narrowband ");
		Thread.sleep(2000);
		Delete.click();
		System.out.println("Clicked on delete Manual Narrowband ");
		log.info("Clicked on Manual delete Narrowband ");
		Thread.sleep(3000);
		getToastMsg();
		return TestManualManualNarrowband;
	}
	public WebElement AutoNArrowband() throws InterruptedException 
	{
	    NarrowbandClassDropdown.click();
		NarrowbandElectricMotorGenerator.click();
		System.out.println("Clicked on Automatic Narrowband class");
		log.info("Clicked on Automatic Narrowband class");
		Thread.sleep(1000);
		NarrowbandSubclassDropdown.click();
		NarrowbandACMotor.click();
		System.out.println("Clicked on Automatic Narrowband sub class");
		log.info("Clicked on Automatic Narrowband sub class");
		Thread.sleep(1000);
		NarrowbandManufactureDropdown.click();
		NarrowbandBaldorRelience.click();
		System.out.println("Clicked on Automatic Narrowband Manufacture Dropdown");
		log.info("Clicked on Automatic Narrowband Manufacture Dropdown");
		Thread.sleep(1000);
		NarrowbandModelDropdown.click();
		NarrowbandModelName.click();
		System.out.println("Clicked on Automatic Narrowband model Dropdown");
		log.info("Clicked on Automatic Narrowband model Dropdown");
		Thread.sleep(1000);
		base.scrollTillElement(NarrowbandNextButton);
		NarrowbandNextButton.click();
		Thread.sleep(3000);
		base.scrollTillElement(NarrowbandCompleteButton);
		NarrowbandCompleteButton.click();
		Thread.sleep(6000);
		base.scrollTillElement(Narrowbandsave);
		Narrowbandsave.click();
		System.out.println("Clicked on Automatic Narrowband save");
		log.info("Clicked on Automatic Narrowband save");
		Thread.sleep(1000);
		Thread.sleep(3000);
		getToastMsg();
		return Narrowbandsave;
	}
	public WebElement AutoNarrowbandValueEdit() throws InterruptedException 
	{
		AutoNarrowbandEditIcon.click();
		System.out.println("Clicked on Automatic Narrowband edit");
		log.info("Clicked on Automatic Narrowband edit");
		Thread.sleep(2000);
		AutoNarrowbandCriticalEdit.click();
		System.out.println("Clicked on Automatic Narrowband critical edit");
		log.info("Clicked on Automatic Narrowband critical edit");
		Thread.sleep(2000);
		AutoNarrowbandCriticalEdit.sendKeys(Keys.CONTROL + "a");
		AutoNarrowbandCriticalEdit.sendKeys(Keys.DELETE);
		Thread.sleep(2000);
		AutoNarrowbandCriticalEdit.sendKeys("1");
		Thread.sleep(2000);
		System.out.println(" Automatic Narrowband critical value edited");
		log.info("Automatic Narrowband critical value edited");
		Thread.sleep(1000);
		AutoNarrowbandEditSave.click();
		System.out.println("Clicked on Automatic Narrowband critical edit save");
		log.info("Clicked on Automatic Narrowband critical edit save");
		Thread.sleep(1000);
		base.scrollTillElement(Narrowbandsave);
		Narrowbandsave.click();
		System.out.println("Clicked on Automatic Narrowband save");
		log.info("Clicked on Automatic Narrowband critical save");
		Thread.sleep(3000);
		getToastMsg();
		return Narrowbandsave;
	}
    public WebElement AutoNArrowbandClassEdit() throws InterruptedException 
    {
	    base.scrollTillElement(AutoNarrowbandBackButton);
	    AutoNarrowbandBackButton.click();
	    System.out.println("Clicked on Automatic Narrowband back");
	    log.info("Clicked on Automatic Narrowband back");
	    Thread.sleep(4000);
	    base.scrollTillElement(AutoNarrowbandPreviousButton);
	    AutoNarrowbandPreviousButton.click();
	    System.out.println("Clicked on Automatic Narrowband previous");
	    log.info("Clicked on Automatic Narrowband previous");
	    Thread.sleep(2000);
	    NarrowbandModelDropdown.click();
	    AutoNarrowband2ndModel.click();
	    System.out.println("Clicked on Automatic Narrowband model Dropdown");
	    log.info("Clicked on Automatic Narrowband model Dropdown");
	    Thread.sleep(1000);
	    base.scrollTillElement(NarrowbandNextButton);
	    NarrowbandNextButton.click();
	    Thread.sleep(3000);
	    base.scrollTillElement(NarrowbandCompleteButton);
	    NarrowbandCompleteButton.click();
	    Thread.sleep(6000);
	    base.scrollTillElement(Narrowbandsave);
	    Narrowbandsave.click();
	    System.out.println("Clicked on Automatic Narrowband save");
	    log.info("Clicked on Automatic Narrowband save");
	    Thread.sleep(3000);
	    getToastMsg();
	    return Narrowbandsave;
    }
    public WebElement AutoNarrowbandDelete() throws InterruptedException 
    {
		AutoNarrowbandDelete.click();
		System.out.println("Clicked on Automatic Narrowband delete");
		log.info("Clicked on Automatic Narrowband delete");
		Delete.click();
		Thread.sleep(2000);
		base.scrollTillElement(Narrowbandsave);
		Narrowbandsave.click();
		System.out.println("Clicked on Automatic Narrowband save");
		log.info("Clicked on Automatic Narrowband critical save");
		Thread.sleep(3000);
		getToastMsg();
		return Narrowbandsave;
	}
}
