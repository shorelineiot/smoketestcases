package PageObjects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import Resources.base;

public class DashboardPage extends base{
	
    public static Logger log =LogManager.getLogger(DashboardPage.class.getName());
	
	//Declare WebDriver Globally
	public WebDriver driver;
	
	SoftAssert sa = new SoftAssert();
	
	//Constructor
	public DashboardPage(WebDriver driver)
	{
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}

	//Page Objects
	@FindBy(how = How.XPATH, using="//span[normalize-space()='Dashboard']")
    WebElement DashboardTab;
	
	@FindBy(how = How.XPATH, using="/html[1]/body[1]/div[1]/div[2]/div[1]/div[1]/header[1]/div[1]/button[3]/span[1]/p[1]/div[1]")
    WebElement UserName;
	
	@FindBy(how = How.XPATH, using="//li[normalize-space()='Log Out']")
    WebElement UserLogout;
	
	@FindBy(how = How.XPATH, using="//span[normalize-space()='Alarms']")
    WebElement AlarmsTab;
	
	@FindBy(how = How.XPATH, using="//span[normalize-space()='Devices']")
    WebElement DevicesTab;
	
	@FindBy(how = How.XPATH, using="//span[normalize-space()='Rules']")
    WebElement RulesTab;
	
	@FindBy(how = How.XPATH, using="//span[normalize-space()='Workflow']")
    WebElement WorkflowTab;
	
	@FindBy(how = How.XPATH, using="//span[normalize-space()='Firmware']")
    WebElement FirmwareTab;
	
	@FindBy(how = How.XPATH, using="//span[normalize-space()='Activity Logs']")
    WebElement ActivityLogsTab;
	
	//Page Methods
	public WebElement getDashboardTab()
	{
		waitForElementToBeVisible(DashboardTab, 30, 2);
		DashboardTab.click();
		System.out.println("Clicked on DashboardTab");
		log.info("Clicked on DashboardTab");
		return DashboardTab;		
	}
	public WebElement getUserName()
	{
		waitForElementToBeVisible(UserName, 30, 2);
		String actualuser = UserName.getText();
		System.out.println("ActualUser:"+actualuser);
		log.info("ActualUser:"+actualuser);
		String expecteduser = "QA";
		System.out.println("ExpectedUser:"+expecteduser);
		Assert.assertEquals(actualuser, expecteduser);
		return UserName;		
	}
	public WebElement getUserLogout() throws InterruptedException
	{
		waitForElementToBeVisible(UserLogout, 30, 2);
		UserLogout.click();
		System.out.println("Clicked on UserLogout");
		log.info("Clicked on UserLogout");
		Thread.sleep(3000);
		String actualurl = driver.getCurrentUrl();
		System.out.println("Actual Url:"+actualurl);
		log.info("Actual Url:"+actualurl);
		String expectedurl = "https://testv2.shorelineiot.com/testautomation/auth/login";
		sa.assertEquals(actualurl,expectedurl);
		sa.assertAll();
		return UserLogout;		
	}
	public WebElement getAlarmsTab()
	{
		waitForElementToBeVisible(AlarmsTab, 30, 2);
		AlarmsTab.click();
		System.out.println("Clicked on AlarmsTab");
		log.info("Clicked on AlarmsTab");
		return AlarmsTab;		
	}
	public WebElement getDevicesTab()
	{
		Actions action =new Actions(driver);
		waitForElementToBeClickable(DevicesTab, 30, 2);
		//DevicesTab.click();
		action.moveToElement(DevicesTab).click().build().perform();
		System.out.println("Clicked on DevicesTab");
		log.info("Clicked on DevicesTab");
		//String actualurl = driver.getCurrentUrl();
		//System.out.println("Actual Url:"+actualurl);
		//log.info("Actual Url:"+actualurl);
		//String expectedurl = "https://testv2.shorelineiot.com/tesstb9bb/app/devices";
		//sa.assertEquals(actualurl,expectedurl);
		//sa.assertAll();
		return DevicesTab;		
	}
	public WebElement getRulesTab()
	{
		waitForElementToBeVisible(RulesTab, 30, 2);
		RulesTab.click();
		System.out.println("Clicked on RulesTab");
		log.info("Clicked on RulesTab");
		return RulesTab;		
	}
	public WebElement getWorkflowTab()
	{
		waitForElementToBeVisible(WorkflowTab, 30, 2);
		WorkflowTab.click();
		System.out.println("Clicked on WorkflowTab");
		log.info("Clicked on WorkflowTab");
		return WorkflowTab;		
	}
	public WebElement getFirmwareTab()
	{
		waitForElementToBeVisible(FirmwareTab, 30, 2);
		FirmwareTab.click();
		System.out.println("Clicked on FirmwareTab");
		log.info("Clicked on FirmwareTab");
		return FirmwareTab;		
	}
	public WebElement getActivityLogsTab()
	{
		waitForElementToBeVisible(ActivityLogsTab, 30, 2);
		ActivityLogsTab.click();
		System.out.println("Clicked on ActivityLogsTab");
		log.info("Clicked on ActivityLogsTab");
		return ActivityLogsTab;		
	}
}
