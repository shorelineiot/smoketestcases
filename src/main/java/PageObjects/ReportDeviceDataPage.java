package PageObjects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.asserts.SoftAssert;

public class ReportDeviceDataPage {
public static Logger log =LogManager.getLogger(ReportDeviceDataPage.class.getName());
	
	//Declare WebDriver Globally
	public WebDriver driver;
		
	//Constructor
	public ReportDeviceDataPage(WebDriver driver)
	{
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}

	//Page Objects
	@FindBy(how = How.XPATH, using="//span[contains(text(),'Reports')]")
	WebElement Reports;
	
	@FindBy(how = How.XPATH, using="//body/div[@id='root']/div[2]/div[1]/div[2]/div[1]/div[1]/ul[1]/div[3]/div[1]/div[1]/div[1]/a[1]/div[1]")
	WebElement ReportsDeviceData;
	
	@FindBy(how = How.XPATH, using="//thead/tr[1]")
	WebElement TableHeaders;
	
	@FindBy(how = How.XPATH, using="//body/div[@id='root']/div[2]/div[1]/div[2]/div[2]/div[2]/div[1]/div[2]/div[1]/div[1]/span[1]/button[1]")
	WebElement ReportDeviceColumnButton;
	
	@FindBy(how = How.XPATH, using="//input[@id='column-toggle-0']")
	WebElement DeviceDataColumn1stOption;
	
	@FindBy(how = How.XPATH, using="//input[@id='column-toggle-1']")
	WebElement DeviceDataColumn2stOption;
	
	@FindBy(how = How.XPATH, using="//body/div[7]/div[1]")
	WebElement ExternalClick;
	
	SoftAssert sa = new SoftAssert();
	public WebElement getReportsTab() throws InterruptedException
	{
		Reports.click();
		System.out.println("Clicked on Reports tab");
		log.info("Clicked on Reports tab");
		Thread.sleep(2000);
		return Reports;		
	}	
	
	public WebElement getReportDeviceDataTab() throws InterruptedException
	{
		getReportsTab();
		ReportsDeviceData.click();
		System.out.println("Clicked on ReportsDeviceData tab");
		log.info("Clicked on ReportsDeviceData tab");
		Thread.sleep(5000);
		System.out.println("ReportsDeviceData Headers : " + TableHeaders.getText());
		sa.assertEquals(TableHeaders.getText(), "Device Id\n" + 
				"Device Name\n" + 
				"Last Connected\n" + 
				"Radial 25600 (Z Axis)\n" + 
				"Axial 25600 (Y Axis)\n" + 
				"Tangential 25600 (X Axis)\n" + 
				"Radial 3200 (Z Axis)\n" + 
				"Axial 3200 (Y Axis)\n" + 
				"Tangential 3200 (X Axis)\n" + 
				"Radial 100 (Z Axis)\n" + 
				"Axial 100 (Y Axis)\n" + 
				"Tangential 100 (X Axis)\n" + 
				"Battery usage accumulator\n" + 
				"Broadband value\n" + 
				"Surface Temperature count\n" + 
				"Humidity count\n" + 
				"Environment Temperature count\n" + 
				"Surface Temperature value\n" + 
				"Environment Temperature value\n" + 
				"Broadband count\n" + 
				"Total Datapoints\n" + 
				"% Battery Used\n" + 
				"Firmware Version\n" + 
				"Observation\n" + 
				"Remarks\n" + 
				"Actions");
		sa.assertAll();
		return ReportsDeviceData;		
	}	
	
	public WebElement DeviceColumn() throws InterruptedException {
		getReportDeviceDataTab();
		ReportDeviceColumnButton.click();
		System.out.println("DeviceData table column option click ");
		log.info("DeviceData table column option click");
		Thread.sleep(1000);
		DeviceDataColumn1stOption.click();
		System.out.println("DeviceData table Device Type column option click ");
		log.info("DeviceData table Device Type column option click");
		DeviceDataColumn2stOption.click();
		System.out.println("DeviceData table Device Name column option click ");
		log.info("DeviceData table Device Name column option click");
		Thread.sleep(1000);
		System.out.println("DeviceData table column option click ");
		log.info("DeviceData table column option click");
		Thread.sleep(1000);
		JavascriptExecutor executor = (JavascriptExecutor) driver;
	    executor.executeScript("arguments[0].click();", ExternalClick);
	    Thread.sleep(1000);
		System.out.println("DeviceData table Headers: "+TableHeaders.getText());
		log.info("DeviceData table Headers:"+TableHeaders.getText());
		sa.assertEquals(TableHeaders.getText(), "Last Connected\n" + 
				"Radial 25600 (Z Axis)\n" + 
				"Axial 25600 (Y Axis)\n" + 
				"Tangential 25600 (X Axis)\n" + 
				"Radial 3200 (Z Axis)\n" + 
				"Axial 3200 (Y Axis)\n" + 
				"Tangential 3200 (X Axis)\n" + 
				"Radial 100 (Z Axis)\n" + 
				"Axial 100 (Y Axis)\n" + 
				"Tangential 100 (X Axis)\n" + 
				"Battery usage accumulator\n" + 
				"Broadband value\n" + 
				"Surface Temperature count\n" + 
				"Humidity count\n" + 
				"Environment Temperature count\n" + 
				"Surface Temperature value\n" + 
				"Environment Temperature value\n" + 
				"Broadband count\n" + 
				"Total Datapoints\n" + 
				"% Battery Used\n" + 
				"Firmware Version\n" + 
				"Observation\n" + 
				"Remarks\n" + 
				"Actions");
		return ReportDeviceColumnButton;
	}

}
