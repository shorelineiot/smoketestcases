package PageObjects;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import Resources.DataFile;
import Resources.base;

public class DeviceSettingsPage extends base{
	
    public static Logger log =LogManager.getLogger(DeviceSettingsPage.class.getName());
	
	//Declare WebDriver Globally
	public WebDriver driver;
		
	//Constructor
	public DeviceSettingsPage(WebDriver driver)
	{
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}

	//PageObjects
	@FindBy(how = How.XPATH, using="//input[@name='deviceName']")
	WebElement DeviceName;
	
	@FindBy(how = How.XPATH, using="//div[@id='popperMenu']//button[1]//span[1]//*[name()='svg']")
	WebElement DeviceNameSave;
	
	//@FindBy(how = How.XPATH, using="/html/body/div[1]/div[2]/div/div[2]/div[2]/div[2]/span/div/div[1]/div/div/div[2]/div[1]/button/span[1]/svg")
	//*[@id="root"]/div[2]/div/div[2]/div[2]/div[2]/span/div/div[1]/div/div/div[2]/div[1]/button/span[1]/svg
	//html/body/div[1]/div[2]/div/div[2]/div[2]/div[2]/span/div/div[1]/div/div/div[2]/div[1]/button/span[1]/svg
	@FindBy(how = How.XPATH, using="/html/body/div[1]/div[2]/div/div[2]/div[2]/div[2]/span/div/div[1]/div/div/div[2]/div[1]/button")
	WebElement DeviceDelete;
	
	@FindBy(how = How.XPATH, using="//span[normalize-space()='Delete']")
	WebElement DeleteButtonPopup;
		
	@FindBy(how = How.XPATH, using="/div/div/div/div/div/div/div/div/div[2]/p")
	WebElement ToasterMessage;
	
	@FindBy(how = How.XPATH, using="/html[1]/body[1]/div[1]/div[2]/div[1]/div[2]/div[2]/div[3]/div[2]/div[1]/div[6]/div[1]/div[1]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/td[1]/div[2]/div[1]")
    public WebElement DeviceNameField;
    
    @FindBy(how = How.XPATH, using="//body/div[@id='root']/div[2]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/div[4]/a[1]/button[1]")
    public WebElement SettingsIcon;
    
    @FindBy(how = How.XPATH, using="//b[contains(text(),'General Settings')]")
    public WebElement GeneralSettings;
    
    @FindBy(how = How.XPATH, using="//body/div[@id='root']/div[2]/div[1]/div[2]/div[2]/div[2]/span[1]/div[1]/div[1]/div[1]/div[1]/div[2]/div[2]/button[1]/span[1]/*[1]")
    public WebElement RebootDevice;
    
    @FindBy(how = How.XPATH, using = "//body/div[@id='root']/div[3]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]")
	public WebElement toastWebElmnt;
    
    @FindBy(how = How.XPATH, using = "/html[1]/body[1]/div[1]/div[2]/div[1]/div[2]/div[2]/div[2]/span[1]/div[1]/div[1]/div[1]/div[1]/div[3]/form[1]/span[1]/div[1]/div[1]/div[1]/div[3]/div[1]/button[1]/span[1]/*[name()='svg'][1]")
	public WebElement CloudUpdateInterval;
    
    @FindBy(how = How.XPATH, using = "//span[contains(text(),'Select All')]")
	public WebElement CloudUpdateIntervalSelectAll;
    
    @FindBy(how = How.XPATH, using = "//span[contains(text(),'Save')]")
	public WebElement CloudUpdateIntervalSave;
    
    @FindBy(how = How.XPATH, using = "//body/div[@id='root']/div[2]/div[1]/div[2]/div[2]/div[2]/span[1]/div[1]/div[1]/div[1]/div[1]/div[3]/form[1]/span[1]/div[1]/div[1]/div[1]/div[3]/div[2]/button[1]/span[1]")
	public WebElement CloudUpdateIntervalSameAsBattery;
    
    @FindBy(how = How.XPATH, using = "//body/div[@id='root']/div[2]/div[1]/div[2]/div[2]/div[2]/span[1]/div[1]/div[1]/div[1]/div[1]/div[3]/form[1]/span[1]/div[1]/div[1]/div[1]/div[5]/button[1]")
   	public WebElement OperatingHoursButton;
    
    @FindBy(how = How.XPATH, using = "//div[@id='mui-component-select-firmwareVersion']")
   	public WebElement FirmwareField;
    
    @FindBy(how = How.XPATH, using = "//body/div[@id='menu-firmwareVersion']/div[3]/ul[1]/li[1]")
   	public WebElement FirmwareOption;
    
    @FindBy(how = How.XPATH, using = "//span[contains(text(),'Apply')]")
   	public WebElement ApplyButton;
    
    @FindBy(how = How.XPATH, using = "//span[contains(text(),'Export data')]")
   	public WebElement ExportButton;
    
    @FindBy(how = How.XPATH, using = "//span[contains(text(),'Download')]")
   	public WebElement ExportDownloadButton;
    
    @FindBy(how = How.XPATH, using = "//span[contains(text(),'Email')]")
   	public WebElement ExportEmailButton;
    
    //@FindBy(how = How.XPATH, using = "/html[1]/body[1]/div[6]/div[1]")
    //@FindBy(how = How.XPATH, using = "//body/div[@id='menu-orgs']/div[1]")
    @FindBy(how = How.XPATH, using = "/html[1]/body[1]/div[18]")
    public WebElement ExportOutsideClick;
    
    @FindBy(how = How.XPATH, using = "//body/div[@id='root']/div[2]/div[1]/div[2]/div[2]/div[1]/div[1]/div[1]/div[5]/a[1]/button[1]")
   	public WebElement ActivityLog;
    
    @FindBy(how = How.XPATH, using = "//body/div[@id='root']/div[2]/div[1]/div[2]/div[2]/div[1]/nav[1]/ol[1]/li[3]")
   	public WebElement ActivityLogBreadcrumb;
	
    @FindBy(how = How.XPATH, using="//button[@title='Delete Device']")
	WebElement PowertrainDeviceDelete;
	//PageMethods
	public WebElement getDeviceName() throws IOException
	{        
		DataFile df = new DataFile();
		ArrayList<String> data =df.getData("DeviceName_Update");		
		DeviceName.click();
		DeviceName.clear();
		DeviceName.sendKeys(data.get(1));
		System.out.println("Entered DeviceName:"+DeviceName.getAttribute("value"));
		log.info("Entered DeviceName:"+DeviceName.getAttribute("value"));
		return DeviceName;
	}	
	public WebElement getDeviceNameSave()
	{
		waitForElementToBeVisible(DeviceNameSave, 30, 2);
		DeviceNameSave.click();
		System.out.println("Clicked on DeviceNameSave");
		log.info("Clicked on DeviceNameSave");
		return DeviceNameSave;		
	}
	public WebElement getDeviceDelete()
	{
		Actions a   = new Actions(driver);
		waitForElementToBeClickable(DeviceDelete, 30, 2);
		//DeviceDelete.click();
		a.moveToElement(DeviceDelete).click().build().perform();
		System.out.println("Clicked on DeviceDelete");
		log.info("Clicked on DeviceDelete");
		return DeviceDelete;		
	}
	public WebElement getPowertrainDeviceDelete()
	{
		Actions a   = new Actions(driver);
		//waitForElementToBeClickable(DeviceDelete, 30, 2);
		//DeviceDelete.click();
		a.moveToElement(PowertrainDeviceDelete).build().perform();
		System.out.println("Powertrain Device Delete is enabled:"+PowertrainDeviceDelete.isEnabled());
		log.info("Powertrain Device Delete is enabled:"+PowertrainDeviceDelete.isEnabled());
		return DeviceDelete;		
	}
	public WebElement getDeleteButtonPopup()
	{
		waitForElementToBeVisible(DeleteButtonPopup, 30, 2);
		DeleteButtonPopup.click();
		System.out.println("Clicked on DeleteButtonPopup");
		log.info("Clicked on DeleteButtonPopup");
		return DeleteButtonPopup;		
	}
	public WebElement getToasterMessage()
	{
		//waitForElementToBeVisible(ToasterMessage, 30, 2);
		System.out.println("Toaster Message:"+ToasterMessage.getText());
		log.info("Toaster Message:"+ToasterMessage.getText());
		return ToasterMessage;		
	}	
	public WebElement getToastMsg()
	{		
	   if (base.isElementExists(toastWebElmnt)) 
	   {
			System.out.println("Toast message : "+toastWebElmnt.getText());
			log.info("Toast message : "+toastWebElmnt.getText());
		} else 
		{
			System.out.println("Toast message not found ");
			log.info("Toast message not found ");
		}
		return toastWebElmnt;		
	}
    public WebElement ClickOnDevice() throws InterruptedException 
    {
    	DeviceNameField.click();
    	Thread.sleep(3000);
    	System.out.println("Device Clicked");
    	log.info("Device Clicked");
    	return DeviceNameField;    	
    }    
    public WebElement ClickOnDeviceSettings() throws InterruptedException 
    {
    	SettingsIcon.click();
    	Thread.sleep(2000);
    	System.out.println("Device Setting :"+ GeneralSettings.getText());
    	log.info("Device Setting :"+ GeneralSettings.getText());
       	return SettingsIcon;    	
    }    
    public WebElement RebootDevice() throws InterruptedException 
    {
    	waitForElementToBeClickable(RebootDevice, 30, 2);
    	RebootDevice.click();    	
    	Thread.sleep(1000);
    	System.out.println("Clicked on Reboot Device");
    	log.info("Clicked on Reboot Device");
    	//getToasterMessage();
       	return RebootDevice;    	
    }
    public WebElement CloudUpdateInterval() throws InterruptedException 
    {
    	CloudUpdateInterval.click();
    	System.out.println("Clicked on Cloud Update Interval");
    	log.info("Clicked on Cloud Update Interval");
    	Thread.sleep(1000);
    	CloudUpdateIntervalSelectAll.click();
    	System.out.println("Clicked on Cloud Update Interval Select All");
    	log.info("Clicked on Cloud Update Interval Select All");
    	Thread.sleep(1000);
    	CloudUpdateIntervalSave.click();
    	System.out.println("Clicked on Cloud Update Interval Save");
    	log.info("Clicked on Cloud Update Interval Save");
    	Thread.sleep(8000);
    	System.out.println("Cloud Upadate Interval :"+ CloudUpdateInterval.getText());
    	log.info("Cloud Upadate Interval  :"+ CloudUpdateInterval.getText());
       	return CloudUpdateInterval;    	
    }
    public WebElement CloudUpdateIntervalSameAsBattery() throws InterruptedException 
    {
    	CloudUpdateIntervalSameAsBattery.click();
    	System.out.println("Clicked on Cloud Update Interval Same as Battery");
    	log.info("Clicked on Cloud Update Interval Same as Battery");
    	Thread.sleep(1000);
    	CloudUpdateIntervalSelectAll.click();
    	System.out.println("Clicked on Cloud Update Interval Select All");
    	log.info("Clicked on Cloud Update Interval Select All");
    	Thread.sleep(1000);
    	CloudUpdateIntervalSave.click();
    	System.out.println("Clicked on Cloud Update Interval Save");
    	log.info("Clicked on Cloud Update Interval Save");
    	Thread.sleep(3000);
    	System.out.println("Cloud Upadate Interval same as battery:"+ CloudUpdateIntervalSameAsBattery.getText());
    	log.info("Cloud Upadate Interval  same as battery:"+ CloudUpdateIntervalSameAsBattery.getText());
       	return CloudUpdateIntervalSameAsBattery;    	
    }
    public WebElement operatingHour() throws InterruptedException 
    {
    	OperatingHoursButton.click();
    	System.out.println("Clicked on Operating Hours Button");
    	log.info("Clicked on Cloud Update Interval");
    	Thread.sleep(1000);
    	CloudUpdateIntervalSelectAll.click();
    	System.out.println("Clicked on Cloud Update Interval Select All");
    	log.info("Clicked on Cloud Update Interval Select All");
    	Thread.sleep(1000);
    	CloudUpdateIntervalSave.click();
    	System.out.println("Clicked on Cloud Update Interval Save");
    	log.info("Clicked on Cloud Update Interval Save");
    	Thread.sleep(3000);
    	System.out.println("Operating Hour:"+ OperatingHoursButton.getText());
    	log.info("Operating Hour:"+ OperatingHoursButton.getText());
       	return OperatingHoursButton;
    	
    }
    public WebElement UploadFirmware() throws InterruptedException 
    {
    	FirmwareField.click();
    	System.out.println("Clicked on Firmware");
    	log.info("Clicked on Firmware");
    	Thread.sleep(1000);
    	FirmwareOption.click();
    	System.out.println("Clicked on Firmware Option");
    	log.info("Clicked on Firmware Option");
    	Thread.sleep(1000);
    	ApplyButton.click();
    	System.out.println("Clicked on Apply Button");
    	log.info("Clicked on Apply Button");
    	Thread.sleep(3000);
    	System.out.println("Firmware :"+ FirmwareField.getText());
    	log.info("Firmware :"+ FirmwareField.getText());
       	return FirmwareField;    	
    }
    public WebElement exportClick() throws InterruptedException 
    {
    	ExportButton.click();
    	System.out.println("Export Button Click");
    	log.info("Export Button Click");
    	Thread.sleep(2000);
    	return ExportButton;
    }
    public WebElement exportDowload() throws InterruptedException 
    {
    	exportClick();
    	ExportDownloadButton.click();
    	System.out.println("Export Download Button Click");
    	log.info("Export Download Button Click");
    	Thread.sleep(3000);
    	
    	//getToasterMessage();
    	//Thread.sleep(5000);//Add Device Name
        //boolean found = base.downloadFile("26 Nov DN Update");
        //System.out.println("General Setting :"+ found);
        // log.info("General Setting :"+ found);
       	return ExportDownloadButton;    	
    }
    public WebElement ExportOutsideClick() throws InterruptedException 
    {
    	JavascriptExecutor executor = (JavascriptExecutor) driver;
    	executor.executeScript("arguments[0].click();", ExportOutsideClick);
    	System.out.println("Export Outside Click");
    	log.info("Export Outside Click");
    	Thread.sleep(2000);
    	return ExportOutsideClick;
    }
    public WebElement exportDowloadEmail() throws InterruptedException 
    {
    	exportClick();
    	System.out.println("Export Button Click");
    	log.info("Export Button Click");
    	ExportEmailButton.click();
    	System.out.println("Export Email Button Click");
    	log.info("Export Email Button Click");
    	Thread.sleep(3000);
    	//getToasterMessage();
       	return ExportEmailButton;  	
    }
    public WebElement ActivityLog() throws InterruptedException 
    {
    	ActivityLog.click();
    	System.out.println("Clicked on Device Activity Logs");
    	log.info("clicked on Device Activity Logs");
       	Thread.sleep(2000);
       	System.out.println("Activity Log Click :"+ ActivityLogBreadcrumb.getText());
    	log.info("Activity LogClick :"+ ActivityLogBreadcrumb.getText());
       	return ActivityLog;
    }
}
