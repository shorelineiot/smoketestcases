package PageObjects;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.asserts.SoftAssert;

import Resources.DataFile;
import Resources.base;

public class RulesPage extends base{
	
    public static Logger log =LogManager.getLogger(RulesPage.class.getName());
	
	//Declare WebDriver Globally
	public WebDriver driver;
			
	//Constructor
	public RulesPage(WebDriver driver)
	{
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}

	//PageObjects
	@FindBy(how = How.XPATH, using="//span[normalize-space()='Create Rules']")
	public WebElement CreateRuleButton;
	
	@FindBy(how = How.XPATH, using="//input[@name='name']")
	public WebElement RuleNameField;
	
	@FindBy(how = How.XPATH, using="//input[@name='holdoff_time_ms']")
	public WebElement HoldOffTime;
	
	@FindBy(how = How.XPATH, using="//*[@id='mui-component-select-alert_level']")
	public WebElement RuleLevelDropdown;
	
	@FindBy(how = How.XPATH, using="//div[@id='menu-alert_level']//li[1]")
	public WebElement RuleLevelDropdownSelect;
	
	@FindBy(how = How.XPATH, using="//div[@id='mui-component-select-conditions[0].device']")
	public WebElement RuleDeviceDropdown;
	
	@FindBy(how = How.XPATH, using="//li[normalize-space()='Royals']")
	public WebElement RuleDeviceSelect;
	
	@FindBy(how = How.XPATH, using="//div[@id='mui-component-select-conditions[0].dpid']")
	public WebElement RuleSensorDropdown;
	
	@FindBy(how = How.XPATH, using="//li[normalize-space()='Humidity']")
	public WebElement RuleSensorSelect;
	
	@FindBy(how = How.XPATH, using="//div[@id='mui-component-select-conditions[0].operator']")
	public WebElement RuleConditionDropdown;
	
	@FindBy(how = How.XPATH, using="//li[normalize-space()='greater than']")
	public WebElement RuleConditionSelect;
	
	@FindBy(how = How.XPATH, using="//input[@name='conditions[0].value']")
	public WebElement RuleConditionValue;
	
	@FindBy(how = How.XPATH, using="//span[normalize-space()='Next']")
	public WebElement NextButton;
	
	@FindBy(how = How.XPATH, using="//span[normalize-space()='Save Rule']")
	public WebElement SaveRuleButton;
	
	//@FindBy(how = How.XPATH, using="//tbody/tr[1]/td[5]/div[1]/h3[1]/div[1]/div[1]/div[1]")
	@FindBy(how = How.XPATH, using="//tbody/tr[1]/td[5]/div[1]/h3[1]/div[1]/div[1]/div[1]")
	public WebElement ToasterMessageCreate;
	
	@FindBy(how = How.XPATH, using="/html/body/div[1]/div[3]/div/div/div/div/div/div/div/div[2]/p")
	public WebElement ToasterMessageUpdate;
	
	//@FindBy(how = How.XPATH, using="/html/body/div[1]/div[2]/div/div[2]/div[2]/div[3]/div/div/div[2]/div/div/div/table/tbody/tr[1]/td[4]/div/div/div/span")
	//@FindBy(how = How.XPATH, using="/html/body/div[1]/div[2]/div/div[2]/div[2]/div[3]/div/div/div[2]/div/div/div/table/tbody/tr[2]/td[4]/div/div/div/span")
	@FindBy(how = How.XPATH, using="//tbody/tr[2]/td[3]/div[1]/div[1]/div[1]/span[1]")
	public WebElement SecondRuleDropdowwn;
	
	@FindBy(how = How.XPATH, using="//span[normalize-space()='Low']")
	public WebElement SecondRuleDropdownSelect;
	
	@FindBy(how = How.XPATH, using="//span[normalize-space()='Apply']")
	public WebElement ApplyButtonConfirmation;
	
	@FindBy(how = How.XPATH, using="//input[@placeholder='Search Rules']")
	public WebElement SearchRuleField;
	
	//@FindBy(how = How.XPATH, using="/html/body/div[1]/div[2]/div/div[2]/div[2]/div[3]/div/div/div[2]/div/div/div/table/tbody/tr[1]/td[5]/button[2]")
	//@FindBy(how = How.XPATH, using="/html[1]/body[1]/div[1]/div[2]/div[1]/div[2]/div[2]/div[3]/div[1]/div[1]/div[2]/div[1]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/td[5]/span[2]/button[1]")                                 	
	@FindBy(how = How.XPATH, using="//tbody/tr[1]/td[4]/span[3]/button[1]")
	public WebElement DeleteSearchRule;
	
	@FindBy(how = How.XPATH, using="//span[contains(text(),'Narrowband Rules')]")
	public WebElement NarrowbandRule;
	
	@FindBy(how = How.XPATH, using="//tbody/tr[1]/td[2]")
	public WebElement Narrowband1stRuleField;
	
	@FindBy(how = How.XPATH, using="//tbody/tr[1]")
	public WebElement Narrowband1stRuleRow;
	
	@FindBy(how = How.XPATH, using="//tbody/tr[1]/td[7]/div[1]/button[1]")
	public WebElement NarrowbandRuleEditIcon;
	
	@FindBy(how = How.XPATH, using="//tbody/tr[1]/td[3]/div[1]/div[1]/input[1]")
	public WebElement NarrowbandRuleCriticalField;
	
	@FindBy(how = How.XPATH, using="//tbody/tr[1]/td[7]/div[1]/button[2]/span[1]")
	public WebElement NarrowbandRuleCriticalEditSave;
	
	@FindBy(how = How.XPATH, using="//tbody/tr[1]/td[7]/div[1]/button[3]")
	public WebElement NarrowbandRuleDelete;
	
	@FindBy(how = How.XPATH, using="//tbody/tr[1]/td[7]/div[1]/button[2]")
	public WebElement NarrowbandRuleMsgButton;
	
	@FindBy(how = How.XPATH, using="//body/div[7]/div[3]/form[1]/div[1]/div[1]/div[1]/div[1]/button[1]")
	public WebElement NarrowbandRuleMsgTagsButton;
	
	@FindBy(how = How.XPATH, using="//body/div[@id='simple-menu']/div[3]/ul[1]/li[1]")
	public WebElement NarrowbandRuleMsgTagsOption;
	
	@FindBy(how = How.XPATH, using="//span[contains(text(),'Save')]")
	public WebElement NarrowbandRuleSave;
	
	@FindBy(how = How.XPATH, using="//span[contains(text(),'Apply')]")
	public WebElement NarrowbandRuleApply;
	
	@FindBy(how = How.XPATH, using = "//body/div[@id='root']/div[3]/div[1]/div[1]/div[1]/div[1]/div[1]/div[1]")
	public WebElement toastWebElmnt;
	
	//PageMethods
	public WebElement getToastMsg()
	{		
	   if (base.isElementExists(toastWebElmnt)) 
	   {
			System.out.println("Toast message : "+toastWebElmnt.getText());
			log.info("Toast message : "+toastWebElmnt.getText());
		} else 
		{
			System.out.println("Toast message not found ");
			log.info("Toast message not found ");
		}
		return toastWebElmnt;		
	}
	
	public WebElement getCreateRuleButton()
	{
		waitForElementToBeVisible(CreateRuleButton, 30, 2);
		CreateRuleButton.click();
		System.out.println("Clicked on Create Rule Button");
		log.info("Clicked on Create Rule Button");
		return CreateRuleButton;		
	}
	public WebElement getRuleNameField() throws IOException
	{
		DataFile df = new DataFile();
		ArrayList<String> data =df.getData("Sensor_Rule_Name");		
		RuleNameField.sendKeys(data.get(1));
		System.out.println("Entered Rule Name:"+RuleNameField.getAttribute("value"));
		log.info("Entered Email Address:"+RuleNameField.getAttribute("value"));
		return RuleNameField;
	}
	public WebElement getRuleHoldOffTime() throws IOException
	{
		DataFile df = new DataFile();
		ArrayList<String> data =df.getData("Sensor_Rule_HoldOffTime");		
		HoldOffTime.sendKeys(data.get(1));
		System.out.println("Entered Hold Off Time:"+HoldOffTime.getAttribute("value"));
		log.info("Entered Hold Off Time:"+HoldOffTime.getAttribute("value"));
		return HoldOffTime;
	}
	public WebElement getRuleLevelDropdown()
	{
		waitForElementToBeVisible(RuleLevelDropdown, 30, 2);
		RuleLevelDropdown.click();
		System.out.println("Clicked on RuleLevelDropdown");
		log.info("Clicked on RuleLevelDropdown");
		return RuleLevelDropdown;		
	}
	public WebElement getRuleLevelDropdownSelect()
	{
		waitForElementToBeVisible(RuleLevelDropdownSelect, 30, 2);
		RuleLevelDropdownSelect.click();
		System.out.println("Clicked on RuleLevelDropdownSelect");
		log.info("Clicked on RuleLevelDropdownSelect");
		return RuleLevelDropdownSelect;		
	}
	public WebElement getRuleDeviceDropdown()
	{
		waitForElementToBeVisible(RuleDeviceDropdown, 30, 2);
		RuleDeviceDropdown.click();
		System.out.println("Clicked on RuleDeviceDropdown");
		log.info("Clicked on RuleDeviceDropdown");
		return RuleDeviceDropdown;		
	}
	public WebElement getRuleDeviceSelect()
	{
		waitForElementToBeVisible(RuleDeviceSelect, 30, 2);
		RuleDeviceSelect.click();
		System.out.println("Clicked on RuleDeviceSelect");
		log.info("Clicked on RuleDeviceSelect");
		return RuleDeviceSelect;		
	}
	public WebElement getRuleSensorDropdown() throws InterruptedException
	{
		waitForElementToBeClickable(RuleSensorDropdown, 80, 2);
		Thread.sleep(5000);
		RuleSensorDropdown.click();
		Thread.sleep(2000);
		System.out.println("Clicked on RuleSensorDropdown");
		log.info("Clicked on RuleSensorDropdown");
		return RuleSensorDropdown;		
	}
	public WebElement getRuleSensorSelect()
	{
		JavascriptExecutor js = (JavascriptExecutor)driver;
		waitForElementToBeVisible(RuleSensorSelect, 80, 2);
		//RuleSensorSelect.click();
		js.executeScript("arguments[0].click();", RuleSensorSelect);
		System.out.println("Clicked on RuleSensorSelect");
		log.info("Clicked on RuleSensorSelect");
		return RuleSensorSelect;		
	}
	public WebElement getRuleConditionDropdown()
	{
		waitForElementToBeVisible(RuleConditionDropdown, 30, 2);
		RuleConditionDropdown.click();
		System.out.println("Clicked on RuleConditionDropdown");
		log.info("Clicked on RuleConditionDropdown");
		return RuleConditionDropdown;		
	}
	public WebElement getRuleConditionSelect()
	{
		waitForElementToBeVisible(RuleConditionSelect, 30, 2);
		RuleConditionSelect.click();
		System.out.println("Clicked on RuleConditionSelect");
		log.info("Clicked on RuleConditionSelect");
		return RuleConditionSelect;		
	}
	public WebElement getRuleConditionValue() throws IOException
	{
		DataFile df = new DataFile();
		ArrayList<String> data =df.getData("Sensor_Rule_ConditionValue");		
		RuleConditionValue.sendKeys(data.get(1));
		System.out.println("Entered Condition Value:"+RuleConditionValue.getAttribute("value"));
		log.info("Entered Condition Value:"+RuleConditionValue.getAttribute("value"));
		return RuleConditionValue;
	}
	public WebElement getNextButton()
	{
		waitForElementToBeVisible(NextButton, 30, 2);
		NextButton.click();
		System.out.println("Clicked on NextButton");
		log.info("Clicked on Create NextButton");
		return NextButton;		
	}
	public WebElement getSaveRuleButton()
	{
		waitForElementToBeVisible(SaveRuleButton, 30, 2);
		SaveRuleButton.click();
		System.out.println("Clicked on Save Rule Button");
		log.info("Clicked on Save Rule Button");
		return SaveRuleButton;		
	}
	public WebElement getToasterMessageRuleCreate()
	{
		waitForElementToBeVisible(ToasterMessageCreate, 30, 2);
		 if (base.isElementExists(ToasterMessageCreate)) {
				String RuleCreateSuccessTextActual = ToasterMessageCreate.getText();
				System.out.println("RuleCreateSuccessTextActual:"+ToasterMessageCreate.getText());
				log.info("RuleCreateSuccessTextActual:"+ToasterMessageCreate.getText());
				String RuleCreateSuccessTextExpected = "Rule created Successfully!";
				System.out.println("RuleCreateSuccessTextExpected:"+RuleCreateSuccessTextExpected);
				log.info("RuleCreateSuccessTextExpected:"+RuleCreateSuccessTextExpected);
				Assert.assertEquals(RuleCreateSuccessTextActual,RuleCreateSuccessTextExpected);
			} else {
				System.out.println("Toast message not found ");
				log.info("Toast message not found ");
			}			
		return ToasterMessageCreate;		
	}
	public WebElement getToasterMessageRuleUpdate()
	{
		waitForElementToBeVisible(ToasterMessageUpdate, 30, 2);
		 
		if (base.isElementExists(ToasterMessageUpdate)) 
		{
			SoftAssert sa = new SoftAssert();
			String RuleUpdateSuccessTextActual = ToasterMessageUpdate.getText();
			System.out.println("RuleUpdateSuccessTextActual:"+ToasterMessageUpdate.getText());
			log.info("RuleUpdateSuccessTextActual:"+ToasterMessageUpdate.getText());
			String RuleUpdateSuccessTextExpected = "Rule updated Successfully!";
			System.out.println("RuleUpdateSuccessTextExpected:"+RuleUpdateSuccessTextExpected);
			log.info("RuleUpdateSuccessTextExpected:"+RuleUpdateSuccessTextExpected);
			sa.assertEquals(RuleUpdateSuccessTextActual,RuleUpdateSuccessTextExpected);
			sa.assertAll();
		} else 
		{
			System.out.println("Toast message not found ");
			log.info("Toast message not found ");
		}			
		return ToasterMessageUpdate;		
	}
	public WebElement getToasterMessageRuleDelete()
	{
		waitForElementToBeVisible(ToasterMessageUpdate, 30, 2);
		 if (base.isElementExists(ToasterMessageUpdate)) {
				String RuleDeleteSuccessTextActual = ToasterMessageUpdate.getText();
				System.out.println("RuleDeleteSuccessTextActual:"+ToasterMessageUpdate.getText());
				log.info("RuleDeleteSuccessTextActual:"+ToasterMessageUpdate.getText());
				String RuleDeleteSuccessTextExpected = "Rule deleted Successfully!";
				System.out.println("RuleDeleteSuccessTextExpected:"+RuleDeleteSuccessTextExpected);
				log.info("RuleDeleteSuccessTextExpected:"+RuleDeleteSuccessTextExpected);
				Assert.assertEquals(RuleDeleteSuccessTextActual,RuleDeleteSuccessTextExpected);
			} else {
				System.out.println("Toast message not found ");
				log.info("Toast message not found ");
			}			
		return ToasterMessageUpdate;		
	}
	public WebElement getSecondRuleDropdowwn()
	{
		waitForElementToBeVisible(SecondRuleDropdowwn, 30, 2);
		SecondRuleDropdowwn.click();
		System.out.println("Clicked on SecondRuleDropdowwn");
		log.info("Clicked on FirstRuleDropdowwn");
		return SecondRuleDropdowwn;		
	}
	public WebElement getSecondRuleDropdowwnSelect()
	{
		waitForElementToBeVisible(SecondRuleDropdownSelect, 30, 2);
		SecondRuleDropdownSelect.click();
		System.out.println("Clicked on SecondRuleDropdowwnSelect");
		log.info("Clicked on SecondRuleDropdowwnSelect");
		return SecondRuleDropdownSelect;		
	}
	public WebElement getApplyButtonConfirmation()
	{
		waitForElementToBeVisible(ApplyButtonConfirmation, 30, 2);
		ApplyButtonConfirmation.click();
		System.out.println("Clicked on ApplyButtonConfirmation");
		log.info("Clicked on ApplyButtonConfirmation");
		return ApplyButtonConfirmation;		
	}
	public WebElement getSearchRuleField() throws IOException
	{
		DataFile df = new DataFile();
		ArrayList<String> data =df.getData("Search_Rule");		
		SearchRuleField.sendKeys(data.get(1));
		System.out.println("Entered Rule in Search Rule:"+SearchRuleField.getAttribute("value"));
		log.info("Entered Rule in Search Rule:"+SearchRuleField.getAttribute("value"));
		return SearchRuleField;
	}
	public WebElement getSearchRuleFieldUpdate() throws IOException
	{
		DataFile df = new DataFile();
		ArrayList<String> data =df.getData("Search_Rule_Update");		
		SearchRuleField.sendKeys(data.get(1));
		System.out.println("Entered Rule in Search Rule:"+SearchRuleField.getAttribute("value"));
		log.info("Entered Rule in Search Rule:"+SearchRuleField.getAttribute("value"));
		return SearchRuleField;
	}
	public WebElement getDeleteSearchRule()
	{
		waitForElementToBeVisible(DeleteSearchRule, 30, 2);
		DeleteSearchRule.click();
		System.out.println("Clicked on Delete Rule");
		log.info("Clicked on Delete Rule");
		return DeleteSearchRule;		
	}	
	public WebElement getNarrowbandRuleButton()
	{
		waitForElementToBeVisible(NarrowbandRule, 30, 2);
		NarrowbandRule.click();
		System.out.println("Clicked on NarrowbandRule Button");
		log.info("Clicked on NarrowbandRule Button");
		return NarrowbandRule;		
	}
	public WebElement searchNarrowband() throws InterruptedException {
		Thread.sleep(3000);
		String NwName = Narrowband1stRuleField.getText();
		SearchRuleField.click();
		SearchRuleField.clear();
		SearchRuleField.sendKeys(NwName);
		Thread.sleep(2000);
		System.out.println("Narrowband Rule search result"+ Narrowband1stRuleRow);
		log.info("Narrowband Rule search result" + Narrowband1stRuleRow);
		return SearchRuleField;
	}
	public WebElement editNarrowbandRule() throws InterruptedException {
		Thread.sleep(3000);
		NarrowbandRuleEditIcon.click();
		System.out.println("Narrowband Rule edit button click");
		log.info("Narrowband Rule edit button click");
		NarrowbandRuleCriticalField.click();
		NarrowbandRuleCriticalField.sendKeys(Keys.CONTROL + "a");
		NarrowbandRuleCriticalField.sendKeys(Keys.DELETE);
		NarrowbandRuleCriticalField.sendKeys("1");
		System.out.println("Narrowband Rule edited with 1 value");
		log.info("Narrowband Rule edited with 1 value");
		NarrowbandRuleCriticalEditSave.click();
		Thread.sleep(4000);
		getToastMsg();
		return NarrowbandRuleEditIcon;
	}
	public WebElement MessageNarrowbandRule() throws InterruptedException {
		Thread.sleep(3000);
		NarrowbandRuleMsgButton.click();
		System.out.println("Narrowband Rule message button click");
		log.info("Narrowband Rule message button click");
		NarrowbandRuleMsgTagsButton.click();
		System.out.println("Narrowband Rule tags button click");
		log.info("Narrowband Rule tags button click");
		NarrowbandRuleMsgTagsOption.click();
		NarrowbandRuleSave.click();
		System.out.println("Narrowband Rule save button click");
		log.info("Narrowband Rule save button click");
		Thread.sleep(4000);
		getToastMsg();
		return NarrowbandRuleMsgButton;
	}
	public WebElement deleteNarrowbandRule() throws InterruptedException {
		Thread.sleep(3000);
		NarrowbandRuleDelete.click();
		NarrowbandRuleApply.click();
		System.out.println("Narrowband Rule delete button click");
		log.info("Narrowband Rule delete button click");
		Thread.sleep(4000);
		getToastMsg();
		return NarrowbandRuleDelete;
	}

}
