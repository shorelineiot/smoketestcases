package PageObjects;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.asserts.SoftAssert;

import Resources.DataFile;

public class DeviceMailboxPage {

	  public static Logger log =LogManager.getLogger(DeviceMailboxPage.class.getName());
		
		//Declare WebDriver Globally
		public WebDriver driver;
			
		//Constructor
		public DeviceMailboxPage(WebDriver driver)
		{
			this.driver=driver;
			PageFactory.initElements(driver, this);
		}

		//PageObjects
		@FindBy(how = How.XPATH, using="//span[contains(text(),'Reports')]")
		WebElement Reports;
		
		@FindBy(how = How.XPATH, using="//body/div[@id='root']/div[2]/div[1]/div[2]/div[1]/div[1]/ul[1]/div[3]/div[1]/div[1]/div[1]/a[3]/div[1]/div[1]/span[1]")
		WebElement DeviceMailbox;
		
		@FindBy(how = How.XPATH, using="//body/div[@id='root']/div[2]/div[1]/div[2]/div[2]/div[2]/div[1]/div[1]/div[2]/div[1]/input[1]")
		WebElement DeviceMailboxSearch;
		
		@FindBy(how = How.XPATH, using="//tbody/tr[1]")
		WebElement DeviceMailbox1stRow;
										
		@FindBy(how = How.XPATH, using="//body/div[@id='root']/div[2]/div[1]/div[2]/div[2]/div[2]/div[1]/div[1]/div[3]/div[1]/div[1]/span[1]/button[1]")
		WebElement DeviceMailboxColumnOption;
		
		@FindBy(how = How.XPATH, using="//input[@id='column-toggle-0']")
		WebElement DeviceMailboxColumn1stOption;
		
		@FindBy(how = How.XPATH, using="//input[@id='column-toggle-1']")
		WebElement DeviceMailboxColumn2stOption;
		
		@FindBy(how = How.XPATH, using="//body/div[7]/div[1]")
		WebElement ExternalClick;
		
		@FindBy(how = How.XPATH, using="//thead/tr[1]")
		WebElement TableHeaders;
		
		//Page Methods	 
		SoftAssert sa = new SoftAssert();
		public WebElement getReportsTab() throws InterruptedException
		{
			Reports.click();
			System.out.println("Clicked on Reports tab");
			log.info("Clicked on Reports tab");
			Thread.sleep(2000);
			return Reports;		
		}	
		public WebElement getDeviceMailboxTab() throws InterruptedException
		{
			getReportsTab();
			DeviceMailbox.click();
			System.out.println("Clicked on DeviceMailbox tab");
			log.info("Clicked on DeviceMailbox tab");
			Thread.sleep(5000);
       		sa.assertEquals(TableHeaders.getText(), "Request Type\n" + 
       				"Device Name\n" + 
       				"Created On\n" + 
       				"Acknowledged On\n" + 
       				"Status\n" + 
       				"Request\n" + 
       				"Response");
       		sa.assertAll();
			return DeviceMailbox;		
		}	
		public WebElement DeviceMailboxSearch() throws InterruptedException, IOException
		{
			DeviceMailboxSearch.click();
			DeviceMailboxSearch.sendKeys(Keys.CONTROL + "a");
			DeviceMailboxSearch.sendKeys(Keys.DELETE);
			DataFile df = new DataFile();
			ArrayList<String> data =df.getData("MailboxRequestType");		
			DeviceMailboxSearch.sendKeys(data.get(1));
			System.out.println("Clicked on DeviceMailbox search");
			log.info("Clicked on DeviceMailbox search");
			Thread.sleep(1000);
			System.out.println("DeviceMailbox search Result: "+DeviceMailbox1stRow.getText());
			log.info("DeviceMailbox search Result:"+DeviceMailbox1stRow.getText());
			return DeviceMailbox;		
		}	
		public WebElement DeviceColumn() throws InterruptedException {
			getDeviceMailboxTab();
			DeviceMailboxColumnOption.click();
			System.out.println("DeviceMailbox table column option click ");
			log.info("DeviceMailbox table column option click");
			Thread.sleep(1000);
			DeviceMailboxColumn1stOption.click();
			System.out.println("DeviceMailbox table Device Type column option click ");
			log.info("DeviceMailbox table Device Type column option click");
			DeviceMailboxColumn2stOption.click();
			System.out.println("DeviceMailbox table Device Name column option click ");
			log.info("DeviceMailbox table Device Name column option click");
			Thread.sleep(1000);
			System.out.println("DeviceMailbox table column option click ");
			log.info("DeviceMailbox table column option click");
			Thread.sleep(1000);
			JavascriptExecutor executor = (JavascriptExecutor) driver;
		    executor.executeScript("arguments[0].click();", ExternalClick);
		    Thread.sleep(1000);
			System.out.println("DeviceMailbox table Headers: "+TableHeaders.getText());
			log.info("DeviceMailbox table Headers:"+TableHeaders.getText());
			sa.assertEquals(TableHeaders.getText(), "Created On\n" + 
					"Acknowledged On\n" + 
					"Status\n" + 
					"Request\n" + 
					"Response");
			return DeviceMailboxColumnOption;
		}
		
}
