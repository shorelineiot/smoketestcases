package PageObjects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.asserts.SoftAssert;

public class ExternalDataPage {
	public static Logger log =LogManager.getLogger(ExternalDataPage.class.getName());
	
	//Declare WebDriver Globally
	public WebDriver driver;
		
	//Constructor
	public ExternalDataPage(WebDriver driver)
	{
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}

	//Page Objects
	@FindBy(how = How.XPATH, using="//span[contains(text(),'Reports')]")
	WebElement Reports;
											
	@FindBy(how = How.XPATH, using="/html[1]/body[1]/div[1]/div[2]/div[1]/div[2]/div[1]/div[1]/ul[1]/div[3]/div[1]/div[1]/div[1]/a[2]/div[1]/div[1]/span[1]")
	WebElement ExternalData;
	
	@FindBy(how = How.XPATH, using="//thead/tr[1]")
	WebElement TableHeaders;
	
	//Page Methods	 
	SoftAssert sa = new SoftAssert();
	public WebElement getReportsTab() throws InterruptedException
	{
		Reports.click();
		System.out.println("Clicked on Reports tab");
		log.info("Clicked on Reports tab");
		Thread.sleep(2000);
		return Reports;		
	}	
	public WebElement getExternalDataTab() throws InterruptedException
	{
		getReportsTab();
		ExternalData.click();
		System.out.println("Clicked on ExternalData tab");
		log.info("Clicked on ExternalData tab");
		Thread.sleep(5000);
		System.out.println("ExternalData Headers : " + TableHeaders.getText());
		sa.assertEquals(TableHeaders.getText(), "Upload Timestamp\n" + 
				"Source\n" + 
				"Total Records\n" + 
				"Success\n" + 
				"Failure\n" + 
				"Data Size(MB)");
		sa.assertAll();
		return ExternalData;		
	}	

}
