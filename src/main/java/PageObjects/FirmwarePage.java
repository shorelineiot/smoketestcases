package PageObjects;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import Resources.DataFile;
import Resources.base;
//import Testv2.File;

public class FirmwarePage extends base{
	
    public static Logger log =LogManager.getLogger(FirmwarePage.class.getName());
	
	//Declare WebDriver Globally
	public WebDriver driver;
		
	//Constructor
	public FirmwarePage(WebDriver driver)
	{
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	//Page Objects
	@FindBy(how = How.XPATH, using="//span[normalize-space()='Upload Firmware']")
	public WebElement UploadFirmware;
	
	@FindBy(how = How.ID, using="fileUploadFirmware")
	public WebElement ChooseFileFirmware;
	
	@FindBy(how = How.ID, using="")
	public WebElement ChooseFileReleaseNotes;
	
	@FindBy(how = How.XPATH, using="//input[@name='firmwareName']")
	public WebElement FirmwareName;
	
	@FindBy(how = How.XPATH, using="//input[@name='firmwareVersion']")
	public WebElement ManifestVersion;
	
	@FindBy(how = How.XPATH, using="//input[@name='description']")
	public WebElement FirmwareDescription;
	
	@FindBy(how = How.XPATH, using="/html[1]/body[1]/div[7]/div[3]/div[2]/form[1]/div[2]/div[1]/div[8]/div[1]/div[1]/div[2]/div[1]")
	public WebElement ModelDropdown;
	
	@FindBy(how = How.XPATH, using="//li[normalize-space()='iCastSense']")
	public WebElement SelectModelICastSense;
	
	@FindBy(how = How.XPATH, using="//span[normalize-space()='Upload']")
	public WebElement UploadButton;
	
	@FindBy(how = How.XPATH, using="//tbody/tr[1]/td[6]/div[1]/button[1]/span[1]//*[name()='svg']//*[name()='g' and @id='Add']//*[name()='g' and @id='Group_1956']//*[name()='g' and @id='Ellipse_1149']//*[name()='circle']")
	public WebElement ActionPlusIcon;
	
	@FindBy(how = How.XPATH, using="/html[1]/body[1]/div[1]/div[2]/div[1]/div[2]/div[2]/div[3]/div[1]/div[2]/div[1]/div[1]/div[1]/table[1]/tbody[1]/tr[1]/td[1]/span[1]/span[1]/input[1]")
	public WebElement SelectDeviceCheckbox;
	
	@FindBy(how = How.XPATH, using="//span[normalize-space()='Update Firmware']")
	public WebElement UpdateFirmwareButton;

	//PageMethods
	public WebElement getUploadFirmwareButton()
	{
		waitForElementToBeVisible(UploadFirmware, 30, 2);
		UploadFirmware.click();
		System.out.println("Clicked on Upload Firmware Button");
		log.info("Clicked on Upload Firmware Button");
		return UploadFirmware;		
	}
	
	String getFile() {
    	return new File("./firmwarefiles/ReleaseNotes.csv").getAbsolutePath();
    }
	
	public WebElement getChooseFileFirmware() throws InterruptedException
	{
		//waitForElementToBeVisible(ChooseFileFirmware, 30, 2);
		JavascriptExecutor jse = ((JavascriptExecutor)driver);        	
		//ChooseFileFirmware.click();
		Thread.sleep(5000);
		//System.out.println("Clicked on Choose File Firmware");
		//log.info("Clicked on Choose File Firmware");
		//String firmwarefile = System.getProperty("/home/amey306/eclipse-workspace/Projects/ShorelineCloudWebProject/firmwarefiles/Firmware");
		jse.executeScript("arguments[0].value='getFile()';", ChooseFileFirmware);
		Thread.sleep(5000);
		//ChooseFileFirmware.sendKeys("/home/amey306/eclipse-workspace/Projects/ShorelineCloudWebProject/firmwarefiles/Firmware");
		log.info("Added Firmware File");
		System.out.println("Added Firmware File");
		return ChooseFileFirmware;		
	}
	public WebElement getChooseFileReleaseNotes(String path) throws InterruptedException, AWTException
	{
		//waitForElementToBeVisible(ChooseFileReleaseNotes, 30, 2);
		//ChooseFileReleaseNotes.click();
		//Thread.sleep(4000);
		//System.out.println("Clicked on Choose File Firmware");
		//log.info("Clicked on Choose File Firmware");
		//String firmwarefile = System.getProperty("/home/amey306/eclipse-workspace/Projects/ShorelineCloudWebProject/firmwarefiles/Firmware");
		Robot robot = new Robot();
		ChooseFileReleaseNotes.click();
		
		robot.setAutoDelay(4000);
		StringSelection strSelection = new StringSelection(path);
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(strSelection, null);
		robot.setAutoDelay(2000);
		robot.keyPress(KeyEvent.VK_ENTER);
        robot.keyRelease(KeyEvent.VK_ENTER);
        robot.keyPress(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_V);
        robot.keyRelease(KeyEvent.VK_CONTROL);
        robot.keyPress(KeyEvent.VK_ENTER);
        robot.delay(200);
        robot.keyRelease(KeyEvent.VK_ENTER);
		//ChooseFileReleaseNotes.sendKeys(getFile());
		log.info("Added Release Notes File");
		System.out.println("Added Release Notes File");
		return ChooseFileReleaseNotes;		
	}
	public WebElement getFirmwareName() throws IOException
	{        
		DataFile df = new DataFile();
		ArrayList<String> data =df.getData("FirmwareName");		
		FirmwareName.click();
		FirmwareName.clear();
		FirmwareName.sendKeys(data.get(1));
		System.out.println("Entered FirmwareName:"+FirmwareName.getAttribute("value"));
		log.info("Entered FirmwareName:"+FirmwareName.getAttribute("value"));
		return FirmwareName;
	}	
	public WebElement getManifestVersion() throws IOException
	{        
		DataFile df = new DataFile();
		ArrayList<String> data =df.getData("ManifestVersion");		
		ManifestVersion.click();
		ManifestVersion.clear();
		ManifestVersion.sendKeys(data.get(1));
		System.out.println("Entered ManifestVersion:"+ManifestVersion.getAttribute("value"));
		log.info("Entered ManifestVersion:"+ManifestVersion.getAttribute("value"));
		return ManifestVersion;
	}
	public WebElement getFirmwareDescription() throws IOException
	{        
		DataFile df = new DataFile();
		ArrayList<String> data =df.getData("FirmwareDescription");		
		FirmwareDescription.click();
		FirmwareDescription.clear();
		FirmwareDescription.sendKeys(data.get(1));
		System.out.println("Entered FirmwareDescription:"+FirmwareDescription.getAttribute("value"));
		log.info("Entered FirmwareDescription:"+FirmwareDescription.getAttribute("value"));
		return FirmwareDescription;
	}
	public WebElement getModelDropdown()
	{
		waitForElementToBeVisible(ModelDropdown, 30, 2);
		ModelDropdown.click();
		System.out.println("Clicked on ModelDropdown");
		log.info("Clicked on ModelDropdown");
		return ModelDropdown;		
	}
	public WebElement getSelectModelICastSense()
	{
		waitForElementToBeVisible(SelectModelICastSense, 30, 2);
		SelectModelICastSense.click();
		System.out.println("Clicked on SelectModelICastSense");
		log.info("Clicked on SelectModelICastSense");
		return SelectModelICastSense;		
	}
	public WebElement getUploadButton()
	{
		waitForElementToBeVisible(UploadButton, 30, 2);
		UploadButton.click();
		System.out.println("Clicked on UploadButton");
		log.info("Clicked on UploadButton");
		return UploadButton;		
	}
	public WebElement getActionPlusIcon()
	{
		waitForElementToBeVisible(ActionPlusIcon, 30, 2);
		ActionPlusIcon.click();
		System.out.println("Clicked on ActionPlusIcon");
		log.info("Clicked on ActionPlusIcon");
		return ActionPlusIcon;		
	}
	public WebElement getSelectDeviceCheckbox()
	{
		waitForElementToBeVisible(SelectDeviceCheckbox, 30, 2);
		SelectDeviceCheckbox.click();
		System.out.println("Clicked on SelectDeviceCheckbox");
		log.info("Clicked on SelectDeviceCheckbox");
		return SelectDeviceCheckbox;		
	}
	public WebElement getUpdateFirmwareButton()
	{
		waitForElementToBeVisible(UpdateFirmwareButton, 30, 2);
		UpdateFirmwareButton.click();
		System.out.println("Clicked on Update Firmware Button");
		log.info("Clicked on Update Firmware Button");
		return UpdateFirmwareButton;		
	}
}
